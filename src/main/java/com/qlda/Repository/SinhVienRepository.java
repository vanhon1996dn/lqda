package com.qlda.Repository;


import com.qlda.Entity.SinhVien;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SinhVienRepository extends CrudRepository<SinhVien,Integer> {
    List<SinhVien> findAll();
    SinhVien findSinhVienById(Integer id);
    SinhVien findSinhVienByMaSV(Integer maSV);
    List<SinhVien> findSinhViensByDoAn_MaDoAn(Integer maDoAn);
    List<SinhVien> findSinhViensByKhoa_Id(Integer khoaId);
}
