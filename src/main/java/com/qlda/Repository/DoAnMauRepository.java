package com.qlda.Repository;

import com.qlda.Entity.DoAnMau;
import com.qlda.Entity.DoAnMau;
import com.qlda.Entity.GiaoVien;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DoAnMauRepository extends CrudRepository<DoAnMau,Integer> {
    List<DoAnMau> findDoAnMausByGiaoVien(GiaoVien giaoVien);
    List<DoAnMau> findDoAnMausByGiaoVien_MaGV(Integer maGV);
    DoAnMau findDoAnMauById(Integer id);
    DoAnMau findDoAnMauByMaDoAn(Integer maDoAn);
    List<DoAnMau> findAll();
}
