package com.qlda.Repository;

import com.qlda.Entity.FileOfDoAn;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileOfDoAnRepository extends CrudRepository<FileOfDoAn,Integer> {

}
