package com.qlda.Repository;

import com.qlda.Entity.HoiDong;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface HoiDongRepository extends CrudRepository<HoiDong,Integer> {
    List<HoiDong> findAll();
    HoiDong findHoiDongById(Integer id);
    HoiDong findHoiDongByMaHD(Integer maHD);
    List<HoiDong> findHoiDongsByKhoa_Id(Integer khoaId);
    List<HoiDong> findHoiDongsByKhoa_IdAndIsOpenTrue(Integer khoaId);
}
