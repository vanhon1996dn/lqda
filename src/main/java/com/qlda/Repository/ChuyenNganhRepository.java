package com.qlda.Repository;

import com.qlda.Entity.ChuyenNganh;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChuyenNganhRepository extends CrudRepository<ChuyenNganh,Integer> {
    ChuyenNganh findChuyenNganhByTenCN(String tenCN);
    ChuyenNganh findChuyenNganhById(Integer id);
    List<ChuyenNganh> findAll();
}
