package com.qlda.Repository;

import com.qlda.Entity.GiaoVien;
import com.qlda.Entity.SinhVien;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GiaoVienRepository extends CrudRepository<GiaoVien,Integer> {
    List<GiaoVien> findAll();
    GiaoVien findGiaoVienById(Integer id);
    GiaoVien findGiaoVienByMaGV(Integer maGV);
    List<GiaoVien> findGiaoViensByKhoa_Id(Integer khoaId);
    List<GiaoVien> findGiaoViensByBangCap_IdGreaterThanEqualAndKhoa_Id(Integer bangCapId,Integer khoaID);

}

