package com.qlda.Repository;

import com.qlda.Entity.Khoa;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

public interface KhoaRepository extends CrudRepository<Khoa,Integer> {
   List<Khoa> findAll();
    Khoa findKhoaById(Integer id);

    Khoa findKhoaByMaKhoa(String maKhoa);
}
