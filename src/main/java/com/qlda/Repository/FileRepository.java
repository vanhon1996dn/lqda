package com.qlda.Repository;

import com.qlda.Entity.File;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileRepository extends CrudRepository<File,Integer> {
    List<File> findAll();
    File findFileByTenFile(String tenFile);
    File findFileById(Integer id);
}
