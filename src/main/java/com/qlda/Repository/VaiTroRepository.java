package com.qlda.Repository;

import com.qlda.Entity.VaiTro;
import com.qlda.Entity.VaiTro;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VaiTroRepository extends CrudRepository<VaiTro,Integer> {
    List<VaiTro> findAll();
    VaiTro findVaiTroById(Integer id);
}
