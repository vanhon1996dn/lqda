package com.qlda.Repository;

import com.qlda.Entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    UserEntity findUserEntityById(int id);
    UserEntity findUserEntityByUserName(String name);
    UserEntity findByUserName(String userName);
    List<UserEntity> findAll();
}
