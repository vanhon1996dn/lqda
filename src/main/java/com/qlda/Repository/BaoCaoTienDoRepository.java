package com.qlda.Repository;

import com.qlda.Entity.BaoCaoTienDo;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BaoCaoTienDoRepository extends CrudRepository<BaoCaoTienDo,Integer> {
    BaoCaoTienDo findBaoCaoTienDoById(Integer id);
    List<BaoCaoTienDo> findAll();
}
