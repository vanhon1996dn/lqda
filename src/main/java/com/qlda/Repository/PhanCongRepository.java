package com.qlda.Repository;

import com.qlda.Entity.PhanCong;
import com.qlda.Entity.PhanCong;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PhanCongRepository extends CrudRepository<PhanCong,Integer> {
    List<PhanCong> findAll();
    List<PhanCong> findPhanCongsByDoAn_MaDoAn(Integer maDoAn);
    PhanCong findPhanCongById(Integer id);
    List<PhanCong> findPhanCongsByDotBaoCao_Id(Integer id);
    List<PhanCong> findPhanCongsByDotBaoCao_IdAndGiaoVien_MaGV(Integer id,Integer maGV);
}
