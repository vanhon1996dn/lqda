package com.qlda.Repository;

import com.qlda.Entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface RoleRepository extends CrudRepository<Role,Integer> {
    Role findById(int roleId);
}
