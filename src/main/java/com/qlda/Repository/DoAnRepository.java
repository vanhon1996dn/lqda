package com.qlda.Repository;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.DoAn;
import com.qlda.Entity.GiaoVien;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface DoAnRepository  extends CrudRepository<DoAn,Integer> {
    DoAn findDoAnById(Integer id);
    DoAn findDoAnByMaDoAn(Integer maDoAn);
    List<DoAn> findAll();
    List<DoAn> findDoAnsByOrderByCreateDateDesc();
    List<DoAn> findDoAnsByIsOpenTrue();
    List<DoAn> findDoAnsByCreateDateGreaterThanEqualAndIsOpenTrue(Date date);
}
