package com.qlda.Repository;

import com.qlda.Entity.RoleUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface RoleUserRepository extends CrudRepository<RoleUser, Integer> {

    @Query(value = "select ru.* from app_role_user ru where user_id = ?1",nativeQuery = true)
    List<RoleUser> findRoleUserByUserId(int userId);

}
