package com.qlda.Repository;

import com.qlda.Entity.ChucVu;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ChucVuRepository extends CrudRepository<ChucVu, Integer> {
    ChucVu findChucVuByTenCV(String tenCV);
    ChucVu findChucVuById(Integer id);
    List<ChucVu> findAll();
}
