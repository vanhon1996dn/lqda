package com.qlda.Repository;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.DoAnDK;
import com.qlda.Entity.GiaoVien;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DoAnDKRepository extends CrudRepository<DoAnDK,Integer> {
    DoAnDK findDoAnDKByGiaoVien(GiaoVien giaoVien);
    DoAnDK findDoAnDKById(Integer id);
    DoAnDK findDoAnDKByMaDoAn(Integer maDoAn);
    List<DoAnDK> findDoAnDKSByIsOpenTrueAndStatusFalseOrderByCreateDate();
    List<DoAnDK> findAll();
}
