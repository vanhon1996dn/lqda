package com.qlda.Repository;

import com.qlda.Entity.DotBaoCao;
import com.qlda.Entity.DotBaoCao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DotBaoCaoRepository extends CrudRepository<DotBaoCao,Integer> {
    DotBaoCao findDotBaoCaoById(Integer id);
    List<DotBaoCao> findDotBaoCaosByDoAn_MaDoAn(Integer maDoAn);
    DotBaoCao findDotBaoCaoByDoAn_MaDoAnAndBaoCaoTienDo_Id(Integer maDoAn,Integer BaoCaoTienDoId);
    List<DotBaoCao> findAll();
    List<DotBaoCao> findDotBaoCaosByOrderByThoiGianDesc();
}
