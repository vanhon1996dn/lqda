package com.qlda.Repository;

import com.qlda.Entity.BangCap;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BangCapRepository extends CrudRepository<BangCap, Integer> {
    BangCap findBangCapByTenBC(String tenBV);
    BangCap findBangCapById(Integer id);
    BangCap findBangCapByMaBC(String maBC);
    List<BangCap> findAll();
}

