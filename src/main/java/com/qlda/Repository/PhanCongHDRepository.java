package com.qlda.Repository;

import com.qlda.Entity.PhanCongHD;
import com.qlda.Entity.PhanCongHD;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface PhanCongHDRepository extends CrudRepository<PhanCongHD,Integer> {
    List<PhanCongHD> findAll();
    PhanCongHD findPhanCongHDById(Integer id);
    List<PhanCongHD> findPhanCongHDSByHoiDong_StartedDateGreaterThanAndHoiDong_KhoaId(Date date,Integer khoaId);
    PhanCongHD findPhanCongHDByHoiDong_MaHDAndAndVaiTroHD_Id(Integer maHD,Integer vaiTroHDId);
}
