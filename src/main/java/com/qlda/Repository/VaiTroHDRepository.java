package com.qlda.Repository;

import com.qlda.Entity.VaiTroHD;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VaiTroHDRepository extends CrudRepository<VaiTroHD,Integer> {
    List<VaiTroHD> findAll();
    VaiTroHD findVaiTroHDById(Integer id);
}