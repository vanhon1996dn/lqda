package com.qlda.Repository;

import com.qlda.Entity.File;
import com.qlda.Entity.FileOfDotBaoCao;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FileOfDotBaoCaoRepository extends CrudRepository<FileOfDotBaoCao,Integer> {

}
