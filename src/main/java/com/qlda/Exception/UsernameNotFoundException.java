package com.qlda.Exception;

public class UsernameNotFoundException {
    private String errorMessage;

    public UsernameNotFoundException(){
        super();
    }

    public UsernameNotFoundException(String errorMessage){
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
