package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenFile;
    private String loai;
    @OneToMany(mappedBy = "file", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<FileOfDoAn> fileOfDoAns;
    @OneToMany(mappedBy = "file", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<FileOfDotBaoCao> fileOfDotBaoCaos;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenFile() {
        return tenFile;
    }

    public void setTenFile(String tenFile) {
        this.tenFile = tenFile;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public List<FileOfDoAn> getFileOfDoAns() {
        return fileOfDoAns;
    }

    public void setFileOfDoAns(List<FileOfDoAn> fileOfDoAns) {
        this.fileOfDoAns = fileOfDoAns;
    }

    public List<FileOfDotBaoCao> getFileOfDotBaoCaos() {
        return fileOfDotBaoCaos;
    }

    public void setFileOfDotBaoCaos(List<FileOfDotBaoCao> fileOfDotBaoCaos) {
        this.fileOfDotBaoCaos = fileOfDotBaoCaos;
    }
}
