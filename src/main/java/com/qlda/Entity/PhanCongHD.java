package com.qlda.Entity;

import javax.persistence.*;
@Entity
public class PhanCongHD {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "vaiTroHD", nullable = false)
    private VaiTroHD vaiTroHD;
    @ManyToOne
    @JoinColumn(name = "giaoVien", nullable = false)
    private GiaoVien giaoVien;
    @ManyToOne
    @JoinColumn(name = "hoiDong", nullable = false)
    private HoiDong hoiDong;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public GiaoVien getGiaoVien() {
        return giaoVien;
    }

    public void setGiaoVien(GiaoVien giaoVien) {
        this.giaoVien = giaoVien;
    }

    public VaiTroHD getVaiTroHD() {
        return vaiTroHD;
    }

    public void setVaiTroHD(VaiTroHD vaiTroHD) {
        this.vaiTroHD = vaiTroHD;
    }

    public HoiDong getHoiDong() {
        return hoiDong;
    }

    public void setHoiDong(HoiDong hoiDong) {
        this.hoiDong = hoiDong;
    }
}
