package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class DotBaoCao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name ="baoCaoTienDo",nullable = false)
    private BaoCaoTienDo baoCaoTienDo;
    private String moTa;
    @ManyToOne
    @JoinColumn(name="doAn" ,nullable = false)
    private DoAn doAn;
    private Date thoiGian;
    private Boolean status;
    @OneToMany(mappedBy = "dotBaoCao", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<FileOfDotBaoCao> fileOfDotBaoCaos;

    @OneToMany(mappedBy = "dotBaoCao", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<PhanCong> phanCongs;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(Date thoiGian) {
        this.thoiGian = thoiGian;
    }

    public BaoCaoTienDo getBaoCaoTienDo() {
        return baoCaoTienDo;
    }

    public void setBaoCaoTienDo(BaoCaoTienDo baoCaoTienDo) {
        this.baoCaoTienDo = baoCaoTienDo;
    }

    public DoAn getDoAn() {
        return doAn;
    }

    public void setDoAn(DoAn doAn) {
        this.doAn = doAn;
    }

    public List<FileOfDotBaoCao> getFileOfDotBaoCaos() {
        return fileOfDotBaoCaos;
    }

    public void setFileOfDotBaoCaos(List<FileOfDotBaoCao> fileOfDotBaoCaos) {
        this.fileOfDotBaoCaos = fileOfDotBaoCaos;
    }

    public List<PhanCong> getPhanCongs() {
        return phanCongs;
    }

    public void setPhanCongs(List<PhanCong> phanCongs) {
        this.phanCongs = phanCongs;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }
}
