package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
@Entity
public class VaiTro {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenVaiTro;
    @OneToMany(mappedBy = "vaiTro", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<PhanCong> phanCongs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenVaiTro() {
        return tenVaiTro;
    }

    public void setTenVaiTro(String tenVaiTro) {
        this.tenVaiTro = tenVaiTro;
    }

    public List<PhanCong> getPhanCongs() {
        return phanCongs;
    }

    public void setPhanCongs(List<PhanCong> phanCongs) {
        this.phanCongs = phanCongs;
    }
}
