package com.qlda.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Entity
public class HoiDong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer maHD;
    private String tenHD;
    @ManyToOne
    @JoinColumn(name = "khoa", nullable = false)
    private Khoa khoa;
    private Boolean isOpen;
    private String diaDiem;
    private Date startedDate;
    private Date finishedDate;
    @OneToMany(mappedBy = "hoiDong",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<DoAn> doAns;
    @OneToMany(mappedBy = "hoiDong",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<PhanCongHD> phanCongHDS;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaHD() {
        return maHD;
    }

    public void setMaHD(Integer maHD) {
        this.maHD = maHD;
    }

    public String getTenHD() {
        return tenHD;
    }

    public void setTenHD(String tenHD) {
        this.tenHD = tenHD;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    public Date getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    public List<DoAn> getDoAns() {
        return doAns;
    }

    public void setDoAns(List<DoAn> doAns) {
        this.doAns = doAns;
    }

    public Khoa getKhoa() {
        return khoa;
    }

    public void setKhoa(Khoa khoa) {
        this.khoa = khoa;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public List<PhanCongHD> getPhanCongHDS() {
        return phanCongHDS;
    }

    public void setPhanCongHDS(List<PhanCongHD> phanCongHDS) {
        this.phanCongHDS = phanCongHDS;
    }
}
