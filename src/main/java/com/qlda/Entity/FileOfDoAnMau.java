package com.qlda.Entity;

import javax.persistence.*;
@Entity
public class FileOfDoAnMau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenFile;
    private String loai;
    @ManyToOne
    @JoinColumn(name = "doAnMau", nullable = false)
    private DoAnMau doAnMau;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenFile() {
        return tenFile;
    }

    public void setTenFile(String tenFile) {
        this.tenFile = tenFile;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public DoAnMau getDoAnMau() {
        return doAnMau;
    }

    public void setDoAnMau(DoAnMau doAnMau) {
        this.doAnMau = doAnMau;
    }
}
