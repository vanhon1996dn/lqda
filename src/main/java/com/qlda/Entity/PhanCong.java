package com.qlda.Entity;

import javax.persistence.*;

@Entity
public class PhanCong {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "vaiTro", nullable = false)
    private VaiTro vaiTro;
    @ManyToOne
    @JoinColumn(name = "giaoVien", nullable = false)
    private GiaoVien giaoVien;
    @ManyToOne
    @JoinColumn(name = "doAn", nullable = false)
    private DoAn doAn;
    @ManyToOne
    @JoinColumn(name = "dotBaoCao")
    private DotBaoCao dotBaoCao;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public VaiTro getVaiTro() {
        return vaiTro;
    }

    public void setVaiTro(VaiTro vaiTro) {
        this.vaiTro = vaiTro;
    }

    public GiaoVien getGiaoVien() {
        return giaoVien;
    }

    public void setGiaoVien(GiaoVien giaoVien) {
        this.giaoVien = giaoVien;
    }

    public DoAn getDoAn() {
        return doAn;
    }

    public void setDoAn(DoAn doAn) {
        this.doAn = doAn;
    }

    public DotBaoCao getDotBaoCao() {
        return dotBaoCao;
    }

    public void setDotBaoCao(DotBaoCao dotBaoCao) {
        this.dotBaoCao = dotBaoCao;
    }
}
