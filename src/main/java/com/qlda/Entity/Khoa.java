package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Max;
import java.util.List;

@Entity
public class Khoa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String maKhoa;
    private String tenKhoa;
    @OneToMany(mappedBy = "khoa", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<SinhVien> sinhVien;
    @OneToMany(mappedBy = "khoa", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<GiaoVien> giaoVien;
    @OneToMany(mappedBy = "khoa", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<HoiDong> hoiDongs;
    @OneToMany(mappedBy = "khoa", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<ChuyenNganh> chuyenNganhs;

    public String getMaKhoa() {
        return maKhoa;
    }

    public void setMaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenKhoa() {
        return tenKhoa;
    }

    public void setTenKhoa(String tenKhoa) {
        this.tenKhoa = tenKhoa;
    }

    public List<SinhVien> getSinhVien() {
        return sinhVien;
    }

    public void setSinhVien(List<SinhVien> sinhVien) {
        this.sinhVien = sinhVien;
    }

    public List<GiaoVien> getGiaoVien() {
        return giaoVien;
    }

    public void setGiaoVien(List<GiaoVien> giaoVien) {
        this.giaoVien = giaoVien;
    }

    public List<ChuyenNganh> getChuyenNganhs() {
        return chuyenNganhs;
    }

    public void setChuyenNganhs(List<ChuyenNganh> chuyenNganhs) {
        this.chuyenNganhs = chuyenNganhs;
    }

    public List<HoiDong> getHoiDongs() {
        return hoiDongs;
    }

    public void setHoiDongs(List<HoiDong> hoiDongs) {
        this.hoiDongs = hoiDongs;
    }
}
