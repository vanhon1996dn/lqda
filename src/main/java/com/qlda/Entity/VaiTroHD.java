package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
@Entity
public class VaiTroHD {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenVaiTro;
    @ManyToOne
    @JoinColumn(name = "dkBangCap")
    private BangCap dkBangCap;
    @OneToMany(mappedBy = "vaiTroHD", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<PhanCongHD> phanCongs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenVaiTro() {
        return tenVaiTro;
    }

    public void setTenVaiTro(String tenVaiTro) {
        this.tenVaiTro = tenVaiTro;
    }

    public List<PhanCongHD> getPhanCongs() {
        return phanCongs;
    }

    public void setPhanCongs(List<PhanCongHD> phanCongs) {
        this.phanCongs = phanCongs;
    }

    public BangCap getDkBangCap() {
        return dkBangCap;
    }

    public void setDkBangCap(BangCap dkBangCap) {
        this.dkBangCap = dkBangCap;
    }
}
