package com.qlda.Entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Entity
public class SinhVien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer maSV;
    private String tenSV;
    private String lop;
    private Date ngaySinh;
    private boolean isCheck;
    @ManyToOne
    @JoinColumn(name = "khoa_id", nullable = false)
    private Khoa khoa;
    @ManyToOne
    @JoinColumn(name = "chuyenNganh", nullable = false)
    private ChuyenNganh chuyenNganh;
    @ManyToOne
    @JoinColumn(name = "doAnDK")
    private DoAnDK doAnDK;
    @ManyToOne
    @JoinColumn(name = "doAn")
    private DoAn doAn;

    public Integer getMaSV() {
        return maSV;
    }

    public void setMaSV(Integer maSV) {
        this.maSV = maSV;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }


    public Khoa getKhoa() {
        return khoa;
    }

    public void setKhoa(Khoa khoa) {
        this.khoa = khoa;
    }

    public ChuyenNganh getChuyenNganh() {
        return chuyenNganh;
    }

    public void setChuyenNganh(ChuyenNganh chuyenNganh) {
        this.chuyenNganh = chuyenNganh;
    }

    public DoAnDK getDoAnDK() {
        return doAnDK;
    }

    public void setDoAnDK(DoAnDK doAnDK) {
        this.doAnDK = doAnDK;
    }

    public DoAn getDoAn() {
        return doAn;
    }

    public void setDoAn(DoAn doAn) {
        this.doAn = doAn;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }
}
