package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
public class ChucVu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String tenCV;
    @OneToMany(mappedBy = "chucVu",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    private List<GiaoVien> giaoViens;
    private Integer diem;

    public Integer getDiem() {
        return diem;
    }

    public void setDiem(Integer diem) {
        this.diem = diem;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenCV() {
        return tenCV;
    }

    public void setTenCV(String tenCV) {
        this.tenCV = tenCV;
    }

    public List<GiaoVien> getGiaoViens() {
        return giaoViens;
    }

    public void setGiaoViens(List<GiaoVien> giaoViens) {
        this.giaoViens = giaoViens;
    }
}
