package com.qlda.Entity;

import javax.persistence.*;
@Entity
public class FileOfDotBaoCao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "file", nullable = false)
    private File file;

    @ManyToOne
    @JoinColumn(name = "dotBaoCao", nullable = false)
    private DotBaoCao dotBaoCao;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }


    public DotBaoCao getDotBaoCao() {
        return dotBaoCao;
    }

    public void setDotBaoCao(DotBaoCao dotBaoCao) {
        this.dotBaoCao = dotBaoCao;
    }
}
