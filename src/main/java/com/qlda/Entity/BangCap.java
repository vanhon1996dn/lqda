package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;
@Entity
public class BangCap {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String maBC;
    private String tenBC;
    @OneToMany(mappedBy = "bangCap",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    private List<GiaoVien> giaoViens;
    private Integer diem;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMaBC() {
        return maBC;
    }

    public void setMaBC(String maBC) {
        this.maBC = maBC;
    }

    public String getTenBC() {
        return tenBC;
    }

    public void setTenBC(String tenBC) {
        this.tenBC = tenBC;
    }

    public List<GiaoVien> getGiaoViens() {
        return giaoViens;
    }

    public void setGiaoViens(List<GiaoVien> giaoViens) {
        this.giaoViens = giaoViens;
    }

    public Integer getDiem() {
        return diem;
    }

    public void setDiem(Integer diem) {
        this.diem = diem;
    }
}
