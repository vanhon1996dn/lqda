package com.qlda.Entity;

import javax.persistence.*;
@Entity
public class FileOfDoAn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "file", nullable = false)
    private File file;
    @ManyToOne
    @JoinColumn(name = "doAn", nullable = false)
    private DoAn doAn;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public DoAn getDoAn() {
        return doAn;
    }

    public void setDoAn(DoAn doAn) {
        this.doAn = doAn;
    }
}
