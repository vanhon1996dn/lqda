package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Entity
public class GiaoVien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer maGV;
    private String tenGV;
    private Date ngaySinh;
    private Integer sinhVienToiDa;
    @OneToMany(mappedBy = "giaoVien", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<DoAnDK>  doAnDK;
    @OneToMany(mappedBy = "giaoVien", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private  List<PhanCong> phanCong;
    @OneToMany(mappedBy = "giaoVien", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private  List<PhanCongHD> phanCongHD;
    @ManyToOne
    @JoinColumn(name = "chucVu",nullable = false)
    private ChucVu chucVu;
    @ManyToOne
    @JoinColumn(name = "bangCap",nullable = false)
    private BangCap bangCap;
    @ManyToOne
    @JoinColumn(name = "khoa", nullable = false)
    private Khoa khoa;
    @ManyToOne
    @JoinColumn(name = "chuyenNganh", nullable = false)
    private ChuyenNganh chuyenNganh;
    @OneToMany(mappedBy = "giaoVien", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<DoAnMau> doAnMaus;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenGV() {
        return tenGV;
    }

    public void setTenGV(String tenGV) {
        this.tenGV = tenGV;
    }

    public ChucVu getChucVu() {
        return chucVu;
    }

    public void setChucVu(ChucVu chucVu) {
        this.chucVu = chucVu;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public Khoa getKhoa() {
        return khoa;
    }

    public void setKhoa(Khoa khoa) {
        this.khoa = khoa;
    }

    public ChuyenNganh getChuyenNganh() {
        return chuyenNganh;
    }

    public void setChuyenNganh(ChuyenNganh chuyenNganh) {
        this.chuyenNganh = chuyenNganh;
    }

    public Integer getMaGV() {
        return maGV;
    }

    public void setMaGV(Integer maGV) {
        this.maGV = maGV;
    }

    public List<DoAnMau> getDoAnMaus() {
        return doAnMaus;
    }

    public void setDoAnMaus(List<DoAnMau> doAnMaus) {
        this.doAnMaus = doAnMaus;
    }

    public Integer getSinhVienToiDa() {
        return sinhVienToiDa;
    }

    public void setSinhVienToiDa(Integer sinhVienToiDa) {
        this.sinhVienToiDa = sinhVienToiDa;
    }

    public List<DoAnDK> getDoAnDK() {
        return doAnDK;
    }

    public void setDoAnDK(List<DoAnDK> doAnDK) {
        this.doAnDK = doAnDK;
    }

    public List<PhanCong> getPhanCong() {
        return phanCong;
    }

    public void setPhanCong(List<PhanCong> phanCong) {
        this.phanCong = phanCong;
    }

    public BangCap getBangCap() {
        return bangCap;
    }

    public void setBangCap(BangCap bangCap) {
        this.bangCap = bangCap;
    }

    public List<PhanCongHD> getPhanCongHD() {
        return phanCongHD;
    }

    public void setPhanCongHD(List<PhanCongHD> phanCongHD) {
        this.phanCongHD = phanCongHD;
    }
}
