package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
@Entity
@Table(name ="DoAn")
public class DoAn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer maDoAn;
    private String tieuDe;
    private String moTa;
    private Date createDate;
    private Date updateDate;
    private Boolean status;
    private Boolean isOpen;
    private String image;
    @OneToMany(mappedBy = "doAn", fetch = FetchType.LAZY,cascade=CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<FileOfDoAn> fileOfDoAns;
    @OneToMany(mappedBy = "doAn",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<DotBaoCao> dotBaoCaos;
    @OneToMany(mappedBy = "doAn",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<PhanCong> phanCongs;
    @OneToMany(mappedBy = "doAn",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<SinhVien> sinhViens;
    @ManyToOne
    @JoinColumn(name = "hoiDong")
    private HoiDong hoiDong;

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public List<FileOfDoAn> getFileOfDoAns() {
        return fileOfDoAns;
    }

    public void setFileOfDoAns(List<FileOfDoAn> fileOfDoAns) {
        this.fileOfDoAns = fileOfDoAns;
    }

    public List<DotBaoCao> getDotBaoCaos() {
        return dotBaoCaos;
    }

    public void setDotBaoCaos(List<DotBaoCao> dotBaoCaos) {
        this.dotBaoCaos = dotBaoCaos;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<PhanCong> getPhanCongs() {
        return phanCongs;
    }

    public void setPhanCongs(List<PhanCong> phanCongs) {
        this.phanCongs = phanCongs;
    }

    public List<SinhVien> getSinhViens() {
        return sinhViens;
    }

    public void setSinhViens(List<SinhVien> sinhViens) {
        this.sinhViens = sinhViens;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public HoiDong getHoiDong() {
        return hoiDong;
    }

    public void setHoiDong(HoiDong hoiDong) {
        this.hoiDong = hoiDong;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }
}

