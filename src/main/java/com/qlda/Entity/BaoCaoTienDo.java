package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class BaoCaoTienDo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @OneToMany(mappedBy = "baoCaoTienDo",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(value = FetchMode.SELECT)
    private List<DotBaoCao> dotBaoCaos;
    private String moTa;
    private Date    thoiGian;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<DotBaoCao> getDotBaoCaos() {
        return dotBaoCaos;
    }

    public void setDotBaoCaos(List<DotBaoCao> dotBaoCaos) {
        this.dotBaoCaos = dotBaoCaos;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(Date thoiGian) {
        this.thoiGian = thoiGian;
    }
}
