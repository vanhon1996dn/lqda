package com.qlda.Entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class DoAnMau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer maDoAn;
    private String tieuDe;
    private String moTa;
    private Date createDate;
    private Date updateDate;
    private Boolean status;
    private String image;
    @ManyToOne
    @JoinColumn(name = "maGV", nullable = false)
    private GiaoVien giaoVien;
    @OneToMany(mappedBy = "doAnMau",fetch =FetchType.LAZY,cascade = CascadeType.ALL)
    @Fetch(FetchMode.SELECT)
    private List<FileOfDoAnMau> fileOfDoAnMaus;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public GiaoVien getGiaoVien() {
        return giaoVien;
    }

    public void setGiaoVien(GiaoVien giaoVien) {
        this.giaoVien = giaoVien;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<FileOfDoAnMau> getFileOfDoAnMaus() {
        return fileOfDoAnMaus;
    }

    public void setFileOfDoAnMaus(List<FileOfDoAnMau> fileOfDoAnMaus) {
        this.fileOfDoAnMaus = fileOfDoAnMaus;
    }
}
