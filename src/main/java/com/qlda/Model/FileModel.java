package com.qlda.Model;

public class FileModel {
    private Integer id;
    private String tenFile;
    private String loai;
    private Integer maDoAn;
    private Integer dotBaoCaoId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenFile() {
        return tenFile;
    }

    public void setTenFile(String tenFile) {
        this.tenFile = tenFile;
    }

    public String getLoai() {
        return loai;
    }

    public void setLoai(String loai) {
        this.loai = loai;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public Integer getDotBaoCaoId() {
        return dotBaoCaoId;
    }

    public void setDotBaoCaoId(Integer dotBaoCaoId) {
        this.dotBaoCaoId = dotBaoCaoId;
    }
}
