package com.qlda.Model;

import java.util.List;

public class AutoPCDoAnChoHD {
    private Integer maDoAn;
    private List<Integer> maHDs;
    private Integer maHDPC;
    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public List<Integer> getMaHDs() {
        return maHDs;
    }

    public void setMaHDs(List<Integer> maHDs) {
        this.maHDs = maHDs;
    }

    public Integer getMaHDPC() {
        return maHDPC;
    }

    public void setMaHDPC(Integer maHDPC) {
        this.maHDPC = maHDPC;
    }
}
