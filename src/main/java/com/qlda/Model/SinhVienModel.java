package com.qlda.Model;

import java.util.Date;

public class SinhVienModel {
    private Integer id;
    private Integer maSV;
    private String tenSV;
    private String lop;
    private Date ngaySinh;
    private Integer khoaId;
    private Integer chuyenNganhId;
    private Boolean isCheck;

    public Integer getMaSV() {
        return maSV;
    }

    public void setMaSV(Integer maSV) {
        this.maSV = maSV;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenSV() {
        return tenSV;
    }

    public void setTenSV(String tenSV) {
        this.tenSV = tenSV;
    }

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }


    public Integer getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public Integer getChuyenNganhId() {
        return chuyenNganhId;
    }

    public void setChuyenNganhId(Integer chuyenNganhId) {
        this.chuyenNganhId = chuyenNganhId;
    }

    public Boolean isCheck() {
        return isCheck;
    }

    public void setCheck(Boolean check) {
        isCheck = check;
    }
}

