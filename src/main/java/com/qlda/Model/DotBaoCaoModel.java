package com.qlda.Model;


import com.qlda.Entity.GiaoVien;

import java.util.Date;
import java.util.List;

public class DotBaoCaoModel {
    private Integer id;
    private Integer maDoAn;
    private List<Integer> fileIds;
    private Integer baoCaoTienDoId;
    private String moTa;
    private Integer maGV;
    private Integer vaiTroId;
    private Date thoiGian;
    private Boolean status;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }


    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getThoiGian() {
        return thoiGian;
    }

    public void setThoiGian(Date thoiGian) {
        this.thoiGian = thoiGian;
    }

    public List<Integer> getFileIds() {
        return fileIds;
    }

    public void setFileIds(List<Integer> fileIds) {
        this.fileIds = fileIds;
    }

    public Integer getBaoCaoTienDoId() {
        return baoCaoTienDoId;
    }

    public void setBaoCaoTienDoId(Integer baoCaoTienDoId) {
        this.baoCaoTienDoId = baoCaoTienDoId;
    }

    public Integer getMaGV() {
        return maGV;
    }

    public void setMaGV(Integer maGV) {
        this.maGV = maGV;
    }

    public Integer getVaiTroId() {
        return vaiTroId;
    }

    public void setVaiTroId(Integer vaiTroId) {
        this.vaiTroId = vaiTroId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
