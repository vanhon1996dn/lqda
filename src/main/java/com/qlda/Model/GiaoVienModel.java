package com.qlda.Model;

import java.util.Date;

public class GiaoVienModel {
    private Integer id;
    private Integer maGV;
    private String tenGV;
    private Integer chucVuId;
    private Integer bangCapId;
    private Date ngaySinh;
    private Integer khoaId;
    private Integer chuyenNganhId;
    private Integer sinhVienToiDa;

    public Integer getMaGV() {
        return maGV;
    }

    public void setMaGV(Integer maGV) {
        this.maGV = maGV;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenGV() {
        return tenGV;
    }

    public void setTenGV(String tenGV) {
        this.tenGV = tenGV;
    }

    public Date getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(Date ngaySinh) {
        this.ngaySinh = ngaySinh;
    }



    public Integer getSinhVienToiDa() {
        return sinhVienToiDa;
    }

    public void setSinhVienToiDa(Integer sinhVienToiDa) {
        this.sinhVienToiDa = sinhVienToiDa;
    }

    public Integer getChucVuId() {
        return chucVuId;
    }

    public void setChucVuId(Integer chucVuId) {
        this.chucVuId = chucVuId;
    }

    public Integer getBangCapId() {
        return bangCapId;
    }

    public void setBangCapId(Integer bangCapId) {
        this.bangCapId = bangCapId;
    }

    public Integer getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public Integer getChuyenNganhId() {
        return chuyenNganhId;
    }

    public void setChuyenNganhId(Integer chuyenNganhId) {
        this.chuyenNganhId = chuyenNganhId;
    }
}
