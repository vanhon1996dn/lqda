package com.qlda.Model;

public class VaiTroModel {
    private Integer id;
    private String tenVaiTro;
    private Integer dKBangCapId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenVaiTro() {
        return tenVaiTro;
    }

    public void setTenVaiTro(String tenVaiTro) {
        this.tenVaiTro = tenVaiTro;
    }

    public Integer getdKBangCapId() {
        return dKBangCapId;
    }

    public void setdKBangCapId(Integer dKBangCapId) {
        this.dKBangCapId = dKBangCapId;
    }
}
