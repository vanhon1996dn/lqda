package com.qlda.Model;

import com.qlda.Entity.GiaoVien;
import com.qlda.Entity.SinhVien;

import java.util.Date;
import java.util.List;

public class DoAnModel {
    private Integer id;
    private Integer maDoAn;
    private String tieuDe;
    private String moTa;
    private Date CreateDate;
    private Date UpdateDate;
    private Boolean status;
    private Boolean isOpen;
    private String image;
    private Integer khoaId;
    private Integer chuyenNganhId;
    private List<Integer> fileIds;
    private List<Integer> dotBaoCaoIds;
    private List<Integer> maGVs;
    private List<Integer> maSVs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getTieuDe() {
        return tieuDe;
    }

    public void setTieuDe(String tieuDe) {
        this.tieuDe = tieuDe;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Date getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(Date createDate) {
        CreateDate = createDate;
    }

    public Date getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(Date updateDate) {
        UpdateDate = updateDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public Integer getChuyenNganhId() {
        return chuyenNganhId;
    }

    public void setChuyenNganhId(Integer chuyenNganhId) {
        this.chuyenNganhId = chuyenNganhId;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public List<Integer> getFileIds() {
        return fileIds;
    }

    public void setFileIds(List<Integer> fileIds) {
        this.fileIds = fileIds;
    }

    public List<Integer> getDotBaoCaoIds() {
        return dotBaoCaoIds;
    }

    public void setDotBaoCaoIds(List<Integer> dotBaoCaoIds) {
        this.dotBaoCaoIds = dotBaoCaoIds;
    }

    public List<Integer> getMaGVs() {
        return maGVs;
    }

    public void setMaGVs(List<Integer> maGVs) {
        this.maGVs = maGVs;
    }

    public List<Integer> getMaSVs() {
        return maSVs;
    }

    public void setMaSVs(List<Integer> maSVs) {
        this.maSVs = maSVs;
    }
}
