package com.qlda.Model;

public class PhanCongHDModel {
    private Integer id;
    private Integer maGV;
    private String tenGV;
    private Integer maHoiDong;
    private Integer vaiTroHDID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaGV() {
        return maGV;
    }

    public void setMaGV(Integer maGV) {
        this.maGV = maGV;
    }

    public String getTenGV() {
        return tenGV;
    }

    public void setTenGV(String tenGV) {
        this.tenGV = tenGV;
    }

    public Integer getMaHoiDong() {
        return maHoiDong;
    }

    public void setMaHoiDong(Integer maHoiDong) {
        this.maHoiDong = maHoiDong;
    }

    public Integer getVaiTroHDID() {
        return vaiTroHDID;
    }

    public void setVaiTroHDID(Integer vaiTroHDID) {
        this.vaiTroHDID = vaiTroHDID;
    }
}
