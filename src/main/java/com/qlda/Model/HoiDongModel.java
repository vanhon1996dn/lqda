package com.qlda.Model;

import java.util.Date;

public class HoiDongModel {
    private Integer id;
    private Integer maHD;
    private String tenHD;
    private Integer khoaId;
    private String diaDiem;
    private Date startedDate;
    private Date finishedDate;
    private Boolean checkTrung;
    private Boolean isOpen;
    private Integer soLuongDoAn;
    private Integer soLuongGV;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaHD() {
        return maHD;
    }

    public void setMaHD(Integer maHD) {
        this.maHD = maHD;
    }

    public String getTenHD() {
        return tenHD;
    }

    public void setTenHD(String tenHD) {
        this.tenHD = tenHD;
    }

    public Integer getKhoaId() {
        return khoaId;
    }

    public void setKhoaId(Integer khoaId) {
        this.khoaId = khoaId;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = startedDate;
    }

    public Date getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(Date finishedDate) {
        this.finishedDate = finishedDate;
    }

    public Boolean getCheckTrung() {
        return checkTrung;
    }

    public void setCheckTrung(Boolean checkTrung) {
        this.checkTrung = checkTrung;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }

    public Integer getSoLuongDoAn() {
        return soLuongDoAn;
    }

    public void setSoLuongDoAn(Integer soLuongDoAn) {
        this.soLuongDoAn = soLuongDoAn;
    }

    public Integer getSoLuongGV() {
        return soLuongGV;
    }

    public void setSoLuongGV(Integer soLuongGV) {
        this.soLuongGV = soLuongGV;
    }
}
