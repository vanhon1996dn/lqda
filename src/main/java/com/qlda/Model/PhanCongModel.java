package com.qlda.Model;

public class PhanCongModel {
    private Integer id;
    private Integer maGV;
    private String tenGV;
    private Integer maDoAn;
    private String vaiTro;
    private Integer vaiTroID;
    public Integer getMaGV() {
        return maGV;
    }

    public void setMaGV(Integer maGV) {
        this.maGV = maGV;
    }

    public Integer getVaiTroID() {
        return vaiTroID;
    }

    public void setVaiTroID(Integer vaiTroID) {
        this.vaiTroID = vaiTroID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenGV() {
        return tenGV;
    }

    public void setTenGV(String tenGV) {
        this.tenGV = tenGV;
    }

    public Integer getMaDoAn() {
        return maDoAn;
    }

    public void setMaDoAn(Integer maDoAn) {
        this.maDoAn = maDoAn;
    }

    public String getVaiTro() {
        return vaiTro;
    }

    public void setVaiTro(String vaiTro) {
        this.vaiTro = vaiTro;
    }
}
