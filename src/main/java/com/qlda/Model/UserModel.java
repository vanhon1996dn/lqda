package com.qlda.Model;

public class UserModel {
    private String userName;
    private String password;
    private String passwordCu;
    private String ten;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getPasswordCu() {
        return passwordCu;
    }

    public void setPasswordCu(String passwordCu) {
        this.passwordCu = passwordCu;
    }
}
