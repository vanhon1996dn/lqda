package com.qlda.Service;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.HoiDong;
import com.qlda.Model.HoiDongModel;

import java.util.List;

public interface HoiDongService {
    public List<HoiDongModel> getAllHoiDong();
    public HoiDongModel getHoiDongByID(Integer id);
    public HoiDongModel getHoiDongByMaHD(Integer maHD);
    public HoiDongModel getHoiDongByMaDoAn(Integer MaDoAn);
    public List<HoiDongModel>  getHoiDongsBykhoaId(Integer khoaId);
    public List<HoiDongModel> getHoiDongsByMaGV(Integer maGV);
    public List<HoiDongModel>  getOpenedHoiDongsBykhoaId(Integer khoaId);
    public List<HoiDongModel>  parseEntitiesToModels(List<HoiDong> HoiDongs);
    public HoiDongModel parseEntityToModel(HoiDong HoiDong);
    public HoiDong parseModelToEntity(HoiDongModel HoiDongModel);
    public int createHoiDong(HoiDongModel HoiDongModel);
    public void updateHoiDong(HoiDongModel HoiDongModel);
    public void deleteHoiDong(Integer maHD);
    public List<Integer> getmaHDMaDoAnCoThePhan(DoAn doAn);
    public Boolean checkHDDaPhanCongGVByKhoaId(Integer khoaId);
}
