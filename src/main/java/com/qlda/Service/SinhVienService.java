package com.qlda.Service;

import com.qlda.Entity.SinhVien;
import com.qlda.Model.SinhVienModel;

import java.util.List;

public interface SinhVienService {
    public List<SinhVienModel> getAllSinhVien();
    public SinhVienModel getSinhVienByID(Integer id);
    public SinhVienModel getSinhVienByMaSV(Integer maSV);
    public List<SinhVienModel> getSinhVienModelsByMaDoAn(Integer maDoAn);
    public List<SinhVienModel>  parseEntitiesToModels(List<SinhVien> sinhViens);
    public SinhVienModel parseEntityToModel(SinhVien sinhVien);
    public SinhVien parseModelToEntity(SinhVienModel sinhVienModel);
    public int createSinhVien(SinhVienModel sinhVienModel);
    public void updateSinhVien(SinhVienModel sinhVienModel);
    public void deleteSinhVien(Integer maSV);
}
