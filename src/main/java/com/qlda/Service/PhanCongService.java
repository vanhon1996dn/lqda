package com.qlda.Service;

import com.qlda.Entity.PhanCong;
import com.qlda.Model.PhanCongModel;

import java.util.List;

public interface PhanCongService {
    public List<PhanCongModel> getAllPhanCong();
    public PhanCongModel getPhanCongByID(Integer id);
    public List<PhanCong> getGVHDPhanCongEntitiesByMaDoAN(Integer maDoAn);
    public List<PhanCongModel> getPhanCongsByDotBaoCaoID(Integer DotBaoCaoID);
    public PhanCongModel parseEntityToModel(PhanCong PhanCong);
    public PhanCong parseModelToEntity(PhanCongModel PhanCongModel);
    public boolean AutoPhanCongGV(Integer khoaId);
    public int createPhanCong(PhanCongModel PhanCongModel);
    public int createPhanCongHuongDanByMaGVAndMaDoAN(Integer maGV,Integer maDoAn);
    public void updatePhanCong(PhanCongModel PhanCongModel);
    public void deletePhanCong(Integer id);
    public Boolean checkPhanCongGVHuongDan(PhanCongModel PhanCongModel);
    public Boolean checkDoAnItHon2GVHD(PhanCongModel PhanCongModel);
}
