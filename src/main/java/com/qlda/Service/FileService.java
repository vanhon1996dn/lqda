package com.qlda.Service;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.DotBaoCao;
import com.qlda.Entity.File;
import com.qlda.Model.FileModel;

import java.util.List;

public interface FileService {
    public List<FileModel> getAllFile();
    public FileModel getFileByID(Integer id);
    public List<Integer> getFileIdsByDotBaoCaoId(Integer dotBaoCaoId);
    public List<Integer> getFileIdsByMaDoAnId(Integer doAnId);
    public List<FileModel> parseEntityListToModelList(List<File> files);
    public FileModel parseEntityToModel(File File);
    public File parseModelToEntity(FileModel FileModel);
    public int createFile(FileModel FileModel);
    public void updateFile(FileModel FileModel);
    public void deleteFile(Integer id);
}
