package com.qlda.Service;

import com.qlda.Entity.BaoCaoTienDo;
import com.qlda.Model.BaoCaoTienDoModel;

import java.util.List;

public interface BaoCaoTienDoService {
    public List<BaoCaoTienDoModel> getAllBaoCaoTienDo();
    public BaoCaoTienDoModel getBaoCaoTienDoByID(Integer id);
    public BaoCaoTienDoModel parseEntityToModel(BaoCaoTienDo BaoCaoTienDo);
    public BaoCaoTienDo parseModelToEntity(BaoCaoTienDoModel BaoCaoTienDoModel);
    public int createBaoCaoTienDo(BaoCaoTienDoModel BaoCaoTienDoModel);
    public void updateBaoCaoTienDo(BaoCaoTienDoModel BaoCaoTienDoModel);
    public void deleteBaoCaoTienDo(Integer id);
}
