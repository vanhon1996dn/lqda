package com.qlda.Service;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.DoAnDK;
import com.qlda.Model.DoAnModel;

import java.util.List;

public interface DoAnService {
    public List<DoAnModel> getAllDoAn();
    public DoAnModel getDoAnByID(Integer id);
    public DoAnModel getDoAnByMaDoAn(Integer maDoAn);
    public DoAnModel getDoAnByMaSV(Integer maSV);
    public List<DoAnModel> getDoAnsByMaGV(Integer maGV);
    public DoAnModel parseEntityToModel(DoAn DoAn);
    public List<DoAnModel> getDoAnsByMaHD(Integer maHD);
    public List<DoAnModel>  parseEntitiesToModels(List<DoAn> DoAns);
    public List<DoAnModel>  getDoAnsByKhoaId(Integer khoaId);
    public List<DoAn>  getDoAnsDaPhanHDByKhoaId(Integer khoaId);
    public List<DoAn>  getDoAnsChuaPhanHDByKhoaId(Integer khoaId);
    public Boolean autoPhanCongDoAnChoHD(Integer khoaId);
    public DoAn parseModelToEntity(DoAnModel DoAnModel);
    public DoAn parseDoAnDKToDoAnEntity(DoAnDK doAnDK);
    public int getSoLuongSinhVien(DoAn doAn);
    public int createDoAn(DoAnDK doAnDK);
    public void updateDoAn(DoAnModel DoAnModel);
    public void deleteDoAn(Integer maDoAn);
}
