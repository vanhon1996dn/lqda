package com.qlda.Service;

import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Repository.SinhVienRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Objects;

public class Useful {
    static int numDoAnDK;
    static int numDoAnMau;
    static int numMaSoGV;
    static int numMaSoSV;
    static int numUserName;
    static int numMaSoHD;



    public  static Integer getMaDoAnDK(Integer khoaId){
        if(Objects.isNull(numDoAnDK) ||numDoAnDK==999){
            numDoAnDK=1;
        }
        Integer maso;
        Date date=new Date();
        maso=(1*1000+date.getYear())*100000+khoaId*1000+numDoAnDK;
        numDoAnDK++;
        return maso;
    }
    public static Integer getMaDoAnMau(Integer khoaId){
        if(Objects.isNull(numDoAnMau) ||numDoAnMau==999){
            numDoAnMau=1;
        }
        Integer maso;
        Date date=new Date();
        maso=(2*1000+date.getYear())*100000+khoaId*1000+numDoAnMau;
        numDoAnMau++;
        return maso;
    }
    public static Integer getMaSoGV(Integer khoaId){
        if(Objects.isNull(numMaSoGV) ||numMaSoGV==999){
            numMaSoGV=1;
        }
        Integer maso;
        Date date=new Date();
        maso=(2*1000+date.getYear())*100000+khoaId*1000+ numMaSoGV;
        numMaSoGV++;
        return maso;
    }
    public static Integer getMaSoSV(Integer khoaId){
        if(Objects.isNull(numMaSoSV) ||numMaSoSV==999){
            numMaSoSV=1;
        }
        Integer maso;
        Date date=new Date();
        maso=(1*1000+date.getYear())*100000+khoaId*1000+numMaSoSV;
        numMaSoSV++;
        return maso;
    }
    public static Integer getAutoUserName(){
        if(Objects.isNull(numUserName) ||numUserName==9999){
            numUserName=1;
        }
        Integer maso;
        Date date=new Date();
        Integer year=date.getYear();
        maso=(3*1000+date.getYear())*100000+numUserName;
        numUserName++;
        return maso;
    }
    public static Integer getMaSoHD(Integer khoaId){
        if(Objects.isNull(numMaSoHD)||numUserName==9999){
            numMaSoHD=1;
        }
        Integer maso;
        Date date=new Date();
        maso=(date.getYear())*100000+khoaId*1000+ numMaSoHD;
        numMaSoHD++;
        return maso;
    }

}
