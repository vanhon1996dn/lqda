package com.qlda.Service;

import com.qlda.Entity.UserEntity;
import com.qlda.Model.UserModel;

import java.util.List;

public interface UserService {
    public List<UserModel> getAllUser();
    public UserModel getUserByID(Integer id);
    public UserModel parseEntityToModel(UserEntity User);
    public UserEntity parseModelToEntity(UserModel UserModel);
    public int createUser(UserModel UserModel);
    public int createUserByAdmin(UserModel UserModel);
    public Boolean checkUser(UserModel UserModel);
    public void updateUser(UserModel UserModel);
    public void updateUserByAdmin(UserModel UserModel);
    public void deleteUser(String userName);
    String getUserNameByToken(String token);
    boolean validateExistedUser(String username);
    List<String> getAuthoritiesByToken(String token);
    List<String> getAuthoritiesByUserName(String userName);
}
