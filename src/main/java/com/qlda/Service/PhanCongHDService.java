package com.qlda.Service;

import com.qlda.Entity.PhanCongHD;
import com.qlda.Model.HoiDongModel;
import com.qlda.Model.PhanCongHDModel;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface PhanCongHDService {
    public List<PhanCongHDModel> getAllPhanCongHD();
    public PhanCongHDModel getPhanCongHDByID(Integer id);
    public List<PhanCongHDModel> getPhanCongHDsByMaHD(Integer maHD);
    public boolean autoPhanCongHD(HoiDongModel hoiDongModel);
    public PhanCongHDModel parseEntityToModel(PhanCongHD PhanCongHD);
    public List<PhanCongHDModel>  parseEntitiesToModels(List<PhanCongHD> phanCongHDs);
    public PhanCongHD parseModelToEntity(PhanCongHDModel PhanCongHDModel);
    public int createPhanCongHD(PhanCongHDModel PhanCongHDModel);
    public void updatePhanCongHD(PhanCongHDModel PhanCongHDModel);
    public void deletePhanCongHD(Integer id);
    public void deletePhanCongHDsByMaHD(Integer maHD);
    public Boolean checkPhanCongHD(PhanCongHDModel phanCongHDModel);
    public Boolean checkPhanCongHDUpdate(PhanCongHDModel phanCongHDModel);
}
