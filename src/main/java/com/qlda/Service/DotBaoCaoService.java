package com.qlda.Service;

import com.qlda.Entity.DotBaoCao;
import com.qlda.Model.DotBaoCaoModel;

import java.util.List;

public interface DotBaoCaoService {
    public List<DotBaoCaoModel> getAllDotBaoCao();
    public DotBaoCaoModel getDotBaoCaoByID(Integer id);
    public List<DotBaoCaoModel> getDotBaoCaosByMaDoAn(Integer maDoAn);
    public List<DotBaoCaoModel> getDotBaoCaosByMaGV(Integer maGV);

//    public List<Integer> getDotBaoCaoIdsByMaDoAn(Integer maDoAn);
    public List<DotBaoCaoModel> parseEntitiesToModels(List<DotBaoCao> dotBaoCaos);
    public DotBaoCaoModel parseEntityToModel(DotBaoCao DotBaoCao);
    public DotBaoCao parseModelToEntity(DotBaoCaoModel DotBaoCaoModel);
    public void createAllDotBaoCaosByBaoCaoTienDoId(Integer baoCaoTienDoId);
    public int createDotBaoCao(DotBaoCaoModel DotBaoCaoModel);
    public void updateDotBaoCao(DotBaoCaoModel DotBaoCaoModel);
    public void deleteDotBaoCao(Integer id);
    public void deleteDotBaoCaoByBaoCaoTienDoId(Integer baoCaoTienDoId);
    public Boolean checkDotBaoCao(DotBaoCaoModel dotBaoCaoModel);
}
