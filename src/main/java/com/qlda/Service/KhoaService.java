package com.qlda.Service;

import com.qlda.Entity.Khoa;
import com.qlda.Model.KhoaModel;

import java.util.List;

public interface KhoaService {
    public List<KhoaModel> getAllKhoa();
    public KhoaModel getKhoaByID(Integer id);
    public KhoaModel parseEntityToModel(Khoa Khoa);
    public Khoa parseModelToEntity(KhoaModel KhoaModel);
    public int createKhoa(KhoaModel KhoaModel);
    public void updateKhoa(KhoaModel KhoaModel);
    public void deleteKhoa(Integer id);

}
