package com.qlda.Service;

import com.qlda.Entity.DoAnDK;
import com.qlda.Model.DoAnModel;

import java.util.List;

public interface DoAnDKService {
    public List<DoAnModel> getAllDoAnDK();
    public DoAnModel getDoAnDKByID(Integer id);
    public DoAnModel getDoAnDKByMaDoAn(Integer maDoAn);
    public DoAnModel getDoAnDKByMaSV(Integer maSV);
    public DoAnModel parseEntityToModel(DoAnDK DoAnDK);
    public List<DoAnModel>  parseEntitiesToModels(List<DoAnDK> doAnDKs);
    public DoAnDK parseModelToEntity(DoAnModel DoAnModel);
    public List<DoAnModel>  getDoAnDKsByKhoaId(Integer khoaId);
    public List<DoAnDK>  getDoAnDKEntitysByKhoaId(Integer khoaId);
    public List<DoAnDK>  getDoAnDKEntitysChuaPhanCongByKhoaId(Integer khoaId);
    public int createDoAnDK(DoAnModel DoAnModel);
    public void updateDoAnDK(DoAnModel DoAnModel);
    public void deleteDoAnDK(Integer maDoAn);
    public DoAnDK convertFromDoAnMau(Integer maDoAnMau);
    public Boolean checkDoAnDK(Integer maDoAn,Integer maSV);
}
