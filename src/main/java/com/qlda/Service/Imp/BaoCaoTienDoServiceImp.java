package com.qlda.Service.Imp;

import com.qlda.Entity.BaoCaoTienDo;
import com.qlda.Model.BaoCaoTienDoModel;
import com.qlda.Repository.BaoCaoTienDoRepository;
import com.qlda.Service.BaoCaoTienDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class BaoCaoTienDoServiceImp implements BaoCaoTienDoService {

    @Autowired
    BaoCaoTienDoRepository baoCaoTienDoRepository;

    public List<BaoCaoTienDoModel> getAllBaoCaoTienDo() {
        List<BaoCaoTienDo> BaoCaoTienDos=baoCaoTienDoRepository.findAll();
        List<BaoCaoTienDoModel> BaoCaoTienDoModels=new ArrayList<>();
        for(BaoCaoTienDo BaoCaoTienDo:BaoCaoTienDos){
            BaoCaoTienDoModel BaoCaoTienDoModel=parseEntityToModel(BaoCaoTienDo);
            BaoCaoTienDoModels.add(BaoCaoTienDoModel);
        }
        return BaoCaoTienDoModels;
    }

    @Override
    public BaoCaoTienDoModel getBaoCaoTienDoByID(Integer id) {
        BaoCaoTienDo BaoCaoTienDo=baoCaoTienDoRepository.findBaoCaoTienDoById(id);
        BaoCaoTienDoModel BaoCaoTienDoModel=parseEntityToModel(BaoCaoTienDo);
        return BaoCaoTienDoModel;
    }

    @Override
    public BaoCaoTienDoModel parseEntityToModel(BaoCaoTienDo BaoCaoTienDo) {
        BaoCaoTienDoModel BaoCaoTienDoModel=new BaoCaoTienDoModel();
        BaoCaoTienDoModel.setId(BaoCaoTienDo.getId());
        BaoCaoTienDoModel.setMoTa(BaoCaoTienDo.getMoTa());
        BaoCaoTienDoModel.setThoiGian(BaoCaoTienDo.getThoiGian());
        return BaoCaoTienDoModel;
    }

    @Override
    public BaoCaoTienDo parseModelToEntity(BaoCaoTienDoModel BaoCaoTienDoModel) {
        BaoCaoTienDo BaoCaoTienDo=new BaoCaoTienDo();
        if(Objects.nonNull(BaoCaoTienDoModel.getId())){
            BaoCaoTienDo.setId(BaoCaoTienDoModel.getId());
        }
        BaoCaoTienDo.setMoTa(BaoCaoTienDoModel.getMoTa());
        BaoCaoTienDo.setThoiGian(BaoCaoTienDoModel.getThoiGian());
        return BaoCaoTienDo;
    }

    @Override
    public int createBaoCaoTienDo(BaoCaoTienDoModel BaoCaoTienDoModel) {
        BaoCaoTienDo BaoCaoTienDo=parseModelToEntity(BaoCaoTienDoModel);
        BaoCaoTienDo BaoCaoTienDo2=baoCaoTienDoRepository.save(BaoCaoTienDo);
        return BaoCaoTienDo2.getId();
    }

    @Override
    public void updateBaoCaoTienDo(BaoCaoTienDoModel BaoCaoTienDoModel) {
        BaoCaoTienDo BaoCaoTienDo=parseModelToEntity(BaoCaoTienDoModel);
        baoCaoTienDoRepository.save(BaoCaoTienDo);

    }

    @Override
    public void deleteBaoCaoTienDo(Integer id) {
        BaoCaoTienDo BaoCaoTienDo=baoCaoTienDoRepository.findBaoCaoTienDoById(id);
        baoCaoTienDoRepository.delete(BaoCaoTienDo);
    }
}
