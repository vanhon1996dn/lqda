package com.qlda.Service.Imp;

        import com.qlda.Entity.ChucVu;
        import com.qlda.Model.ChucVuModel;
        import com.qlda.Repository.ChucVuRepository;
        import com.qlda.Service.ChucVuService;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Service;

        import java.util.ArrayList;
        import java.util.List;
        import java.util.Objects;

@Service
public class ChucVuServiceImp implements ChucVuService {

    @Autowired
    ChucVuRepository chucVuRepository;

    public List<ChucVuModel> getAllChucVu() {
        List<ChucVu> ChucVus=chucVuRepository.findAll();
        List<ChucVuModel> ChucVuModels=new ArrayList<>();
        for(ChucVu ChucVu:ChucVus){
            ChucVuModel ChucVuModel=parseEntityToModel(ChucVu);
            ChucVuModels.add(ChucVuModel);
        }
        return ChucVuModels;
    }

    @Override
    public ChucVuModel getChucVuByID(Integer id) {
        ChucVu ChucVu=chucVuRepository.findChucVuById(id);
        ChucVuModel ChucVuModel=parseEntityToModel(ChucVu);
        return ChucVuModel;
    }

    @Override
    public ChucVuModel parseEntityToModel(ChucVu ChucVu) {
        ChucVuModel ChucVuModel=new ChucVuModel();
        ChucVuModel.setId(ChucVu.getId());
        ChucVuModel.setTenCV(ChucVu.getTenCV());
        return ChucVuModel;
    }

    @Override
    public ChucVu parseModelToEntity(ChucVuModel ChucVuModel) {
        ChucVu ChucVu=new ChucVu();
        if(Objects.nonNull(ChucVuModel.getId())){
            ChucVu.setId(ChucVuModel.getId());
        }
        ChucVu.setTenCV(ChucVuModel.getTenCV());
        return ChucVu;
    }

    @Override
    public int createChucVu(ChucVuModel ChucVuModel) {
        ChucVu ChucVu=parseModelToEntity(ChucVuModel);
        if(Objects.nonNull(ChucVu.getDiem())){
            ChucVu.setDiem(3);
        }
        ChucVu ChucVu2=chucVuRepository.save(ChucVu);
        return ChucVu2.getId();
    }

    @Override
    public void updateChucVu(ChucVuModel ChucVuModel) {

        ChucVu ChucVu=chucVuRepository.findChucVuById(ChucVuModel.getId());
        if(Objects.nonNull(ChucVuModel.getTenCV())){
            ChucVu.setTenCV(ChucVuModel.getTenCV());
        }
        if(Objects.nonNull(ChucVuModel.getDiem())){
            ChucVu.setDiem(ChucVuModel.getDiem());
        }
        chucVuRepository.save(ChucVu);

    }

    @Override
    public void deleteChucVu(Integer id) {
        ChucVu ChucVu=chucVuRepository.findChucVuById(id);
        chucVuRepository.delete(ChucVu);
    }

}
