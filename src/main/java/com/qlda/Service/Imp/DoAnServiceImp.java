package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Exception.SaveImageException;
import com.qlda.Model.*;
import com.qlda.Repository.*;
import com.qlda.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoAnServiceImp implements DoAnService {
    static int num;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DoAnDKRepository doAnDKRepository;
    @Autowired
    DotBaoCaoService dotBaoCaoService;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    SinhVienService sinhVienService;
    @Autowired
    HoiDongRepository hoiDongRepository;
    @Autowired
    HoiDongService hoiDongService;
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    PhanCongService phanCongService;
    @Autowired
    PhanCongRepository phanCongRepository;
    @Value("${images.path}")
    private String UPLOAD_PATH;

    public List<DoAnModel> getAllDoAn() {
        List<DoAn> doAns=doAnRepository.findAll();
        return parseEntitiesToModels(doAns);
    }

    @Override
    public DoAnModel getDoAnByID(Integer id) {
        DoAn doAn=doAnRepository.findDoAnById(id);
        DoAnModel doAnModel=parseEntityToModel(doAn);
        return doAnModel;
    }

    @Override
    public DoAnModel getDoAnByMaDoAn(Integer maDoAn) {
        DoAn doAn=doAnRepository.findDoAnByMaDoAn(maDoAn);
        return parseEntityToModel(doAn);
    }

    @Override
    public DoAnModel getDoAnByMaSV(Integer maSV) {
        DoAn doAn=sinhVienRepository.findSinhVienByMaSV(maSV).getDoAn();
        return parseEntityToModel(doAn);
    }

    @Override
    public List<DoAnModel> getDoAnsByMaGV(Integer maGV) {
        List<DoAn> doAns=giaoVienRepository.findGiaoVienByMaGV(maGV).getPhanCong().stream()
                .filter(phanCong -> phanCong.getVaiTro().getId()==1).map(PhanCong::getDoAn).collect(Collectors.toList());
        return parseEntitiesToModels(doAns);
    }

    @Override
    public List<DoAnModel> getDoAnsByMaHD(Integer maHD) {
        List<DoAn> doAns=hoiDongRepository.findHoiDongByMaHD(maHD).getDoAns();
        return parseEntitiesToModels(doAns);
    }

    @Override
    public List<DoAnModel> parseEntitiesToModels(List<DoAn> doAns) {
        List<DoAnModel> doAnModels=new ArrayList<>();
        for(DoAn doAn:doAns){
            DoAnModel doAnModel=parseEntityToModel(doAn);
            doAnModels.add(doAnModel);
        }
        return doAnModels;
    }

    @Override
    public DoAnModel parseEntityToModel(DoAn doAn) {
        DoAnModel doAnModel=new DoAnModel();
        doAnModel.setId(doAn.getId());
        doAnModel.setTieuDe(doAn.getTieuDe());
        doAnModel.setMoTa(doAn.getMoTa());
        doAnModel.setUpdateDate(doAn.getUpdateDate());
        doAnModel.setCreateDate(doAn.getCreateDate());
        doAnModel.setMaDoAn(doAn.getMaDoAn());
        doAnModel.setStatus(doAn.getStatus());
        List<Integer> maGVs=doAn.getPhanCongs()
                .stream().filter(pc ->pc.getVaiTro().getId().equals(1))
                .map(PhanCong::getGiaoVien).map(GiaoVien::getMaGV).collect(Collectors.toList());
        doAnModel.setMaGVs(maGVs);
        List<Integer> maSVs=doAn.getSinhViens().stream().map(SinhVien::getMaSV).collect(Collectors.toList());
        doAnModel.setMaSVs(maSVs);
        if(Objects.nonNull(doAn.getImage())){
            doAnModel.setImage(doAn.getImage());
        }
        if(Objects.nonNull(doAn.getDotBaoCaos())){
            List<Integer> dotBaoCaoIds=doAn.getDotBaoCaos().stream().map(DotBaoCao::getId).collect(Collectors.toList());
            doAnModel.setDotBaoCaoIds(dotBaoCaoIds);
        }
        SinhVien sinhVien=doAn.getSinhViens().get(0);
        doAnModel.setChuyenNganhId(sinhVien.getKhoa().getId());
        doAnModel.setKhoaId(sinhVien.getKhoa().getId());
        doAnModel.setOpen(doAn.getOpen());
        return doAnModel;
    }

    @Override
    public DoAn parseModelToEntity(DoAnModel doAnModel) {
        DoAn doAn=new DoAn();
        doAn.setTieuDe(doAnModel.getTieuDe());
        if(Objects.nonNull(doAnModel.getId())){
            doAn.setId(doAnModel.getId());
        }
        if(Objects.nonNull(doAnModel.getMaDoAn())){
            doAn.setMaDoAn(doAnModel.getMaDoAn());
        }
        if(Objects.nonNull(doAnModel.getImage())){
            doAn.setImage(doAnModel.getImage());
        }
        doAn.setMoTa(doAnModel.getMoTa());
        if(Objects.nonNull(doAnModel.getStatus())){
            doAn.setStatus(doAnModel.getStatus());
        }

        return doAn;
    }

    @Override
    public DoAn parseDoAnDKToDoAnEntity(DoAnDK doAnDK) {
        DoAn doAn=new DoAn();
        doAn.setMaDoAn(doAnDK.getMaDoAn());
        doAn.setStatus(Boolean.FALSE);
        doAn.setOpen(Boolean.TRUE);
        doAn.setTieuDe(doAnDK.getTieuDe());
        doAn.setMoTa(doAnDK.getMoTa());
        return  doAn;
    }

    @Override
    public List<DoAnModel> getDoAnsByKhoaId(Integer khoaId) {
        List<DoAn> doAns=doAnRepository.findAll().stream().filter(doAn -> doAn.getSinhViens().get(0).getKhoa().getId()==khoaId)
                .sorted((da1,da2)->soSanhSoLuongSinhVien(da1,da2)).collect(Collectors.toList());
        return parseEntitiesToModels(doAns);
    }

    @Override
    public int getSoLuongSinhVien(DoAn doAn) {
        return doAn.getSinhViens().size();
    }
    public int soSanhSoLuongSinhVien(DoAn doAn1,DoAn doAn2) {
        return doAn2.getSinhViens().size()-doAn1.getSinhViens().size();
    }
    @Transactional
    @Override
    public int createDoAn(DoAnDK doAnDK) {
        DoAn doAn=parseDoAnDKToDoAnEntity(doAnDK);
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAn.setCreateDate(out);
        doAn.setUpdateDate(out);
        DoAn doAn2=doAnRepository.save(doAn);
        List<SinhVien> sinhViens=doAnDK.getSinhVienList();
        for(SinhVien sinhVien:sinhViens){
            sinhVien.setCheck(Boolean.TRUE);
            sinhVien.setDoAn(doAn2);
            sinhVienRepository.save(sinhVien);
        }

        GiaoVien giaoVien=doAnDK.getGiaoVien();
        PhanCongModel phanCongModel=new PhanCongModel();
        phanCongModel.setMaDoAn(doAn2.getMaDoAn());
        phanCongModel.setVaiTroID(1);
        phanCongModel.setMaGV(giaoVien.getMaGV());
        phanCongService.createPhanCong(phanCongModel);
        return doAn2.getId();
    }

    @Override
    public List<DoAn> getDoAnsDaPhanHDByKhoaId(Integer khoaId) {
        List<DoAn> doAns=doAnRepository.findDoAnsByIsOpenTrue().stream()
                .filter(doAn -> doAn.getSinhViens().get(0).getKhoa().getId()==khoaId)
                .filter(doAn -> Objects.nonNull(doAn.getHoiDong()))
               .collect(Collectors.toList());
        return doAns;
    }

    @Override
    public List<DoAn> getDoAnsChuaPhanHDByKhoaId(Integer khoaId) {
        List<DoAn> doAns=doAnRepository.findDoAnsByIsOpenTrue().stream()
                .filter(doAn -> doAn.getSinhViens().get(0).getKhoa().getId()==khoaId)
                .filter(doAn -> Objects.isNull(doAn.getHoiDong()))
                .collect(Collectors.toList());
        return doAns;
    }
    @Transactional
    @Override
    public Boolean autoPhanCongDoAnChoHD(Integer khoaId) {
        Integer SoLuongDoAnChenhLech=2;
        List<DoAn> doAnsChuaPC=getDoAnsChuaPhanHDByKhoaId(khoaId);
        List<DoAn> doAnsDaPC=getDoAnsDaPhanHDByKhoaId(khoaId);
        Map<Integer,Integer> mapMaHD_SLDA=new HashMap<>();
        List<HoiDongModel> hoiDongModels=hoiDongService.getOpenedHoiDongsBykhoaId(khoaId).stream()
                .sorted(Comparator.comparing((hoiDongModel)->hoiDongModel.getSoLuongDoAn())).collect(Collectors.toList());
        for(HoiDongModel hoiDongModel:hoiDongModels){
            mapMaHD_SLDA.put(hoiDongModel.getMaHD(),hoiDongModel.getSoLuongDoAn());
        }
        float trungBinhPhanCong = (doAnsChuaPC.size() + doAnsDaPC.size()) / hoiDongModels.size();
        List<AutoPCDoAnChoHD> autoPCs=new ArrayList<>();
        for(DoAn doAn:doAnsChuaPC){
            AutoPCDoAnChoHD autoPC=new AutoPCDoAnChoHD();
            autoPC.setMaDoAn(doAn.getMaDoAn());
            autoPC.setMaHDs(hoiDongService.getmaHDMaDoAnCoThePhan(doAn));
            autoPCs.add(autoPC);
        }
        autoPCs=autoPCs.stream().sorted(Comparator.comparing((auto)->auto.getMaHDs().size())).collect(Collectors.toList());
        Random random=new Random();
        if(autoPCs.get(0).getMaHDs().size()<1){
            return Boolean.FALSE;
        }else{
            for(AutoPCDoAnChoHD auto:autoPCs){
                if(auto.getMaHDs().size()==1){
                    Integer maHDSePC=auto.getMaHDs().get(0);
                    auto.setMaHDPC(maHDSePC);
                    Integer SLDA_HienTai=mapMaHD_SLDA.get(maHDSePC);
                    mapMaHD_SLDA.put(maHDSePC,SLDA_HienTai+1);
                    if(SLDA_HienTai>=trungBinhPhanCong+SoLuongDoAnChenhLech){
                        return Boolean.FALSE;
                    }
                }else{
                   while (true){
                        Integer maHDSePC=auto.getMaHDs().get(random.nextInt(auto.getMaHDs().size()));
                        Integer SLDA_HienTai=mapMaHD_SLDA.get(maHDSePC);
                        if(SLDA_HienTai>=trungBinhPhanCong+SoLuongDoAnChenhLech){
                            List<Integer> maHDsConlai=auto.getMaHDs();
                            maHDsConlai.remove(maHDsConlai.indexOf(maHDSePC));
                            if(maHDsConlai.size()==0){
                                return Boolean.FALSE;
                            }
                            auto.setMaHDs(maHDsConlai);
                        }else{
                            auto.setMaHDPC(maHDSePC);
                            mapMaHD_SLDA.put(maHDSePC,SLDA_HienTai+1);
                            break;
                        }
                    }

                }
            }
        }
        for(AutoPCDoAnChoHD autoDaPC:autoPCs){
            DoAn doAn=doAnRepository.findDoAnByMaDoAn(autoDaPC.getMaDoAn());
            doAn.setHoiDong(hoiDongRepository.findHoiDongByMaHD(autoDaPC.getMaHDPC()));
            doAnRepository.save(doAn);

        }

        return Boolean.TRUE;
    }
    @Transactional
    @Override
    public void updateDoAn(DoAnModel doAnModel) {
        DoAn doAn=doAnRepository.findDoAnByMaDoAn(doAnModel.getMaDoAn());
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAn.setUpdateDate(out);
        if(Objects.nonNull(doAnModel.getTieuDe())&&(Objects.isNull(doAn.getDotBaoCaos())||doAn.getDotBaoCaos().size()<2)){
            doAn.setTieuDe(doAnModel.getTieuDe());
        }
        if(Objects.nonNull(doAnModel.getStatus())){
            doAn.setStatus(doAnModel.getStatus());
        }
        if(Objects.nonNull(doAnModel.getMoTa())){
            doAn.setMoTa(doAnModel.getMoTa());
        }
        if(Objects.nonNull(doAnModel.getOpen())){
            doAn.setOpen(doAnModel.getOpen());
        }
        if (Objects.nonNull(doAnModel.getImage())){
            String image = saveImage(doAnModel.getImage());
            doAn.setImage(image);
        }
        List<PhanCong> phanCongs=phanCongService.getGVHDPhanCongEntitiesByMaDoAN(doAnModel.getMaDoAn());
        PhanCong phanCong;
        if(phanCongs.size()==doAnModel.getMaGVs().size()){
            for(int i=0;i<phanCongs.size();i++){
                if(phanCongs.get(i).getGiaoVien().getMaGV()==doAnModel.getMaGVs().get(i)){
                    continue;
                }else{
                    phanCongs.get(i).setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(doAnModel.getMaGVs().get(i)));
                }
            }
            phanCongRepository.save(phanCongs);
        }else{
            if(phanCongs.size()>doAnModel.getMaGVs().size()){
                int i=0;
                for(;i<doAnModel.getMaGVs().size();i++){
                    phanCongs.get(i).setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(doAnModel.getMaGVs().get(i)));
                    phanCongRepository.save( phanCongs.get(i));
                }
                for(;i<phanCongs.size();i++){
                    phanCongRepository.delete( phanCongs.get(i));
                }
            }else{
                int i=0;
                for(;i<phanCongs.size();i++){
                    phanCongs.get(i).setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(doAnModel.getMaGVs().get(i)));
                    phanCongRepository.save( phanCongs.get(i));
                }
                for(;i<doAnModel.getMaGVs().size();i++){
                    phanCongService.createPhanCongHuongDanByMaGVAndMaDoAN(doAnModel.getMaGVs().get(i),doAnModel.getMaDoAn());
                }
            }
        }
        doAnRepository.save(doAn);
    }

    @Override
    public void deleteDoAn(Integer maDoAn) {
        DoAn doAn=doAnRepository.findDoAnByMaDoAn(maDoAn);
        List<SinhVien> sinhViens=doAn.getSinhViens();
        for(SinhVien sinhVien:sinhViens){
            sinhVien.setCheck(Boolean.FALSE);
            sinhVienRepository.save(sinhVien);
        }
        doAnRepository.delete(doAn);
    }


    private String saveImage(String base64Image) throws SaveImageException {
        String imageName = UUID.randomUUID().toString() + ".jpg";
        BufferedImage image;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64Image);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            File outputfile = new File(UPLOAD_PATH + imageName);
            ImageIO.write(image, "jpg", outputfile);
            return "images/" + imageName;
        } catch (Exception e) {
            throw new SaveImageException("The image is invalid!");
        }
    }
}
