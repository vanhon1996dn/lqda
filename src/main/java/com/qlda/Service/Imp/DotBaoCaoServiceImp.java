package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.DotBaoCaoModel;
import com.qlda.Repository.*;
import com.qlda.Service.DotBaoCaoService;
import com.qlda.Service.FileService;
import com.qlda.Service.GiaoVienService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class DotBaoCaoServiceImp implements DotBaoCaoService {
    @Autowired
    DotBaoCaoRepository dotBaoCaoRepository;
    @Autowired
    BaoCaoTienDoRepository baoCaoTienDoRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    VaiTroRepository vaiTroRepository;
    @Autowired
    PhanCongRepository phanCongRepository;
    @Autowired
    FileService fileService;
    public List<DotBaoCaoModel> getAllDotBaoCao() {
        List<DotBaoCao> dotBaoCaos=dotBaoCaoRepository.findDotBaoCaosByOrderByThoiGianDesc();
        return parseEntitiesToModels(dotBaoCaos);
    }

    @Override
    public DotBaoCaoModel getDotBaoCaoByID(Integer id) {
        DotBaoCao dotBaoCao=dotBaoCaoRepository.findDotBaoCaoById(id);
        DotBaoCaoModel dotBaoCaoModel=parseEntityToModel(dotBaoCao);
        return dotBaoCaoModel;
    }

    @Override
    public List<DotBaoCaoModel> getDotBaoCaosByMaDoAn(Integer maDoAn) {
        List<DotBaoCao> dotBaoCaos=dotBaoCaoRepository.findDotBaoCaosByDoAn_MaDoAn(maDoAn);
        return parseEntitiesToModels(dotBaoCaos);
    }

    @Override
    public List<DotBaoCaoModel> getDotBaoCaosByMaGV(Integer maGV) {
        Date date=new Date();

        List<DotBaoCao> dotBaoCaos=giaoVienRepository.findGiaoVienByMaGV(maGV).getPhanCong().stream()
                .filter(phanCong -> phanCong.getVaiTro().getId()>1).map(PhanCong::getDotBaoCao)
                .filter(dotBaoCao -> dotBaoCao.getThoiGian().before(date)).collect(Collectors.toList());
        return parseEntitiesToModels(dotBaoCaos);
    }

    @Override
    public List<DotBaoCaoModel> parseEntitiesToModels(List<DotBaoCao> dotBaoCaos) {
        List<DotBaoCaoModel> dotBaoCaoModels=new ArrayList<>();
        for(DotBaoCao dotBaoCao:dotBaoCaos){
            DotBaoCaoModel dotBaoCaoModel=parseEntityToModel(dotBaoCao);
            dotBaoCaoModels.add(dotBaoCaoModel);
        }
        return dotBaoCaoModels;
    }

    @Override
    public DotBaoCaoModel parseEntityToModel(DotBaoCao dotBaoCao) {
        DotBaoCaoModel dotBaoCaoModel=new DotBaoCaoModel();
        dotBaoCaoModel.setId(dotBaoCao.getId());
        dotBaoCaoModel.setBaoCaoTienDoId(dotBaoCao.getBaoCaoTienDo().getId());
        dotBaoCaoModel.setMaDoAn(dotBaoCao.getDoAn().getMaDoAn());
        dotBaoCaoModel.setThoiGian(dotBaoCao.getThoiGian());
        dotBaoCaoModel.setStatus(dotBaoCao.getStatus());
        if(Objects.nonNull(dotBaoCao.getFileOfDotBaoCaos())){
            dotBaoCaoModel.setFileIds(fileService.getFileIdsByDotBaoCaoId(dotBaoCao.getId()));
        }
        PhanCong phanCong= phanCongRepository.findPhanCongsByDotBaoCao_Id(dotBaoCao.getId()).stream()
                .filter(phanCong1 -> phanCong1.getVaiTro().getId()>1).collect(Collectors.toList()).get(0);
        dotBaoCaoModel.setMaGV(phanCong.getGiaoVien().getMaGV());
        dotBaoCaoModel.setVaiTroId(phanCong.getVaiTro().getId());
        dotBaoCaoModel.setMoTa(dotBaoCao.getMoTa());
        return dotBaoCaoModel;
    }

    @Override
    public DotBaoCao parseModelToEntity(DotBaoCaoModel dotBaoCaoModel) {
        DotBaoCao dotBaoCao=new DotBaoCao();
        if(Objects.nonNull(dotBaoCaoModel.getId())){
            dotBaoCao.setId(dotBaoCaoModel.getId());
        }
        dotBaoCao.setBaoCaoTienDo(baoCaoTienDoRepository.findBaoCaoTienDoById(dotBaoCaoModel.getBaoCaoTienDoId()));
        if(Objects.nonNull(dotBaoCaoModel.getThoiGian())){
            dotBaoCao.setThoiGian(dotBaoCaoModel.getThoiGian());
        }else{
            dotBaoCao.setThoiGian(dotBaoCao.getBaoCaoTienDo().getThoiGian());
        }

        dotBaoCao.setDoAn(doAnRepository.findDoAnByMaDoAn(dotBaoCaoModel.getMaDoAn()));
        dotBaoCao.setStatus(dotBaoCaoModel.getStatus());
        dotBaoCao.setMoTa(dotBaoCaoModel.getMoTa());
        return dotBaoCao;
    }
    @Transactional
    @Override
    public void createAllDotBaoCaosByBaoCaoTienDoId(Integer baoCaoTienDoId) {
        List<DoAn> doAns=doAnRepository.findDoAnsByIsOpenTrue();
        BaoCaoTienDo baoCaoTienDo=baoCaoTienDoRepository.findBaoCaoTienDoById(baoCaoTienDoId);
        VaiTro vaiTro=vaiTroRepository.findVaiTroById(2);
        for(DoAn doAn:doAns){
            List<DotBaoCao> dotBaoCaos=doAn.getDotBaoCaos().stream().filter(dotBaoCao -> dotBaoCao.getBaoCaoTienDo().getId()==baoCaoTienDoId).collect(Collectors.toList());
           if(Objects.isNull(dotBaoCaos)||dotBaoCaos.size()==0 ){
               DotBaoCao dotBaoCao=new DotBaoCao();
               dotBaoCao.setDoAn(doAn);
               dotBaoCao.setMoTa("");
               dotBaoCao.setStatus(Boolean.FALSE);
               dotBaoCao.setThoiGian(baoCaoTienDo.getThoiGian());
               dotBaoCao.setBaoCaoTienDo(baoCaoTienDo);
               DotBaoCao dotBaoCao2=dotBaoCaoRepository.save(dotBaoCao);
               List<PhanCong> phanCongs=doAn.getPhanCongs();
               List<PhanCong> giaoVien=phanCongs.stream()
                       .filter(phanCong1 -> phanCong1.getVaiTro().getId()<2)
                       .collect(Collectors.toList());
               PhanCong phanCong=new PhanCong();
               phanCong.setDotBaoCao(dotBaoCao2);
               phanCong.setDoAn(doAn);


               phanCong.setGiaoVien(giaoVien.get(0).getGiaoVien());
               phanCong.setVaiTro(vaiTro);
               phanCongRepository.save(phanCong);

           }
        }
    }
    @Transactional
    @Override
    public int createDotBaoCao(DotBaoCaoModel dotBaoCaoModel) {
        DoAn doAn=doAnRepository.findDoAnByMaDoAn(dotBaoCaoModel.getMaDoAn());
        DotBaoCao dotBaoCao=parseModelToEntity(dotBaoCaoModel);
        dotBaoCao.setStatus(Boolean.FALSE);
        DotBaoCao dotBaoCao2=dotBaoCaoRepository.save(dotBaoCao);
        PhanCong phanCong=new PhanCong();
        phanCong.setDotBaoCao(dotBaoCao2);
        phanCong.setDoAn(doAn);
        phanCong.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(dotBaoCaoModel.getMaGV()));
        phanCong.setVaiTro(vaiTroRepository.findVaiTroById(dotBaoCaoModel.getVaiTroId()));
        phanCongRepository.save(phanCong);
        return dotBaoCao2.getId();
    }
    @Transactional
    @Override
    public void updateDotBaoCao(DotBaoCaoModel dotBaoCaoModel) {
        DotBaoCao dotBaoCao=parseModelToEntity(dotBaoCaoModel);
        DoAn doAn=dotBaoCao.getDoAn();
        doAn.setStatus(dotBaoCao.getStatus());
        doAnRepository.save(doAn);
        PhanCong phanCong=phanCongRepository.findPhanCongsByDotBaoCao_Id(dotBaoCaoModel.getId()).get(0);
        phanCong.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(dotBaoCaoModel.getMaGV()));
        phanCong.setVaiTro(vaiTroRepository.findVaiTroById(dotBaoCaoModel.getVaiTroId()));
        phanCongRepository.save(phanCong);
        dotBaoCaoRepository.save(dotBaoCao);

    }

    @Override
    public void deleteDotBaoCao(Integer id) {
        DotBaoCao dotBaoCao=dotBaoCaoRepository.findDotBaoCaoById(id);
        List<PhanCong> phanCongs=dotBaoCao.getPhanCongs();
        phanCongRepository.delete(phanCongs);
        dotBaoCaoRepository.delete(dotBaoCao);
    }

    @Override
    public void deleteDotBaoCaoByBaoCaoTienDoId(Integer baoCaoTienDoId) {
        BaoCaoTienDo baoCaoTienDo=baoCaoTienDoRepository.findBaoCaoTienDoById(baoCaoTienDoId);
        List<DotBaoCao> dotBaoCaos=baoCaoTienDo.getDotBaoCaos();
        dotBaoCaoRepository.delete(dotBaoCaos);
    }

    @Override
    public Boolean checkDotBaoCao(DotBaoCaoModel dotBaoCaoModel) {
        DoAn doAn=doAnRepository.findDoAnByMaDoAn(dotBaoCaoModel.getMaDoAn());
        if(dotBaoCaoModel.getVaiTroId()<2){
            return Boolean.FALSE;
        }
        List<DotBaoCao> dotBaoCaos=doAn.getDotBaoCaos().stream().filter(dotBaoCao -> dotBaoCao.getBaoCaoTienDo().getId()==dotBaoCaoModel.getBaoCaoTienDoId()).collect(Collectors.toList());
        if(Objects.nonNull(dotBaoCaos)&&Objects.nonNull(dotBaoCaoModel.getId())&&dotBaoCaos.get(0).getId()==dotBaoCaoModel.getId()
                &&dotBaoCaos.get(0).getBaoCaoTienDo().getId()==dotBaoCaoModel.getBaoCaoTienDoId()){
            return Boolean.TRUE;
        }
        if(Objects.nonNull(dotBaoCaos)&&dotBaoCaos.size()>0){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}
