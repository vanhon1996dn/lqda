package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.FileModel;
import com.qlda.Repository.*;
import com.qlda.Service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FileServiceImp implements FileService {

    @Autowired
    FileRepository fileRepository;
    @Autowired
    FileOfDotBaoCaoRepository fileOfDotBaoCaoRepository;
    @Autowired
    FileOfDoAnRepository fileOfDoAnRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DotBaoCaoRepository dotBaoCaoRepository;
    public List<FileModel> getAllFile() {
        List<File> files=fileRepository.findAll();
        return parseEntityListToModelList(files);
    }

    @Override
    public FileModel getFileByID(Integer id) {
        File file=fileRepository.findFileById(id);
        FileModel fileModel=parseEntityToModel(file);
        return fileModel;
    }

    @Override
    public List<Integer> getFileIdsByDotBaoCaoId(Integer dotBaoCaoId) {
        DotBaoCao dotBaoCao=dotBaoCaoRepository.findDotBaoCaoById(dotBaoCaoId);
        List<Integer> FileIds=
                dotBaoCao.getFileOfDotBaoCaos().stream().map(FileOfDotBaoCao::getFile).map(File::getId).collect(Collectors.toList());
        return FileIds;
    }

    @Override
    public List<Integer> getFileIdsByMaDoAnId(Integer doAnId) {
        return null;
    }

    @Override
    public List<FileModel> parseEntityListToModelList(List<File> files) {
        List<FileModel> fileModels=new ArrayList<>();
        for(File file:files){
            FileModel fileModel=parseEntityToModel(file);
            fileModels.add(fileModel);
        }
        return fileModels;
    }

    @Override
    public FileModel parseEntityToModel(File file) {
        FileModel fileModel=new FileModel();
        fileModel.setId(file.getId());
        fileModel.setTenFile(file.getTenFile());
        fileModel.setLoai(file.getLoai());
        return fileModel;
    }

    @Override
    public File parseModelToEntity(FileModel fileModel) {
        File file=new File();
        if(Objects.nonNull(fileModel.getId())){
            file.setId(fileModel.getId());
        }
        file.setTenFile(fileModel.getTenFile());
        file.setLoai(fileModel.getLoai());
        return file;
    }

    @Override
    public int createFile(FileModel fileModel) {
        File file=parseModelToEntity(fileModel);
        File file2=fileRepository.save(file);
        return file2.getId();
    }

    @Override
    public void updateFile(FileModel fileModel) {
        File file=parseModelToEntity(fileModel);
        fileRepository.save(file);

    }

    @Override
    public void deleteFile(Integer id) {
        File file=fileRepository.findFileById(id);
        fileRepository.delete(file);
    }
}
