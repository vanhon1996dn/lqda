package com.qlda.Service.Imp;

import com.qlda.Entity.Role;
import com.qlda.Entity.RoleUser;
import com.qlda.Entity.UserEntity;
import com.qlda.Model.UserModel;
import com.qlda.Repository.*;
import com.qlda.Service.Useful;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import io.jsonwebtoken.Jwts;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserServiceImp implements UserService {
    static int num;
    @Autowired
    UserRepository userRepository;
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    RoleUserRepository roleUserRepository;
    @Value("${security.signing-key}")
    private String signingKey;

    public List<UserModel> getAllUser() {
        List<UserEntity> Users=userRepository.findAll();
        List<UserModel> UserModels=new ArrayList<>();
        for(UserEntity User:Users){
            UserModel UserModel=parseEntityToModel(User);
            UserModels.add(UserModel);
        }
        return UserModels;
    }

    @Override
    public UserModel getUserByID(Integer id) {
        UserEntity User=userRepository.findUserEntityById(id);
        UserModel UserModel=parseEntityToModel(User);
        return UserModel;
    }

    @Override
    public UserModel parseEntityToModel(UserEntity User) {
        UserModel UserModel=new UserModel();
        UserModel.setUserName(User.getUserName());
        UserModel.setPassword(User.getPassword());

        return UserModel;
    }

    @Override
    public UserEntity parseModelToEntity(UserModel UserModel) {
        UserEntity User=new UserEntity();

        if(Objects.nonNull(UserModel.getPassword())){
            ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
            String passwordEncode = shaPasswordEncoder.encodePassword(UserModel.getPassword(), null);
            User.setPassword(passwordEncode);
        }
        User.setUserName(UserModel.getUserName());
        return User;
    }

    @Override
    public int createUser(UserModel UserModel) {
        UserEntity User=parseModelToEntity(UserModel);
        Date date;
        Integer maso=Integer.parseInt(User.getUserName());
        Role role;
        if(maso<200000000){
            date=sinhVienRepository.findSinhVienByMaSV(maso).getNgaySinh();
            role = roleRepository.findById(1); // role Sinhvien
        }else{
            date=giaoVienRepository.findGiaoVienByMaGV(maso).getNgaySinh();
            role = roleRepository.findById(2); // role giaovien
        }
        String reportDate=String.format("%1$td%1$tm%1$tY", date);
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
        String passwordEncode = shaPasswordEncoder.encodePassword(reportDate, null);
        User.setPassword(passwordEncode);
        UserEntity User2=userRepository.save(User);
        RoleUser roleUser = new RoleUser();
        roleUser.setUser(User2);
        roleUser.setRole(role);
        roleUserRepository.save(roleUser);
        return User2.getId();
    }


    @Override
    public void updateUser(UserModel UserModel) {

        UserEntity User=userRepository.findByUserName(UserModel.getUserName());
        if(Objects.nonNull(UserModel.getPassword())){
            ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
            String passwordEncode = shaPasswordEncoder.encodePassword(UserModel.getPassword(), null);
            User.setPassword(passwordEncode);
        }
        userRepository.save(User);

    }
    @Override
    public int createUserByAdmin(UserModel UserModel) {
        UserEntity userEntity=new UserEntity();
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
        String passwordEncode = shaPasswordEncoder.encodePassword(UserModel.getPassword(), null);
        userEntity.setPassword(passwordEncode);
        userEntity.setUserName(Useful.getAutoUserName().toString());
        UserEntity User2=userRepository.save(userEntity);
        RoleUser roleUser = new RoleUser();
        roleUser.setUser(User2);
        roleUser.setRole(roleRepository.findById(3));
        roleUserRepository.save(roleUser);
        return 0;
    }

    @Override
    public void updateUserByAdmin(UserModel UserModel) {
        UserEntity userEntity=userRepository.findByUserName(UserModel.getUserName());
        ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
        String passwordEncode = shaPasswordEncoder.encodePassword(UserModel.getPassword(), null);
        userEntity.setPassword(passwordEncode);
        userRepository.save(userEntity);

    }

    @Override
    public Boolean checkUser(UserModel UserModel) {
        UserEntity User=userRepository.findByUserName(UserModel.getUserName());
        if(Objects.nonNull(UserModel.getPasswordCu())){
            ShaPasswordEncoder shaPasswordEncoder = new ShaPasswordEncoder(256);
            String passwordEncode = shaPasswordEncoder.encodePassword(UserModel.getPasswordCu(), null);
            if(User.getPassword().equals(passwordEncode)){
                return Boolean.TRUE;
            }
            return Boolean.FALSE;
        }
        return Boolean.FALSE;
    }
    @Transactional
    @Override
    public void deleteUser(String userName) {
        UserEntity User=userRepository.findUserEntityByUserName(userName);
        roleUserRepository.delete(User.getRoleUsers());
        userRepository.delete(User);
    }
    @Override
    public String getUserNameByToken(String token) {
        String user = (String) Jwts.parser().setSigningKey(signingKey.getBytes())
                .parseClaimsJws(token.replace("Bearer ",""))
                .getBody().get("user_name");
        return user;

    }

    @Override
    public boolean validateExistedUser(String username) {
        UserEntity existedUser = userRepository.findByUserName(username);
        if(existedUser != null) {
            return true;
        }
        return false;
    }

    @Override
    public List<String> getAuthoritiesByToken(String token) {
        return getAuthoritiesByUserName(getUserNameByToken(token));
    }

    @Override
    public List<String> getAuthoritiesByUserName(String userName) {
        UserEntity userEntity=userRepository.findUserEntityByUserName(userName);
        List<String> authorities=userEntity.getRoleUsers().stream().map(RoleUser::getRole).map(Role::getRoleName).collect(Collectors.toList());
        return authorities;
    }


}
