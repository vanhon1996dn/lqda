package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.SinhVienModel;
import com.qlda.Model.UserModel;
import com.qlda.Repository.ChuyenNganhRepository;
import com.qlda.Repository.KhoaRepository;
import com.qlda.Repository.SinhVienRepository;
import com.qlda.Service.SinhVienService;
import com.qlda.Service.Useful;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class SinhVienServiceImp implements SinhVienService {
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    KhoaRepository khoaRepository;
    @Autowired
    ChuyenNganhRepository chuyenNganhRepository;
    @Autowired
    UserService userService;
    static int num;
    @Override
    public List<SinhVienModel> getAllSinhVien() {
        List<SinhVien> sinhViens=sinhVienRepository.findAll();
        return parseEntitiesToModels(sinhViens);
    }

    @Override
    public SinhVienModel getSinhVienByID(Integer id) {
        SinhVien sinhVien=sinhVienRepository.findSinhVienById(id);
        return parseEntityToModel(sinhVien);
    }

    @Override
    public SinhVienModel getSinhVienByMaSV(Integer maSV) {
        SinhVien sinhVien=sinhVienRepository.findSinhVienByMaSV(maSV);
        return parseEntityToModel(sinhVien);
    }

    @Override
    public List<SinhVienModel> getSinhVienModelsByMaDoAn(Integer maDoAn) {
        List<SinhVien> sinhViens=sinhVienRepository.findSinhViensByDoAn_MaDoAn(maDoAn);
        return parseEntitiesToModels(sinhViens);
    }

    @Override
    public List<SinhVienModel> parseEntitiesToModels(List<SinhVien> sinhViens) {
        List<SinhVienModel> SinhVienModels=new ArrayList<>();
        for(SinhVien sinhVien:sinhViens){
            SinhVienModel SinhVienModel=parseEntityToModel(sinhVien);
            SinhVienModels.add(SinhVienModel);
        }
        return SinhVienModels;
    }

    @Override
    public SinhVienModel parseEntityToModel(SinhVien sinhVien) {
        SinhVienModel sinhVienModel=new SinhVienModel();
        sinhVienModel.setId(sinhVien.getId());
        sinhVienModel.setMaSV(sinhVien.getMaSV());
        sinhVienModel.setLop(sinhVien.getLop());
        sinhVienModel.setTenSV(sinhVien.getTenSV());
        sinhVienModel.setNgaySinh(sinhVien.getNgaySinh());
        sinhVienModel.setKhoaId(sinhVien.getKhoa().getId());
        sinhVienModel.setChuyenNganhId(sinhVien.getChuyenNganh().getId());
        sinhVienModel.setCheck(sinhVien.isCheck());
        return sinhVienModel;
    }

    @Override
    public SinhVien parseModelToEntity(SinhVienModel sinhVienModel) {
        SinhVien sinhVien=new SinhVien();
        if(Objects.nonNull(sinhVienModel.getId())){
            sinhVien.setId(sinhVienModel.getId());
        }
        if(Objects.nonNull(sinhVienModel.getMaSV())){
            sinhVien.setMaSV(sinhVienModel.getMaSV());
        }
        if(Objects.nonNull(sinhVienModel.isCheck())){
            sinhVien.setCheck(sinhVienModel.isCheck());
        }
        sinhVien.setLop(sinhVienModel.getLop());
        sinhVien.setTenSV(sinhVienModel.getTenSV());
        sinhVien.setNgaySinh(sinhVienModel.getNgaySinh());
        sinhVien.setKhoa(khoaRepository.findKhoaById(sinhVienModel.getKhoaId()));
        sinhVien.setChuyenNganh(chuyenNganhRepository.findChuyenNganhById(sinhVienModel.getChuyenNganhId()));
        return sinhVien;
    }
    @Transactional
    @Override
    public int createSinhVien(SinhVienModel sinhVienModel) {
        SinhVien sinhVien=parseModelToEntity(sinhVienModel);
        sinhVien.setCheck(Boolean.FALSE);
        sinhVien.setMaSV(Useful.getMaSoSV(sinhVienModel.getKhoaId()));
        SinhVien sinhVien2=sinhVienRepository.save(sinhVien);
        UserModel userModel=new UserModel();
        userModel.setUserName(sinhVien.getMaSV().toString());
        userService.createUser(userModel);
        return sinhVien2.getId();
    }

    @Override
    public void updateSinhVien(SinhVienModel SinhVienModel) {
        SinhVien SinhVien=parseModelToEntity(SinhVienModel);
        sinhVienRepository.save(SinhVien);

    }
    @Transactional
    @Override
    public void deleteSinhVien(Integer maSV) {
        SinhVien SinhVien=sinhVienRepository.findSinhVienByMaSV(maSV);
        userService.deleteUser(SinhVien.getMaSV().toString());
        sinhVienRepository.delete(SinhVien);
    }



}
