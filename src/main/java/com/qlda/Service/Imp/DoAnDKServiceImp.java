package com.qlda.Service.Imp;

import com.qlda.Entity.DoAnDK;
import com.qlda.Entity.DoAnMau;
import com.qlda.Entity.GiaoVien;
import com.qlda.Entity.SinhVien;
import com.qlda.Exception.SaveImageException;
import com.qlda.Model.DoAnModel;
import com.qlda.Model.GiaoVienModel;
import com.qlda.Repository.DoAnDKRepository;
import com.qlda.Repository.DoAnMauRepository;
import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Repository.SinhVienRepository;
import com.qlda.Service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoAnDKServiceImp implements DoAnDKService {
    static int num;
    @Autowired
    DoAnDKRepository doAnDKRepository;
    @Autowired
    DoAnService doAnService;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    SinhVienService sinhVienService;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    SinhVienRepository sinhVienRepository;
    @Autowired
    DoAnMauRepository doAnMauRepository;
    @Value("${images.path}")
    private String UPLOAD_PATH;

    public List<DoAnModel> getAllDoAnDK() {
        List<DoAnDK> doAnDKs=doAnDKRepository.findAll();
        return parseEntitiesToModels(doAnDKs);
    }

    @Override
    public DoAnModel getDoAnDKByID(Integer id) {
        DoAnDK doAnDK=doAnDKRepository.findDoAnDKById(id);
        DoAnModel doAnModel=parseEntityToModel(doAnDK);
        return doAnModel;
    }

    @Override
    public DoAnModel getDoAnDKByMaDoAn(Integer maDoAn) {
        DoAnDK doAnDK=doAnDKRepository.findDoAnDKByMaDoAn(maDoAn);
        return  parseEntityToModel(doAnDK);
    }

    @Override
    public DoAnModel getDoAnDKByMaSV(Integer maSV) {
        DoAnDK doAnDK=sinhVienRepository.findSinhVienByMaSV(maSV).getDoAnDK();
        return parseEntityToModel(doAnDK);
    }

    @Override
    public DoAnModel parseEntityToModel(DoAnDK doAnDK) {
        DoAnModel doAnModel=new DoAnModel();
        doAnModel.setTieuDe(doAnDK.getTieuDe());
        doAnModel.setId(doAnDK.getId());
        doAnModel.setMoTa(doAnDK.getMoTa());
        doAnModel.setCreateDate(doAnDK.getCreateDate());
        doAnModel.setUpdateDate(doAnDK.getUpdateDate());
        doAnModel.setMaDoAn(doAnDK.getMaDoAn());
        doAnModel.setStatus(doAnDK.getStatus());
        doAnModel.setOpen(doAnDK.getOpen());
        List<Integer> maGVs=new ArrayList<>();
        maGVs.add(doAnDK.getGiaoVien().getMaGV());
        doAnModel.setMaGVs(maGVs);
        List<Integer> maSVs=doAnDK.getSinhVienList().stream().map(SinhVien::getMaSV).collect(Collectors.toList());
        doAnModel.setKhoaId(doAnDK.getSinhVienList().get(0).getKhoa().getId());
        doAnModel.setChuyenNganhId(doAnDK.getSinhVienList().get(0).getChuyenNganh().getId());
        doAnModel.setMaSVs(maSVs);

        return doAnModel;
    }

    @Override
    public List<DoAnModel> parseEntitiesToModels(List<DoAnDK> doAnDKs) {
        List<DoAnModel> doAnModels=new ArrayList<>();
        for(DoAnDK doAnDK:doAnDKs){
            DoAnModel doAnModel=parseEntityToModel(doAnDK);
            doAnModels.add(doAnModel);
        }
        return doAnModels;
    }

    @Override
    public DoAnDK parseModelToEntity(DoAnModel doAnModel) {
        DoAnDK doAnDK=new DoAnDK();
        if(Objects.nonNull(doAnModel.getId())){
            doAnDK.setId(doAnModel.getId());
        }
        if(Objects.nonNull(doAnModel.getMaDoAn())){
            doAnDK.setMaDoAn(doAnModel.getMaDoAn());
        }
        doAnDK.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(doAnModel.getMaGVs().get(0)));
        doAnDK.setTieuDe(doAnModel.getTieuDe());
        doAnDK.setMoTa(doAnModel.getMoTa());
        if(Objects.nonNull(doAnModel.getOpen())){
            doAnDK.setOpen(doAnModel.getOpen());
        }
        return doAnDK;
    }

    @Override
    public List<DoAnModel> getDoAnDKsByKhoaId(Integer khoaId) {
        List<DoAnDK> doAnDKs=doAnDKRepository.findAll().stream().filter(doAnDK -> doAnDK.getSinhVienList().get(0).getKhoa().getId()==khoaId)
                .sorted((da1,da2)->soSanhSoLuongSinhVien(da1,da2)).collect(Collectors.toList());
        return parseEntitiesToModels(doAnDKs);
    }

    @Override
    public List<DoAnDK> getDoAnDKEntitysChuaPhanCongByKhoaId(Integer khoaId) {
        List<DoAnDK> doAnDKs=doAnDKRepository.findDoAnDKSByIsOpenTrueAndStatusFalseOrderByCreateDate().stream().filter(doAnDK -> doAnDK.getSinhVienList().get(0).getKhoa().getId()==khoaId)
                .sorted((da1,da2)->soSanhSoLuongSinhVien(da1,da2)).collect(Collectors.toList());
        return doAnDKs;
    }

    @Override
    public List<DoAnDK> getDoAnDKEntitysByKhoaId(Integer khoaId) {
        List<DoAnDK> doAnDKs=doAnDKRepository.findAll().stream().filter(doAnDK -> doAnDK.getSinhVienList().get(0).getKhoa().getId()==khoaId)
                .sorted((da1,da2)->soSanhSoLuongSinhVien(da1,da2)).collect(Collectors.toList());
        return doAnDKs;
    }

    public int getSoLuongSinhVien(DoAnDK doAn) {
        return doAn.getSinhVienList().size();
    }
    public int soSanhSoLuongSinhVien(DoAnDK doAn1,DoAnDK doAn2) {
        return doAn2.getSinhVienList().size()-doAn1.getSinhVienList().size();
    }
    @Transactional
    @Override
    public int createDoAnDK(DoAnModel doAnModel) {
        SinhVien sinhVien=sinhVienRepository.findSinhVienByMaSV(doAnModel.getMaSVs().get(0));
        if(Objects.nonNull(doAnModel.getMaDoAn())){
            Integer maDoAn=doAnModel.getMaDoAn();
            if(maDoAn<200000000){
                DoAnDK doAnDK=doAnDKRepository.findDoAnDKByMaDoAn(doAnModel.getMaDoAn());
                sinhVien.setDoAnDK(doAnDK);
                sinhVienRepository.save(sinhVien);
                return doAnDK.getId();
            }else{
                DoAnDK doAnDK=convertFromDoAnMau(doAnModel.getMaDoAn());
                doAnDK.setMaDoAn(Useful.getMaDoAnDK(sinhVien.getKhoa().getId()));
                DoAnDK doAnDK2=doAnDKRepository.save(doAnDK);
                sinhVien.setDoAnDK(doAnDK2);
                sinhVienRepository.save(sinhVien);
                return doAnDK2.getId();
            }
        }else{
            DoAnDK doAnDK=parseModelToEntity(doAnModel);
            Date in = new Date();
            LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
            Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
            doAnDK.setCreateDate(out);
            doAnDK.setUpdateDate(out);
            doAnDK.setMaDoAn(Useful.getMaDoAnDK(sinhVienRepository.findSinhVienByMaSV(doAnModel.getMaSVs().get(0)).getKhoa().getId()));
            doAnDK.setOpen(Boolean.FALSE);
            doAnDK.setStatus(Boolean.FALSE);
            DoAnDK doAnDK2=doAnDKRepository.save(doAnDK);
            sinhVien.setDoAnDK(doAnDK2);
            sinhVienRepository.save(sinhVien);
            return doAnDK2.getId();
        }

    }
    @Transactional
    @Override
    public void updateDoAnDK(DoAnModel doAnModel) {
        DoAnDK doAnDK=doAnDKRepository.findDoAnDKByMaDoAn(doAnModel.getMaDoAn());
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAnDK.setUpdateDate(out);
        if(Objects.nonNull(doAnModel.getOpen())){
            doAnDK.setOpen(doAnModel.getOpen());
        }
        if(Objects.nonNull(doAnModel.getMoTa())){
            doAnDK.setMoTa(doAnModel.getMoTa());
        }

        if(Objects.nonNull(doAnModel.getMaGVs())){
            doAnDK.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(doAnModel.getMaGVs().get(0)));
        }

            if(doAnModel.getStatus()){
                doAnDK.setStatus(doAnModel.getStatus());
                doAnDKRepository.save(doAnDK);
                doAnService.createDoAn(doAnDK);
            }
        doAnDKRepository.save(doAnDK);
    }

    @Override
    public void deleteDoAnDK(Integer maDoAn) {
        DoAnDK doAnDK=doAnDKRepository.findDoAnDKByMaDoAn(maDoAn);
        doAnDKRepository.delete(doAnDK);
    }


    @Override
    public DoAnDK convertFromDoAnMau(Integer maDoAnMau) {
        DoAnMau doAnMau=doAnMauRepository.findDoAnMauByMaDoAn(maDoAnMau);
        DoAnDK doAnDK=new DoAnDK();

        doAnDK.setGiaoVien(doAnMau.getGiaoVien());
        doAnDK.setCreateDate(doAnMau.getCreateDate());
        doAnDK.setMoTa(doAnMau.getMoTa());
        doAnDK.setStatus(Boolean.FALSE);
        doAnDK.setOpen(Boolean.FALSE);
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAnDK.setUpdateDate(out);
        return doAnDK;
    }

    @Override
    public Boolean checkDoAnDK(Integer maDoAn,Integer maSV) {
        SinhVien sinhVien=sinhVienRepository.findSinhVienByMaSV(maSV);
        if(Objects.nonNull(sinhVien.getDoAnDK())){
            return Boolean.FALSE;
        }
        DoAnDK doAnDK ;
        if(maDoAn<200000000){
             doAnDK=doAnDKRepository.findDoAnDKByMaDoAn(maDoAn);
            if(Objects.isNull(doAnDK)||doAnDK.getSinhVienList().size()>1 && !doAnDK.getOpen()){
                return Boolean.FALSE;
            }

        }else{
            DoAnMau doAnMau=doAnMauRepository.findDoAnMauByMaDoAn(maDoAn);
            if(Objects.isNull(doAnMau)){
                return  Boolean.FALSE;
            }
        }


        return Boolean.TRUE;
    }

}
