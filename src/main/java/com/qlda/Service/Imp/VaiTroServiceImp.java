package com.qlda.Service.Imp;

import com.qlda.Entity.VaiTro;
import com.qlda.Model.VaiTroModel;
import com.qlda.Repository.VaiTroRepository;
import com.qlda.Service.VaiTroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Service
public class VaiTroServiceImp implements VaiTroService {
    @Autowired
    VaiTroRepository vaiTroRepository;

    public List<VaiTroModel> getAllVaiTro() {
        List<VaiTro> VaiTros=vaiTroRepository.findAll();
        List<VaiTroModel> VaiTroModels=new ArrayList<>();
        for(VaiTro VaiTro:VaiTros){
            VaiTroModel VaiTroModel=parseEntityToModel(VaiTro);
            VaiTroModels.add(VaiTroModel);
        }
        return VaiTroModels;
    }

    @Override
    public VaiTroModel getVaiTroByID(Integer id) {
        VaiTro VaiTro=vaiTroRepository.findVaiTroById(id);
        VaiTroModel VaiTroModel=parseEntityToModel(VaiTro);
        return VaiTroModel;
    }

    @Override
    public VaiTroModel parseEntityToModel(VaiTro VaiTro) {
        VaiTroModel VaiTroModel=new VaiTroModel();
        VaiTroModel.setId(VaiTro.getId());
        VaiTroModel.setTenVaiTro(VaiTro.getTenVaiTro());
        return VaiTroModel;
    }

    @Override
    public VaiTro parseModelToEntity(VaiTroModel VaiTroModel) {
        VaiTro VaiTro=new VaiTro();
        if(Objects.nonNull(VaiTroModel.getId())){
            VaiTro.setId(VaiTroModel.getId());
        }
        VaiTro.setTenVaiTro(VaiTroModel.getTenVaiTro());
        return VaiTro;
    }

    @Override
    public int createVaiTro(VaiTroModel VaiTroModel) {
        VaiTro VaiTro=parseModelToEntity(VaiTroModel);
        VaiTro VaiTro2=vaiTroRepository.save(VaiTro);
        return VaiTro2.getId();
    }

    @Override
    public void updateVaiTro(VaiTroModel VaiTroModel) {
        VaiTro VaiTro=parseModelToEntity(VaiTroModel);
        vaiTroRepository.save(VaiTro);

    }

    @Override
    public void deleteVaiTro(Integer id) {
        VaiTro VaiTro=vaiTroRepository.findVaiTroById(id);
        vaiTroRepository.delete(VaiTro);
    }
}
