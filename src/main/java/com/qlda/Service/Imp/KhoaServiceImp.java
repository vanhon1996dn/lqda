package com.qlda.Service.Imp;

import com.qlda.Entity.Khoa;
import com.qlda.Model.KhoaModel;
import com.qlda.Repository.KhoaRepository;
import com.qlda.Service.KhoaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Service
public class KhoaServiceImp implements KhoaService {
    @Autowired
    KhoaRepository khoaRepository;

    public List<KhoaModel> getAllKhoa() {
        List<Khoa> Khoas=khoaRepository.findAll();
        List<KhoaModel> KhoaModels=new ArrayList<>();
        for(Khoa Khoa:Khoas){
            KhoaModel KhoaModel=parseEntityToModel(Khoa);
            KhoaModels.add(KhoaModel);
        }
        return KhoaModels;
    }

    @Override
    public KhoaModel getKhoaByID(Integer id) {
        Khoa Khoa=khoaRepository.findKhoaById(id);
        KhoaModel KhoaModel=parseEntityToModel(Khoa);
        return KhoaModel;
    }

    @Override
    public KhoaModel parseEntityToModel(Khoa Khoa) {
        KhoaModel khoaModel=new KhoaModel();
        khoaModel.setId(Khoa.getId());
        khoaModel.setTenKhoa(Khoa.getTenKhoa());
        khoaModel.setMaKhoa(Khoa.getMaKhoa());
        return khoaModel;
    }

    @Override
    public Khoa parseModelToEntity(KhoaModel KhoaModel) {
        Khoa khoa=new Khoa();
        if(Objects.nonNull(KhoaModel.getId())){
            khoa.setId(KhoaModel.getId());
        }
        khoa.setTenKhoa(KhoaModel.getTenKhoa());
        khoa.setMaKhoa(KhoaModel.getMaKhoa());
        return khoa;
    }

    @Override
    public int createKhoa(KhoaModel KhoaModel) {
        Khoa Khoa=parseModelToEntity(KhoaModel);
        Khoa Khoa2=khoaRepository.save(Khoa);
        return Khoa2.getId();
    }

    @Override
    public void updateKhoa(KhoaModel KhoaModel) {
        Khoa Khoa=parseModelToEntity(KhoaModel);
        khoaRepository.save(Khoa);

    }

    @Override
    public void deleteKhoa(Integer id) {
        Khoa Khoa=khoaRepository.findKhoaById(id);
        khoaRepository.delete(Khoa);
    }
}
