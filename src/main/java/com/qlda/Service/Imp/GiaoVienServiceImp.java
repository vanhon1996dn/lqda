package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.GiaoVienModel;
import com.qlda.Model.UserModel;
import com.qlda.Repository.*;
import com.qlda.Service.GiaoVienService;

import com.qlda.Service.Useful;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GiaoVienServiceImp implements GiaoVienService {
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    KhoaRepository khoaRepository;
    @Autowired
    ChuyenNganhRepository chuyenNganhRepository;
    @Autowired
    ChucVuRepository chucVuRepository;
    @Autowired
    BangCapRepository bangCapRepository;
    @Autowired
    PhanCongRepository phanCongRepository;
    @Autowired
    PhanCongHDRepository phanCongHDRepository;
    @Autowired
    DotBaoCaoRepository dotBaoCaoRepository;
    @Autowired
    HoiDongRepository hoiDongRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    UserService userService;
    private int num;
    @Override
    public List<GiaoVienModel> getAllGiaoVien() {
        List<GiaoVien> GiaoViens=giaoVienRepository.findAll();
        return parseEntitiesToModels(GiaoViens);
    }

    @Override
    public GiaoVienModel getGiaoVienByMaGV(Integer maGV) {
        GiaoVien GiaoVien=giaoVienRepository.findGiaoVienByMaGV(maGV);
        return parseEntityToModel(GiaoVien);
    }


    @Override
    public Map<Integer, Integer> getMapMaGiaoVien_SLSVHDByKhoaId(Integer khoaId) {
        Map<Integer, Integer> mapGV=new HashMap<>();

        List<GiaoVien> GiaoViens=giaoVienRepository.findGiaoViensByKhoa_Id(khoaId);
        for(GiaoVien giaoVien:GiaoViens){
            int SLSVCoTheHD=giaoVien.getSinhVienToiDa()-getSLSVDaHDByMaGV(giaoVien);
            if(SLSVCoTheHD>0){
                mapGV.put(giaoVien.getMaGV(),SLSVCoTheHD);
            }

        }
        return mapGV;
    }


    @Override
    public Map<Integer, List<Integer>> getMapCNId_MaGVByMaGVs(List<Integer> maGVs) {
        Map<Integer, List<Integer>> mapCNId_MaGV= new HashMap<>();
        List<Integer> maGVDaLuus=new ArrayList<>();
        for(Integer maGV:maGVs){
            GiaoVien giaoVien=giaoVienRepository.findGiaoVienByMaGV(maGV);
            if(mapCNId_MaGV.containsKey(giaoVien.getChuyenNganh().getId())){
                maGVDaLuus=mapCNId_MaGV.get(giaoVien.getChuyenNganh().getId());
                maGVDaLuus.add(maGV);
                mapCNId_MaGV.put(giaoVien.getChuyenNganh().getId(),maGVDaLuus);
            }else{
                maGVDaLuus=new ArrayList<>();
                maGVDaLuus.add(maGV);
                mapCNId_MaGV.put(giaoVien.getChuyenNganh().getId(),maGVDaLuus);
            }
        }
        return mapCNId_MaGV;
    }

    @Override
    public int getSLSVDaHDByMaGV(GiaoVien giaoVien) {
        int SLSVDaHuongDan=0;
        List<DoAn> doAns=giaoVien.getPhanCong().stream().filter(pc->pc.getVaiTro().getId()==1).map(PhanCong::getDoAn).collect(Collectors.toList());

        for (DoAn doAn:doAns){
            SLSVDaHuongDan += doAn.getSinhViens().size();
        }
        return SLSVDaHuongDan;
    }

    @Override
    public void autoAddSinhVienToiDa() {
        List<GiaoVien> GiaoViens=giaoVienRepository.findAll();
        for(GiaoVien giaoVien:GiaoViens){
            giaoVien.setSinhVienToiDa(giaoVien.getChucVu().getDiem()+giaoVien.getBangCap().getDiem());

        }
        giaoVienRepository.save(GiaoViens);
    }



    @Override
    public Integer getMaGVByDotBaoCaoID(Integer DotBaoCaoID) {
        DotBaoCao dotBaoCao=dotBaoCaoRepository.findDotBaoCaoById(DotBaoCaoID);
        Integer maGV=dotBaoCao.getPhanCongs().get(0).getGiaoVien().getMaGV();
        return maGV;
    }

    @Override
    public List<GiaoVienModel> getGiaoViensByKhoaId(Integer khoaId) {
        List<GiaoVien> GiaoViens=giaoVienRepository.findGiaoViensByKhoa_Id(khoaId);
        return parseEntitiesToModels(GiaoViens);
    }

    @Override
    public GiaoVienModel getGiaoVienByDotBaoCaoId(Integer dotBaoCaoId) {
        DotBaoCao dotBaoCao=dotBaoCaoRepository.findDotBaoCaoById(dotBaoCaoId);
        GiaoVien giaoVien= dotBaoCao.getPhanCongs().get(0).getGiaoVien();
        return parseEntityToModel(giaoVien);
    }

    @Override
    public List<GiaoVien> getGiaoVienEntitiesByBangCapIdAndKhoaId(Integer bangCapId, Integer khoaId) {
        List<GiaoVien> giaoViens=giaoVienRepository.findGiaoViensByBangCap_IdGreaterThanEqualAndKhoa_Id(bangCapId,khoaId);
        return giaoViens;
    }

    @Override
    public List<GiaoVien> getGiaoVienEntitiesDaPhanCongHDByKhoaId(Integer khoaId) {
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        List<PhanCongHD> phanCongHDS=phanCongHDRepository.findPhanCongHDSByHoiDong_StartedDateGreaterThanAndHoiDong_KhoaId(out,khoaId);
        return phanCongHDS.stream().map(PhanCongHD::getGiaoVien).collect(Collectors.toList());
    }



    @Override
    public List<GiaoVienModel> getGiaoViensByMaHD(Integer maHD) {
        HoiDong hoiDong=hoiDongRepository.findHoiDongByMaHD(maHD);
        List<GiaoVien> GiaoViens=hoiDong.getPhanCongHDS().stream().map(PhanCongHD::getGiaoVien).collect(Collectors.toList());
        return parseEntitiesToModels(GiaoViens);
    }

    @Override
    public List<GiaoVienModel> getGiaoViensByMaDoAn(Integer maDoAn) {
        List<GiaoVien> GiaoViens=doAnRepository.findDoAnByMaDoAn(maDoAn).getPhanCongs().stream()
                .filter(phanCong -> phanCong.getVaiTro().getId()==1)
                .map(PhanCong::getGiaoVien).collect(Collectors.toList());
        return parseEntitiesToModels(GiaoViens);
    }

    @Override
    public List<GiaoVienModel> parseEntitiesToModels(List<GiaoVien> giaoViens) {
        List<GiaoVienModel> GiaoVienModels=new ArrayList<>();
        for(GiaoVien GiaoVien:giaoViens){
            GiaoVienModel GiaoVienModel=parseEntityToModel(GiaoVien);
            GiaoVienModels.add(GiaoVienModel);
        }
        return GiaoVienModels;
    }

    @Override
    public GiaoVienModel parseEntityToModel(GiaoVien giaoVien) {
        GiaoVienModel giaoVienModel=new GiaoVienModel();
        giaoVienModel.setId(giaoVien.getId());
        giaoVienModel.setMaGV(giaoVien.getMaGV());
        giaoVienModel.setChucVuId(giaoVien.getChucVu().getId());
        giaoVienModel.setBangCapId(giaoVien.getBangCap().getId());
        giaoVienModel.setTenGV(giaoVien.getTenGV());
        giaoVienModel.setNgaySinh(giaoVien.getNgaySinh());
        giaoVienModel.setKhoaId(giaoVien.getKhoa().getId());
        giaoVienModel.setChuyenNganhId(giaoVien.getChuyenNganh().getId());
        if(Objects.nonNull(giaoVien.getSinhVienToiDa())){
            giaoVienModel.setSinhVienToiDa(giaoVien.getSinhVienToiDa());
        }
        return giaoVienModel;
    }

    @Override
    public GiaoVien parseModelToEntity(GiaoVienModel GiaoVienModel) {
        GiaoVien giaoVien=new GiaoVien();
        if(Objects.nonNull(GiaoVienModel.getId())){
            giaoVien.setId(GiaoVienModel.getId());
        }
        if(Objects.nonNull(GiaoVienModel.getMaGV())){
            giaoVien.setMaGV(GiaoVienModel.getMaGV());
        }

        giaoVien.setChucVu(chucVuRepository.findChucVuById(GiaoVienModel.getChucVuId()));
        giaoVien.setBangCap(bangCapRepository.findBangCapById(GiaoVienModel.getBangCapId()));
        giaoVien.setTenGV(GiaoVienModel.getTenGV());
        giaoVien.setNgaySinh(GiaoVienModel.getNgaySinh());
        giaoVien.setKhoa(khoaRepository.findKhoaById(GiaoVienModel.getKhoaId()));
        giaoVien.setChuyenNganh(chuyenNganhRepository.findChuyenNganhById(GiaoVienModel.getChuyenNganhId()));
        if(Objects.nonNull(GiaoVienModel.getSinhVienToiDa())){
            giaoVien.setSinhVienToiDa(GiaoVienModel.getSinhVienToiDa());
        }else{
            giaoVien.setSinhVienToiDa(chucVuRepository.findChucVuById(GiaoVienModel.getChucVuId()).getDiem()+bangCapRepository.findBangCapById(GiaoVienModel.getBangCapId()).getDiem());
        }
        return giaoVien;
    }
    @Transactional
    @Override
    public int createGiaoVien(GiaoVienModel giaoVienModel) {
        GiaoVien giaoVien=parseModelToEntity(giaoVienModel);
        giaoVien.setMaGV(Useful.getMaSoGV(giaoVienModel.getKhoaId()));
        GiaoVien giaoVien2=giaoVienRepository.save(giaoVien);
        UserModel userModel=new UserModel();
        userModel.setUserName(giaoVien.getMaGV().toString());
        userService.createUser(userModel);
        return giaoVien2.getId();
    }
    @Transactional
    @Override
    public void updateGiaoVien(GiaoVienModel GiaoVienModel) {
        GiaoVien GiaoVien=parseModelToEntity(GiaoVienModel);
        giaoVienRepository.save(GiaoVien);

    }

    @Transactional
    @Override
    public void deleteGiaoVien(Integer maGV) {
        GiaoVien GiaoVien=giaoVienRepository.findGiaoVienByMaGV(maGV);
        userService.deleteUser(GiaoVien.getMaGV().toString());
        giaoVienRepository.delete(GiaoVien);
    }

}
