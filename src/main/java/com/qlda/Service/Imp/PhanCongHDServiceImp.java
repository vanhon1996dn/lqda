package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.HoiDongModel;
import com.qlda.Model.PhanCongHDModel;
import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Repository.HoiDongRepository;
import com.qlda.Repository.PhanCongHDRepository;
import com.qlda.Repository.VaiTroHDRepository;
import com.qlda.Service.GiaoVienService;
import com.qlda.Service.PhanCongHDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class PhanCongHDServiceImp implements PhanCongHDService {
    @Autowired
    PhanCongHDRepository phanCongHDRepository;
    @Autowired
    VaiTroHDRepository vaiTroHDRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    HoiDongRepository hoiDongRepository;
    @Override
    public List<PhanCongHDModel> getAllPhanCongHD() {
        List<PhanCongHD> phanCongHDs=phanCongHDRepository.findAll();
        return parseEntitiesToModels(phanCongHDs);
    }

    @Override
    public List<PhanCongHDModel> getPhanCongHDsByMaHD(Integer maHD) {
        HoiDong hoiDong=hoiDongRepository.findHoiDongByMaHD(maHD);
        List<PhanCongHD> phanCongHDS=hoiDong.getPhanCongHDS();
        return parseEntitiesToModels(phanCongHDS);
    }

    @Override
    public PhanCongHDModel getPhanCongHDByID(Integer id) {
        PhanCongHD phanCongHD=phanCongHDRepository.findPhanCongHDById(id);
        PhanCongHDModel phanCongHDModel=parseEntityToModel(phanCongHD);
        return phanCongHDModel;
    }
    @Transactional
    @Override
    public boolean autoPhanCongHD(HoiDongModel hoiDongModel) {
        HoiDong hoiDong=hoiDongRepository.findHoiDongByMaHD(hoiDongModel.getMaHD());
        List<VaiTroHD> vaiTroHDS=vaiTroHDRepository.findAll();
        Random random=new Random();
        List<GiaoVien> giaoViens=new ArrayList<>();
        List<GiaoVien> giaoViensDaPhanCongHD=new ArrayList<>();
        if(Objects.nonNull(hoiDongModel.getCheckTrung())&&!hoiDongModel.getCheckTrung()) {
            giaoViensDaPhanCongHD= giaoVienService.getGiaoVienEntitiesDaPhanCongHDByKhoaId(hoiDong.getKhoa().getId());
        }
        for(VaiTroHD vaiTroHD:vaiTroHDS){
            if(Objects.isNull(phanCongHDRepository.findPhanCongHDByHoiDong_MaHDAndAndVaiTroHD_Id(hoiDongModel.getMaHD(),vaiTroHD.getId()))){
                giaoViens= giaoVienService.getGiaoVienEntitiesByBangCapIdAndKhoaId(vaiTroHD.getDkBangCap().getId(),hoiDong.getKhoa().getId());

                giaoViens.removeAll(giaoViensDaPhanCongHD);
                if(giaoViens.size()==0){
                    return false;
                }else{
                    GiaoVien giaoVien=giaoViens.get(random.nextInt(giaoViens.size()));
                    PhanCongHD phanCongHD=new PhanCongHD();
                    phanCongHD.setHoiDong(hoiDong);
                    phanCongHD.setGiaoVien(giaoVien);
                    phanCongHD.setVaiTroHD(vaiTroHD);
                    phanCongHDRepository.save(phanCongHD);
                    giaoViensDaPhanCongHD.add(giaoVien);
                }
            }

        }
        return true;
    }


    @Override
    public PhanCongHDModel parseEntityToModel(PhanCongHD phanCongHD) {
        PhanCongHDModel phanCongHDModel=new PhanCongHDModel();
        phanCongHDModel.setId(phanCongHD.getId());
        phanCongHDModel.setMaHoiDong(phanCongHD.getHoiDong().getMaHD());
        phanCongHDModel.setTenGV(phanCongHD.getGiaoVien().getTenGV());
        phanCongHDModel.setMaGV(phanCongHD.getGiaoVien().getMaGV());
        phanCongHDModel.setVaiTroHDID(phanCongHD.getVaiTroHD().getId());
        return phanCongHDModel;
    }

    @Override
    public List<PhanCongHDModel> parseEntitiesToModels(List<PhanCongHD> phanCongHDs) {
        List<PhanCongHDModel> phanCongHDModels=new ArrayList<>();
        for(PhanCongHD phanCongHD:phanCongHDs){
            PhanCongHDModel phanCongHDModel=parseEntityToModel(phanCongHD);
            phanCongHDModels.add(phanCongHDModel);
        }
        return phanCongHDModels;
    }

    @Override
    public PhanCongHD parseModelToEntity(PhanCongHDModel phanCongHDModel) {
        PhanCongHD phanCongHD=new PhanCongHD();
        if(Objects.nonNull(phanCongHDModel.getId())){
            phanCongHD.setId(phanCongHDModel.getId());
        }
        phanCongHD.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(phanCongHDModel.getMaGV()));
        phanCongHD.setVaiTroHD(vaiTroHDRepository.findVaiTroHDById(phanCongHDModel.getVaiTroHDID()));
        phanCongHD.setHoiDong(hoiDongRepository.findHoiDongByMaHD(phanCongHDModel.getMaHoiDong()));
        return phanCongHD;
    }

    @Override
    public int createPhanCongHD(PhanCongHDModel phanCongHDModel) {
        PhanCongHD phanCongHD=parseModelToEntity(phanCongHDModel);
        PhanCongHD phanCongHD2=phanCongHDRepository.save(phanCongHD);
        return phanCongHD2.getId();
    }

    @Override
    public void updatePhanCongHD(PhanCongHDModel phanCongHDModel) {
        PhanCongHD phanCongHD=parseModelToEntity(phanCongHDModel);
        phanCongHDRepository.save(phanCongHD);

    }

    @Override
    public void deletePhanCongHD(Integer id) {
        PhanCongHD phanCongHD=phanCongHDRepository.findPhanCongHDById(id);
        phanCongHDRepository.delete(phanCongHD);
    }

    @Override
    public void deletePhanCongHDsByMaHD(Integer maHD) {
        List<PhanCongHD> phanCongHDS=hoiDongRepository.findHoiDongByMaHD(maHD).getPhanCongHDS();
        phanCongHDRepository.delete(phanCongHDS);
    }

    @Override
    public Boolean checkPhanCongHD(PhanCongHDModel phanCongHDModel) {
        HoiDong hoiDong=hoiDongRepository.findHoiDongByMaHD(phanCongHDModel.getMaHoiDong());
        List<PhanCongHD> phanCongHDS=hoiDong.getPhanCongHDS().stream().filter(phanCongHD -> phanCongHD.getVaiTroHD().getId().equals(phanCongHDModel.getVaiTroHDID())).collect(Collectors.toList());
        if(Objects.nonNull(phanCongHDS)&&phanCongHDS.size()>0){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean checkPhanCongHDUpdate(PhanCongHDModel phanCongHDModel) {
        HoiDong hoiDong=hoiDongRepository.findHoiDongByMaHD(phanCongHDModel.getMaHoiDong());
        List<PhanCongHD> phanCongHDS=hoiDong.getPhanCongHDS().stream().filter(phanCongHD -> phanCongHD.getVaiTroHD().getId().equals(phanCongHDModel.getVaiTroHDID())).collect(Collectors.toList());
        if(Objects.nonNull(phanCongHDS)&&phanCongHDS.size()==1&&phanCongHDS.get(0).getId()!=phanCongHDModel.getId()){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }


}
