package com.qlda.Service.Imp;

import com.qlda.Entity.DoAnMau;
import com.qlda.Entity.FileOfDoAnMau;
import com.qlda.Exception.SaveImageException;
import com.qlda.Model.DoAnModel;
import com.qlda.Repository.DoAnMauRepository;
import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Service.DoAnMauService;
import com.qlda.Service.Useful;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class DoAnMauServiceImp implements DoAnMauService {
    static int num;
    @Autowired
    DoAnMauRepository doAnMauRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Value("${images.path}")
    private String UPLOAD_PATH;

    public List<DoAnModel> getAllDoAnMau() {
        List<DoAnMau> doAnMaus=doAnMauRepository.findAll();
        return parseEntitiesToModels(doAnMaus);
    }

    @Override
    public DoAnModel getDoAnMauByID(Integer id) {
        DoAnMau doAnMau=doAnMauRepository.findDoAnMauById(id);
        DoAnModel DoAnModel=parseEntityToModel(doAnMau);
        return DoAnModel;
    }

    @Override
    public List<DoAnModel> getDoAnMausByMaGV(Integer maGV) {
        List<DoAnMau> doAnMaus=doAnMauRepository.findDoAnMausByGiaoVien_MaGV(maGV);
        return parseEntitiesToModels(doAnMaus);
    }

    @Override
    public DoAnModel getDoAnMauByMaDoAn(Integer maDoAn) {
        DoAnMau doAnMau=doAnMauRepository.findDoAnMauByMaDoAn(maDoAn);
        return parseEntityToModel(doAnMau);
    }

    @Override
    public List<DoAnModel> parseEntitiesToModels(List<DoAnMau> doAnMaus) {
        List<DoAnModel> DoAnModels=new ArrayList<>();
        for(DoAnMau doAnMau:doAnMaus){
            DoAnModel DoAnModel=parseEntityToModel(doAnMau);
            DoAnModels.add(DoAnModel);
        }
        return DoAnModels;
    }

    @Override
    public DoAnModel parseEntityToModel(DoAnMau doAnMau) {
        DoAnModel doAnModel=new DoAnModel();
        doAnModel.setId(doAnMau.getId());
        doAnModel.setTieuDe(doAnMau.getTieuDe());
        doAnModel.setMoTa(doAnMau.getMoTa());
        List<Integer> maGVs=new ArrayList<>();
        maGVs.add(doAnMau.getGiaoVien().getMaGV());
        doAnModel.setMaDoAn(doAnMau.getMaDoAn());
        doAnModel.setMaGVs(maGVs);
        doAnModel.setCreateDate(doAnMau.getCreateDate());
        doAnModel.setUpdateDate(doAnMau.getUpdateDate());
        doAnModel.setStatus(doAnMau.getStatus());
        doAnModel.setKhoaId(doAnMau.getGiaoVien().getKhoa().getId());
        doAnModel.setChuyenNganhId(doAnMau.getGiaoVien().getChuyenNganh().getId());
        if(Objects.nonNull(doAnMau.getFileOfDoAnMaus())){
            doAnModel.setFileIds(doAnMau.getFileOfDoAnMaus().stream().map(FileOfDoAnMau::getId).collect(Collectors.toList()));
        }
        if(Objects.nonNull(doAnMau.getImage())){
            doAnModel.setImage(doAnMau.getImage());
        }

        return doAnModel;
    }

    @Override
    public DoAnMau parseModelToEntity(DoAnModel DoAnModel) {
        DoAnMau doAnMau=new DoAnMau();
        if(Objects.nonNull(DoAnModel.getId())){
            doAnMau.setId(DoAnModel.getId());
        }
        if(Objects.nonNull(DoAnModel.getMaDoAn())){
            doAnMau.setMaDoAn(DoAnModel.getMaDoAn());
        }
        if(Objects.nonNull(DoAnModel.getImage())){
            doAnMau.setImage(DoAnModel.getImage());
        }
        doAnMau.setTieuDe(DoAnModel.getTieuDe());
        doAnMau.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(DoAnModel.getMaGVs().get(0)));
        doAnMau.setMoTa(DoAnModel.getMoTa());
        return doAnMau;
    }

    @Override
    public int createDoAnMau(DoAnModel DoAnModel) {
        DoAnMau doAnMau=parseModelToEntity(DoAnModel);
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAnMau.setCreateDate(out);
        doAnMau.setUpdateDate(out);
        doAnMau.setMaDoAn(Useful.getMaDoAnMau(giaoVienRepository.findGiaoVienByMaGV(DoAnModel.getMaGVs().get(0)).getKhoa().getId()));
        doAnMau.setStatus(Boolean.FALSE);
        DoAnMau doAnMau2=doAnMauRepository.save(doAnMau);
        return doAnMau2.getId();
    }

    @Override
    public void updateDoAnMau(DoAnModel DoAnModel) {
        DoAnMau doAnMau=doAnMauRepository.findDoAnMauByMaDoAn(DoAnModel.getMaDoAn());
        Date in = new Date();
        LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.systemDefault());
        Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        doAnMau.setUpdateDate(out);
        if(Objects.nonNull(DoAnModel.getStatus())){
            doAnMau.setStatus(DoAnModel.getStatus());
        }
        if(Objects.nonNull(DoAnModel.getMoTa())){
            doAnMau.setMoTa(DoAnModel.getMoTa());
        }
        if (DoAnModel.getImage() != null){
            String image = saveImage(DoAnModel.getImage());
            doAnMau.setImage(image);
        }
        doAnMauRepository.save(doAnMau);

    }

    @Override
    public void deleteDoAnMau(Integer maDoAn) {
        DoAnMau doAnMau=doAnMauRepository.findDoAnMauByMaDoAn(maDoAn);
        doAnMauRepository.delete(doAnMau);
    }

    private String saveImage(String base64Image) throws SaveImageException {
        String imageName = UUID.randomUUID().toString() + ".jpg";
        BufferedImage image;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(base64Image);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
            File outputfile = new File(UPLOAD_PATH + imageName);
            ImageIO.write(image, "jpg", outputfile);
            return "images/" + imageName;
        } catch (Exception e) {
            throw new SaveImageException("The image is invalid!");
        }
    }
}
