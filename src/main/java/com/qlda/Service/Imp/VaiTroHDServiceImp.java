package com.qlda.Service.Imp;

import com.qlda.Entity.VaiTroHD;
import com.qlda.Model.VaiTroModel;
import com.qlda.Repository.BangCapRepository;
import com.qlda.Repository.VaiTroHDRepository;
import com.qlda.Service.VaiTroHDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Service
public class VaiTroHDServiceImp implements VaiTroHDService {
    @Autowired
    VaiTroHDRepository vaiTroHDRepository;
    @Autowired
    BangCapRepository bangCapRepository;
    public List<VaiTroModel> getAllVaiTroHD() {
        List<VaiTroHD> VaiTroHDs=vaiTroHDRepository.findAll();
        List<VaiTroModel> VaiTroModels=new ArrayList<>();
        for(VaiTroHD VaiTroHD:VaiTroHDs){
            VaiTroModel VaiTroModel=parseEntityToModel(VaiTroHD);
            VaiTroModels.add(VaiTroModel);
        }
        return VaiTroModels;
    }

    @Override
    public VaiTroModel getVaiTroHDByID(Integer id) {
        VaiTroHD VaiTroHD=vaiTroHDRepository.findVaiTroHDById(id);
        VaiTroModel VaiTroModel=parseEntityToModel(VaiTroHD);
        return VaiTroModel;
    }

    @Override
    public VaiTroModel parseEntityToModel(VaiTroHD VaiTroHD) {
        VaiTroModel VaiTroModel=new VaiTroModel();
        VaiTroModel.setId(VaiTroHD.getId());
        VaiTroModel.setTenVaiTro(VaiTroHD.getTenVaiTro());
        VaiTroModel.setdKBangCapId(VaiTroHD.getDkBangCap().getId());
        return VaiTroModel;
    }

    @Override
    public VaiTroHD parseModelToEntity(VaiTroModel VaiTroModel) {
        VaiTroHD VaiTroHD=new VaiTroHD();
        if(Objects.nonNull(VaiTroModel.getId())){
            VaiTroHD.setId(VaiTroModel.getId());
        }
        VaiTroHD.setTenVaiTro(VaiTroModel.getTenVaiTro());
        VaiTroHD.setDkBangCap(bangCapRepository.findBangCapById(VaiTroModel.getdKBangCapId()));
        return VaiTroHD;
    }

    @Override
    public int createVaiTroHD(VaiTroModel VaiTroModel) {
        VaiTroHD VaiTroHD=parseModelToEntity(VaiTroModel);
        VaiTroHD VaiTroHD2=vaiTroHDRepository.save(VaiTroHD);
        return VaiTroHD2.getId();
    }

    @Override
    public void updateVaiTroHD(VaiTroModel VaiTroModel) {
        VaiTroHD VaiTroHD=parseModelToEntity(VaiTroModel);
        vaiTroHDRepository.save(VaiTroHD);

    }

    @Override
    public void deleteVaiTroHD(Integer id) {
        VaiTroHD VaiTroHD=vaiTroHDRepository.findVaiTroHDById(id);
        vaiTroHDRepository.delete(VaiTroHD);
    }
}
