package com.qlda.Service.Imp;

import com.qlda.Entity.DoAn;
import com.qlda.Entity.DoAnDK;
import com.qlda.Entity.GiaoVien;
import com.qlda.Entity.PhanCong;
import com.qlda.Model.DoAnModel;
import com.qlda.Model.GiaoVienModel;
import com.qlda.Model.PhanCongModel;
import com.qlda.Repository.DoAnRepository;
import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Repository.PhanCongRepository;
import com.qlda.Repository.VaiTroRepository;
import com.qlda.Service.DoAnDKService;
import com.qlda.Service.DoAnService;
import com.qlda.Service.GiaoVienService;
import com.qlda.Service.PhanCongService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class PhanCongServiceImp implements PhanCongService {
    @Autowired
    PhanCongRepository phanCongRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    GiaoVienService giaoVienService;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    DoAnService doAnService;
    @Autowired
    DoAnDKService doAnDKService;
    @Autowired
    VaiTroRepository vaiTroRepository;
    @Override
    public List<PhanCongModel> getAllPhanCong() {
        List<PhanCong> phanCongs=phanCongRepository.findAll();
        List<PhanCongModel> phanCongModels=new ArrayList<>();
        for(PhanCong phanCong:phanCongs){
            PhanCongModel phanCongModel=parseEntityToModel(phanCong);
            phanCongModels.add(phanCongModel);
        }
        return phanCongModels;
    }

    @Override
    public PhanCongModel getPhanCongByID(Integer id) {
        PhanCong phanCong=phanCongRepository.findPhanCongById(id);
        PhanCongModel phanCongModel=parseEntityToModel(phanCong);
        return phanCongModel;
    }

    @Override
    public List<PhanCong> getGVHDPhanCongEntitiesByMaDoAN(Integer maDoAn) {
        List<PhanCong> phanCongs=phanCongRepository.findPhanCongsByDoAn_MaDoAn(maDoAn).stream()
                .filter(phanCong -> phanCong.getVaiTro().getId()==1).collect(Collectors.toList());
        return phanCongs;
    }

    @Override
    public List<PhanCongModel> getPhanCongsByDotBaoCaoID(Integer DotBaoCaoID) {
        List<PhanCong> phanCongs=phanCongRepository.findPhanCongsByDotBaoCao_Id(DotBaoCaoID);
        List<PhanCongModel> phanCongModels=new ArrayList<>();
        for(PhanCong phanCong:phanCongs){
            PhanCongModel phanCongModel=parseEntityToModel(phanCong);
            phanCongModels.add(phanCongModel);
        }
        return phanCongModels;
    }

    @Override
    public PhanCongModel parseEntityToModel(PhanCong phanCong) {
        PhanCongModel phanCongModel=new PhanCongModel();
        phanCongModel.setId(phanCong.getId());
        phanCongModel.setMaDoAn(phanCong.getDoAn().getMaDoAn());
        phanCongModel.setTenGV(phanCong.getGiaoVien().getTenGV());
        phanCongModel.setMaGV(phanCong.getGiaoVien().getMaGV());
        phanCongModel.setVaiTro(phanCong.getVaiTro().getTenVaiTro());
        phanCongModel.setVaiTroID(phanCong.getVaiTro().getId());
        return phanCongModel;
    }

    @Override
    public PhanCong parseModelToEntity(PhanCongModel phanCongModel) {
        PhanCong phanCong=new PhanCong();
        if(Objects.nonNull(phanCongModel.getId())){
            phanCong.setId(phanCongModel.getId());
        }
        phanCong.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(phanCongModel.getMaGV()));
        phanCong.setVaiTro(vaiTroRepository.findVaiTroById(phanCongModel.getVaiTroID()));
        phanCong.setDoAn(doAnRepository.findDoAnByMaDoAn(phanCongModel.getMaDoAn()));
        return phanCong;
    }
    @Transactional
    @Override
    public boolean AutoPhanCongGV(Integer khoaId) {
        Map<Integer,Integer> mapMaGV_SLSVHD =giaoVienService.getMapMaGiaoVien_SLSVHDByKhoaId(khoaId);
        List<Integer> listMaGVs = new ArrayList<>(mapMaGV_SLSVHD.keySet());
        Map<Integer,List<Integer>> mapCNId_MaGV=giaoVienService.getMapCNId_MaGVByMaGVs(listMaGVs );

        List<DoAnDK> doAnDKS=doAnDKService.getDoAnDKEntitysChuaPhanCongByKhoaId(khoaId);
        List<DoAnDK> doAnDKConLaiS=new ArrayList<>();
        for(DoAnDK doAnDK:doAnDKS){

            if( (doAnDK.getSinhVienList().get(0).getChuyenNganh().getId() == doAnDK.getGiaoVien().getChuyenNganh().getId())
                    && mapMaGV_SLSVHD.containsKey(doAnDK.getGiaoVien().getMaGV())
                    && mapMaGV_SLSVHD.get(doAnDK.getGiaoVien().getMaGV()) >=doAnDK.getSinhVienList().size() ){
                int SoLuongSVCoTheHD=mapMaGV_SLSVHD.get(doAnDK.getGiaoVien().getMaGV());
                int SLSVConLai=SoLuongSVCoTheHD-doAnDK.getSinhVienList().size();
                if( SLSVConLai<1){
                    Integer chuyenNganhId=doAnDK.getGiaoVien().getChuyenNganh().getId();
                    mapMaGV_SLSVHD.remove(doAnDK.getGiaoVien().getMaGV());
                    List<Integer> maGVs=mapCNId_MaGV.get(chuyenNganhId);
                    maGVs.remove(maGVs.indexOf(doAnDK.getGiaoVien().getMaGV()));
                    if(maGVs.size()>0){
                        mapCNId_MaGV.put(chuyenNganhId,maGVs);
                    }else{
                        mapCNId_MaGV.remove(chuyenNganhId);
                    }
                    doAnService.createDoAn(doAnDK);

                }else {
                    mapMaGV_SLSVHD.put(doAnDK.getGiaoVien().getMaGV(),SLSVConLai);
                    doAnService.createDoAn(doAnDK);
                }

            }else{
                doAnDKConLaiS.add(doAnDK);
            }
        }
        Random random=new Random();
        for(DoAnDK doAnDKConLai:doAnDKConLaiS){
            int SV_chuyenNganhId=doAnDKConLai.getSinhVienList().get(0).getChuyenNganh().getId();
            List<Integer> maGVs2=new ArrayList<>();
            if(mapCNId_MaGV.containsKey(SV_chuyenNganhId)){
                maGVs2=mapCNId_MaGV.get(SV_chuyenNganhId);
            }else{
                maGVs2.addAll(mapMaGV_SLSVHD.keySet());
            }
            Integer maGV=maGVs2.get(random.nextInt(maGVs2.size()));
            int SoLuongSVCoTheHD=mapMaGV_SLSVHD.get(maGV);
            if(mapMaGV_SLSVHD.get(maGV) >= doAnDKConLai.getSinhVienList().size()){
                doAnDKConLai.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(maGV));
                int SLSVConLai=SoLuongSVCoTheHD-doAnDKConLai.getSinhVienList().size();
                if( SLSVConLai<1){
                    Integer Gv_chuyenNganhId=doAnDKConLai.getGiaoVien().getChuyenNganh().getId();
                    mapMaGV_SLSVHD.remove(doAnDKConLai.getGiaoVien().getMaGV());
                    List<Integer> maGVs=mapCNId_MaGV.get(Gv_chuyenNganhId);
                    maGVs.remove(maGVs.indexOf(doAnDKConLai.getGiaoVien().getMaGV()));
                    if(maGVs.size()==0){
                        mapCNId_MaGV.remove(Gv_chuyenNganhId);
                    }else{
                        mapCNId_MaGV.put(Gv_chuyenNganhId,maGVs);
                    }

                }else {
                    mapMaGV_SLSVHD.put(doAnDKConLai.getGiaoVien().getMaGV(),SLSVConLai);
                }
                doAnService.createDoAn(doAnDKConLai);
            }

        }

        return true;
    }
    public int soSanhSoLuongSinhVien(DoAnModel doAn1,DoAnModel doAn2) {
        return doAn1.getMaSVs().size()-doAn2.getMaSVs().size();
    }
    @Override
    public int createPhanCong(PhanCongModel phanCongModel) {
        PhanCong phanCong=parseModelToEntity(phanCongModel);
        PhanCong phanCong2=phanCongRepository.save(phanCong);
        return phanCong2.getId();
    }

    @Override
    public int createPhanCongHuongDanByMaGVAndMaDoAN(Integer maGV, Integer maDoAn) {
        PhanCong phanCong=new PhanCong();
        phanCong.setVaiTro(vaiTroRepository.findVaiTroById(1));
        phanCong.setGiaoVien(giaoVienRepository.findGiaoVienByMaGV(maGV));
        phanCong.setDoAn(doAnRepository.findDoAnByMaDoAn(maDoAn));
        PhanCong phanCong2=phanCongRepository.save(phanCong);
        return phanCong2.getId();
    }

    @Override
    public void updatePhanCong(PhanCongModel phanCongModel) {
        PhanCong phanCong=parseModelToEntity(phanCongModel);
        phanCongRepository.save(phanCong);

    }

    @Override
    public void deletePhanCong(Integer id) {
        PhanCong phanCong=phanCongRepository.findPhanCongById(id);
        phanCongRepository.delete(phanCong);
    }

    @Override
    public Boolean checkPhanCongGVHuongDan(PhanCongModel PhanCongModel) {
        if(PhanCongModel.getVaiTroID()>1){
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public Boolean checkDoAnItHon2GVHD(PhanCongModel PhanCongModel) {
        List<PhanCong> phanCongs=phanCongRepository.findPhanCongsByDoAn_MaDoAn(PhanCongModel.getMaDoAn()).stream()
                .filter(phanCong -> phanCong.getVaiTro().getId()==1).collect(Collectors.toList());
        if(Objects.isNull(phanCongs)||phanCongs.size()<2){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
}
