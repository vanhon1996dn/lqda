package com.qlda.Service.Imp;

import com.qlda.Entity.BangCap;
import com.qlda.Model.BangCapModel;
import com.qlda.Repository.BangCapRepository;
import com.qlda.Service.BangCapService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@Service
public class BangCapServiceImp implements BangCapService {

    @Autowired
    BangCapRepository bangCapRepository;

    public List<BangCapModel> getAllBangCap() {
        List<BangCap> bangCaps=bangCapRepository.findAll();
        List<BangCapModel> bangCapModels=new ArrayList<>();
        for(BangCap bangCap:bangCaps){
            BangCapModel bangCapModel=parseEntityToModel(bangCap);
            bangCapModels.add(bangCapModel);
        }
        return bangCapModels;
    }

    @Override
    public BangCapModel getBangCapByID(Integer id) {
        BangCap bangCap=bangCapRepository.findBangCapById(id);
        BangCapModel bangCapModel=parseEntityToModel(bangCap);
        return bangCapModel;
    }

    @Override
    public BangCapModel parseEntityToModel(BangCap bangCap) {
        BangCapModel bangCapModel=new BangCapModel();
        bangCapModel.setId(bangCap.getId());
        bangCapModel.setTenBC(bangCap.getTenBC());
        bangCapModel.setMaBC(bangCap.getMaBC());
        bangCapModel.setDiem(bangCap.getDiem());
        return bangCapModel;
    }

    @Override
    public BangCap parseModelToEntity(BangCapModel bangCapModel) {
        BangCap bangCap=new BangCap();
        if(Objects.nonNull(bangCapModel.getId())){
            bangCap.setId(bangCapModel.getId());
        }
        bangCap.setTenBC(bangCapModel.getTenBC());
        bangCap.setMaBC(bangCapModel.getMaBC());
        if(Objects.nonNull(bangCapModel.getDiem())){
            bangCap.setDiem(bangCapModel.getDiem());
        }
        return bangCap;
    }

    @Override
    public int createBangCap(BangCapModel bangCapModel) {
        BangCap bangCap=parseModelToEntity(bangCapModel);
        if(Objects.nonNull(bangCap.getDiem())){
            bangCap.setDiem(3);
        }
        BangCap bangCap2=bangCapRepository.save(bangCap);
        return bangCap2.getId();
    }

    @Override
    public void updateBangCap(BangCapModel bangCapModel) {
        BangCap bangCap=parseModelToEntity(bangCapModel);
        BangCap BangCap=bangCapRepository.findBangCapById(bangCapModel.getId());
        if(Objects.nonNull(bangCapModel.getTenBC())){
            BangCap.setTenBC(bangCapModel.getTenBC());
        }
        if(Objects.nonNull(bangCapModel.getDiem())){
            BangCap.setDiem(bangCapModel.getDiem());
        }
        if(Objects.nonNull(bangCapModel.getMaBC())){
            BangCap.setMaBC(bangCapModel.getMaBC());
        }
        bangCapRepository.save(bangCap);

    }

    @Override
    public void deleteBangCap(Integer id) {
        BangCap bangCap=bangCapRepository.findBangCapById(id);
        bangCapRepository.delete(bangCap);
    }

}
