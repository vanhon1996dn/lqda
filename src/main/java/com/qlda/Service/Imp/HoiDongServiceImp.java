package com.qlda.Service.Imp;

import com.qlda.Entity.*;
import com.qlda.Model.HoiDongModel;
import com.qlda.Repository.DoAnRepository;
import com.qlda.Repository.GiaoVienRepository;
import com.qlda.Repository.HoiDongRepository;
import com.qlda.Repository.KhoaRepository;
import com.qlda.Service.GiaoVienService;
import com.qlda.Service.HoiDongService;
import com.qlda.Service.Useful;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class HoiDongServiceImp implements HoiDongService {
    @Autowired
    HoiDongRepository hoiDongRepository;
    @Autowired
    DoAnRepository doAnRepository;
    @Autowired
    GiaoVienRepository giaoVienRepository;
    @Autowired
    KhoaRepository khoaRepository;
    private int num;
    @Override
    public List<HoiDongModel> getAllHoiDong() {
        List<HoiDong> HoiDongs=hoiDongRepository.findAll();
        return parseEntitiesToModels(HoiDongs);
    }

    @Override
    public HoiDongModel getHoiDongByID(Integer id) {
        HoiDong HoiDong=hoiDongRepository.findHoiDongById(id);
        return parseEntityToModel(HoiDong);
    }

    @Override
    public HoiDongModel getHoiDongByMaHD(Integer maHD) {
        HoiDong HoiDong=hoiDongRepository.findHoiDongByMaHD(maHD);
        return parseEntityToModel(HoiDong);
    }

    @Override
    public HoiDongModel getHoiDongByMaDoAn(Integer MaDoAn) {
        HoiDong HoiDong=doAnRepository.findDoAnByMaDoAn(MaDoAn).getHoiDong();
        return parseEntityToModel(HoiDong);
    }

    @Override
    public List<HoiDongModel> getHoiDongsBykhoaId(Integer khoaId) {
        List<HoiDong> hoiDongs=hoiDongRepository.findHoiDongsByKhoa_Id(khoaId);
        return parseEntitiesToModels(hoiDongs);
    }

    @Override
    public List<HoiDongModel> getOpenedHoiDongsBykhoaId(Integer khoaId) {
        List<HoiDong> hoiDongs=hoiDongRepository.findHoiDongsByKhoa_IdAndIsOpenTrue(khoaId);
        return parseEntitiesToModels(hoiDongs);
    }

    @Override
    public List<HoiDongModel> getHoiDongsByMaGV(Integer maGV) {
        Date date=new Date();
        if(date.getMonth()>1){
            date.setMonth(date.getMonth()-1);
        }else{
            date.setMonth(12);
        }
        List<HoiDong> hoiDongs=giaoVienRepository.findGiaoVienByMaGV(maGV).getPhanCongHD().stream()
                .map(PhanCongHD::getHoiDong).filter(hoiDong -> hoiDong.getStartedDate().before(date)).collect(Collectors.toList());
        return parseEntitiesToModels(hoiDongs);
    }

    @Override
    public List<HoiDongModel> parseEntitiesToModels(List<HoiDong> HoiDongs) {
        List<HoiDongModel> HoiDongModels=new ArrayList<>();
        for(HoiDong HoiDong:HoiDongs){
            HoiDongModel HoiDongModel=parseEntityToModel(HoiDong);
            HoiDongModels.add(HoiDongModel);
        }
        return HoiDongModels;
    }


    @Override
    public HoiDongModel parseEntityToModel(HoiDong hoiDong) {
        HoiDongModel hoiDongModel=new HoiDongModel();
        hoiDongModel.setId(hoiDong.getId());
        hoiDongModel.setDiaDiem(hoiDong.getDiaDiem());
        hoiDongModel.setKhoaId(hoiDong.getKhoa().getId());
        hoiDongModel.setMaHD(hoiDong.getMaHD());
        hoiDongModel.setTenHD(hoiDong.getTenHD());
        hoiDongModel.setStartedDate(hoiDong.getStartedDate());
        hoiDongModel.setFinishedDate(hoiDong.getFinishedDate());
        hoiDongModel.setOpen(hoiDong.getOpen());
        if(Objects.nonNull( hoiDong.getDoAns())){
            hoiDongModel.setSoLuongDoAn(hoiDong.getDoAns().size());
        }else{
            hoiDongModel.setSoLuongDoAn(0);
        }
        if(Objects.nonNull( hoiDong.getPhanCongHDS())){
            hoiDongModel.setSoLuongGV(hoiDong.getPhanCongHDS().size());
        }else{
            hoiDongModel.setSoLuongGV(0);
        }
        return hoiDongModel;
    }

    @Override
    public HoiDong parseModelToEntity(HoiDongModel hoiDongModel) {
        HoiDong hoiDong=new HoiDong();
        if(Objects.nonNull(hoiDongModel.getId())){
            hoiDong.setId(hoiDongModel.getId());
        }
        if(Objects.nonNull(hoiDongModel.getMaHD())){
            hoiDong.setMaHD(hoiDongModel.getMaHD());
        }
        if(Objects.nonNull(hoiDongModel.getOpen())){
            hoiDong.setOpen(hoiDongModel.getOpen());
        }
        hoiDong.setTenHD(hoiDongModel.getTenHD());
        hoiDong.setFinishedDate(hoiDongModel.getFinishedDate());
        hoiDong.setStartedDate(hoiDongModel.getStartedDate());
        hoiDong.setKhoa(khoaRepository.findKhoaById(hoiDongModel.getKhoaId()));
        hoiDong.setDiaDiem(hoiDongModel.getDiaDiem());
        return hoiDong;
    }

    @Override
    public int createHoiDong(HoiDongModel HoiDongModel) {
        HoiDong HoiDong=parseModelToEntity(HoiDongModel);
        HoiDong.setMaHD(Useful.getMaSoHD(HoiDongModel.getKhoaId()));
        HoiDong.setOpen(Boolean.TRUE);
        HoiDong HoiDong2=hoiDongRepository.save(HoiDong);
        return HoiDong2.getId();
    }

    @Override
    public void updateHoiDong(HoiDongModel HoiDongModel) {
        HoiDong HoiDong=parseModelToEntity(HoiDongModel);
        hoiDongRepository.save(HoiDong);

    }


    @Override
    public void deleteHoiDong(Integer maHD) {
        HoiDong HoiDong=hoiDongRepository.findHoiDongByMaHD(maHD);
        hoiDongRepository.delete(HoiDong);
    }

    @Override
    public List<Integer> getmaHDMaDoAnCoThePhan(DoAn doAn) {
        List<Integer> maHDs=new ArrayList<>();
        List<HoiDong> hoiDongs=hoiDongRepository.findHoiDongsByKhoa_IdAndIsOpenTrue(doAn.getSinhViens().get(0).getKhoa().getId());
        for(HoiDong hoiDong:hoiDongs){
            List<GiaoVien> giaoViens=hoiDong.getPhanCongHDS().stream().map(PhanCongHD::getGiaoVien).collect(Collectors.toList());
            List<GiaoVien> giaoVienCuaDoAn=doAn.getPhanCongs().stream().filter(phanCong -> phanCong.getVaiTro().getId()==1)
                    .map(PhanCong::getGiaoVien).collect(Collectors.toList());;
            if(giaoViens.contains(giaoVienCuaDoAn.get(0))){
                continue;
            }else{
                maHDs.add(hoiDong.getMaHD());
            }
        }
        return maHDs;
    }

    @Override
    public Boolean checkHDDaPhanCongGVByKhoaId(Integer khoaId) {
        Boolean check=Boolean.TRUE;
        List<HoiDong> hoiDongs=hoiDongRepository.findHoiDongsByKhoa_IdAndIsOpenTrue(khoaId);
        for(HoiDong hoiDong:hoiDongs){
            if(hoiDong.getPhanCongHDS().size()<5){
                check=Boolean.FALSE;
            }
        }
        return check;
    }
}
