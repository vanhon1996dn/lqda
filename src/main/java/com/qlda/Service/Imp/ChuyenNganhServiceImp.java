package com.qlda.Service.Imp;

import com.qlda.Entity.ChuyenNganh;
import com.qlda.Model.ChuyenNganhModel;
import com.qlda.Repository.ChuyenNganhRepository;
import com.qlda.Service.ChuyenNganhService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ChuyenNganhServiceImp implements ChuyenNganhService {

    @Autowired
    ChuyenNganhRepository chuyenNganhRepository;

    public List<ChuyenNganhModel> getAllChuyenNganh() {
        List<ChuyenNganh> chuyenNganhs=chuyenNganhRepository.findAll();
        List<ChuyenNganhModel> chuyenNganhModels=new ArrayList<>();
        for(ChuyenNganh chuyenNganh:chuyenNganhs){
            ChuyenNganhModel chuyenNganhModel=parseEntityToModel(chuyenNganh);
            chuyenNganhModels.add(chuyenNganhModel);
        }
        return chuyenNganhModels;
    }

    @Override
    public ChuyenNganhModel getChuyenNganhByID(Integer id) {
        ChuyenNganh chuyenNganh=chuyenNganhRepository.findChuyenNganhById(id);
        ChuyenNganhModel chuyenNganhModel=parseEntityToModel(chuyenNganh);
        return chuyenNganhModel;
    }

    @Override
    public ChuyenNganhModel parseEntityToModel(ChuyenNganh chuyenNganh) {
        ChuyenNganhModel chuyenNganhModel=new ChuyenNganhModel();
        chuyenNganhModel.setId(chuyenNganh.getId());
        chuyenNganhModel.setTenCN(chuyenNganh.getTenCN());
        return chuyenNganhModel;
    }

    @Override
    public ChuyenNganh parseModelToEntity(ChuyenNganhModel chuyenNganhModel) {
        ChuyenNganh chuyenNganh=new ChuyenNganh();
        if(Objects.nonNull(chuyenNganhModel.getId())){
            chuyenNganh.setId(chuyenNganhModel.getId());
        }
        chuyenNganh.setTenCN(chuyenNganhModel.getTenCN());
        return chuyenNganh;
    }

    @Override
    public int createChuyenNganh(ChuyenNganhModel chuyenNganhModel) {
        ChuyenNganh chuyenNganh=parseModelToEntity(chuyenNganhModel);
        ChuyenNganh chuyenNganh2=chuyenNganhRepository.save(chuyenNganh);
        return chuyenNganh2.getId();
    }

    @Override
    public void updateChuyenNganh(ChuyenNganhModel chuyenNganhModel) {
        ChuyenNganh chuyenNganh=parseModelToEntity(chuyenNganhModel);
        chuyenNganhRepository.save(chuyenNganh);

    }

    @Override
    public void deleteChuyenNganh(Integer id) {
        ChuyenNganh chuyenNganh=chuyenNganhRepository.findChuyenNganhById(id);
        chuyenNganhRepository.delete(chuyenNganh);
    }
}
