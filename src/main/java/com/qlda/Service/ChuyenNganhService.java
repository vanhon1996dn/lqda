package com.qlda.Service;

import com.qlda.Entity.ChuyenNganh;
import com.qlda.Model.ChuyenNganhModel;

import java.util.List;

public interface ChuyenNganhService {
    public List<ChuyenNganhModel> getAllChuyenNganh();
    public ChuyenNganhModel getChuyenNganhByID(Integer id);
    public ChuyenNganhModel parseEntityToModel(ChuyenNganh ChuyenNganh);
    public ChuyenNganh parseModelToEntity(ChuyenNganhModel ChuyenNganhModel);
    public int createChuyenNganh(ChuyenNganhModel ChuyenNganhModel);
    public void updateChuyenNganh(ChuyenNganhModel ChuyenNganhModel);
    public void deleteChuyenNganh(Integer id);
}
