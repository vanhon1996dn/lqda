package com.qlda.Service;

import com.qlda.Entity.DoAnMau;
import com.qlda.Model.DoAnModel;

import java.util.List;

public interface DoAnMauService {
    public List<DoAnModel> getAllDoAnMau();
    public DoAnModel getDoAnMauByID(Integer id);
    public DoAnModel getDoAnMauByMaDoAn(Integer maDoAn);
    public List<DoAnModel> getDoAnMausByMaGV(Integer maGV);
    public List<DoAnModel>  parseEntitiesToModels(List<DoAnMau> doAnMaus);
    public DoAnModel parseEntityToModel(DoAnMau DoAnMau);
    public DoAnMau parseModelToEntity(DoAnModel DoAnModel);
    public int createDoAnMau(DoAnModel DoAnModel);
    public void updateDoAnMau(DoAnModel DoAnModel);
    public void deleteDoAnMau(Integer maDoAn);
}
