package com.qlda.Service;

import com.qlda.Entity.VaiTroHD;
import com.qlda.Model.VaiTroModel;

import java.util.List;

public interface VaiTroHDService {
    public List<VaiTroModel> getAllVaiTroHD();
    public VaiTroModel getVaiTroHDByID(Integer id);
    public VaiTroModel parseEntityToModel(VaiTroHD VaiTroHD);
    public VaiTroHD parseModelToEntity(VaiTroModel VaiTroModel);
    public int createVaiTroHD(VaiTroModel VaiTroModel);
    public void updateVaiTroHD(VaiTroModel VaiTroModel);
    public void deleteVaiTroHD(Integer id);
}
