package com.qlda.Service;

import com.qlda.Entity.DotBaoCao;
import com.qlda.Entity.GiaoVien;
import com.qlda.Model.GiaoVienModel;

import java.util.List;
import java.util.Map;

public interface GiaoVienService {
    public List<GiaoVienModel> getAllGiaoVien();
    public GiaoVienModel getGiaoVienByMaGV(Integer maGV);
    public List<GiaoVienModel> getGiaoViensByKhoaId(Integer khoaId);
    public int getSLSVDaHDByMaGV(GiaoVien giaoVien);
    public Map<Integer,Integer> getMapMaGiaoVien_SLSVHDByKhoaId(Integer khoaId);
    public Map<Integer,List<Integer>> getMapCNId_MaGVByMaGVs(List<Integer> maGVs);
    public GiaoVienModel getGiaoVienByDotBaoCaoId(Integer dotBaoCaoId);
    public List<GiaoVienModel> getGiaoViensByMaHD(Integer maHD);
    public List<GiaoVienModel> getGiaoViensByMaDoAn(Integer maDoAn);
    public  List<GiaoVien> getGiaoVienEntitiesByBangCapIdAndKhoaId(Integer bangCapId,Integer khoaId);
    public  List<GiaoVien> getGiaoVienEntitiesDaPhanCongHDByKhoaId(Integer khoaId);
    public Integer getMaGVByDotBaoCaoID(Integer DotBaoCaoID);
    public List<GiaoVienModel>  parseEntitiesToModels(List<GiaoVien> giaoViens);
    public GiaoVienModel parseEntityToModel(GiaoVien giaoVien);
    public GiaoVien parseModelToEntity(GiaoVienModel giaoVienModel);
    public int createGiaoVien(GiaoVienModel giaoVienModel);
    public void updateGiaoVien(GiaoVienModel giaoVienModel);
    public void deleteGiaoVien(Integer maGV);
    public void autoAddSinhVienToiDa();
}
