package com.qlda.Service;

import com.qlda.Entity.BangCap;
import com.qlda.Model.BangCapModel;

import java.util.List;

public interface BangCapService {
    public List<BangCapModel> getAllBangCap();
    public BangCapModel getBangCapByID(Integer id);
    public BangCapModel parseEntityToModel(BangCap BangCap);
    public BangCap parseModelToEntity(BangCapModel BangCapModel);
    public int createBangCap(BangCapModel BangCapModel);
    public void updateBangCap(BangCapModel BangCapModel);
    public void deleteBangCap(Integer id);
}
