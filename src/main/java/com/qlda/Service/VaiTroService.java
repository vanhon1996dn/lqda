package com.qlda.Service;

import com.qlda.Entity.VaiTro;
import com.qlda.Model.VaiTroModel;

import java.util.List;

public interface VaiTroService {
    public List<VaiTroModel> getAllVaiTro();
    public VaiTroModel getVaiTroByID(Integer id);
    public VaiTroModel parseEntityToModel(VaiTro VaiTro);
    public VaiTro parseModelToEntity(VaiTroModel VaiTroModel);
    public int createVaiTro(VaiTroModel VaiTroModel);
    public void updateVaiTro(VaiTroModel VaiTroModel);
    public void deleteVaiTro(Integer id);
}
