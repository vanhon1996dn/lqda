package com.qlda.Service;

import com.qlda.Entity.ChucVu;
import com.qlda.Model.ChucVuModel;

import java.util.List;

public interface ChucVuService {
    public List<ChucVuModel> getAllChucVu();
    public ChucVuModel getChucVuByID(Integer id);
    public ChucVuModel parseEntityToModel(ChucVu ChucVu);
    public ChucVu parseModelToEntity(ChucVuModel ChucVuModel);
    public int createChucVu(ChucVuModel ChucVuModel);
    public void updateChucVu(ChucVuModel ChucVuModel);
    public void deleteChucVu(Integer id);
}
