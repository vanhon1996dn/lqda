package com.qlda.Controller;

import com.qlda.Model.VaiTroModel;
import com.qlda.Service.UserService;
import com.qlda.Service.VaiTroHDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class VaiTroHDController {
    @Autowired
    private VaiTroHDService VaiTroHDService;
    @Autowired
    private UserService userService;
    @GetMapping("/vthd")
    private ResponseEntity getAllVaiTroHD(){
        List<VaiTroModel> VaiTroHDList=VaiTroHDService.getAllVaiTroHD();
        return new ResponseEntity(VaiTroHDList, HttpStatus.CREATED);

    }
    @GetMapping("/vthd/{id}")
    private ResponseEntity getVaiTroHD(@PathVariable(value = "id") int VaiTroHDID){
        VaiTroModel VaiTroHD=VaiTroHDService.getVaiTroHDByID(VaiTroHDID);
        return new ResponseEntity(VaiTroHD,HttpStatus.CREATED);
    }

    @PostMapping("/vthd/create")
    public ResponseEntity createVaiTroHD(@RequestBody VaiTroModel VaiTroModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int VaiTroHDID = VaiTroHDService.createVaiTroHD(VaiTroModel);
            return new ResponseEntity(VaiTroHDID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/vthd/update")
    public ResponseEntity updateVaiTroHD(@RequestBody  VaiTroModel VaiTroModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            VaiTroHDService.updateVaiTroHD(VaiTroModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/vthd/delete/{id}")
    public ResponseEntity deleteVaiTroHD(@PathVariable(value = "id") int VaiTroHDID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            VaiTroHDService.deleteVaiTroHD(VaiTroHDID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
