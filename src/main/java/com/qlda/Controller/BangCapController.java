package com.qlda.Controller;

import com.qlda.Model.BangCapModel;
import com.qlda.Service.BangCapService;
import com.qlda.Service.ChucVuService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class BangCapController {
    @Autowired
    private  BangCapService bangCapService;
    @Autowired
    private UserService userService;
    @GetMapping("/bc")
    private ResponseEntity getAllBangCap( ){
            List<BangCapModel> BangCapList=bangCapService.getAllBangCap();
            return new ResponseEntity(BangCapList, HttpStatus.OK);
    }
    @GetMapping("/bc/{id}")
    private ResponseEntity getBangCap(@PathVariable(value = "id") int BangCapID,@RequestHeader("Authorization") String token){
        BangCapModel BangCap=bangCapService.getBangCapByID(BangCapID);
        return new ResponseEntity(BangCap,HttpStatus.OK);
    }

    @PostMapping("/bc/create")
    public ResponseEntity createBangCap(@RequestBody BangCapModel BangCapModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int BangCapID = bangCapService.createBangCap(BangCapModel);
            return new ResponseEntity(BangCapID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/bc/update")
    public ResponseEntity updateBangCap(@RequestBody  BangCapModel BangCapModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            bangCapService.updateBangCap(BangCapModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
    }
    @DeleteMapping("/bc/delete/{id}")
    public ResponseEntity deleteBangCap(@PathVariable(value = "id") int BangCapID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
        bangCapService.deleteBangCap(BangCapID);
        return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
}
