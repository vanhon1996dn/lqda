package com.qlda.Controller;

import com.qlda.Entity.PhanCong;
import com.qlda.Model.PhanCongModel;
import com.qlda.Service.PhanCongHDService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class PhanCongController {
    @Autowired
    private com.qlda.Service.PhanCongService PhanCongService;
    @Autowired
    private UserService userService;
    @GetMapping("/pc")
    private ResponseEntity getAllPhanCong(){
        List<PhanCongModel> PhanCongList=PhanCongService.getAllPhanCong();
        return new ResponseEntity(PhanCongList, HttpStatus.OK);

    }
    @GetMapping("/pc/{id}")
    private ResponseEntity getPhanCong(@PathVariable(value = "id") int PhanCongID){
        PhanCongModel PhanCong=PhanCongService.getPhanCongByID(PhanCongID);
        return new ResponseEntity(PhanCong,HttpStatus.OK);
    }
    @GetMapping("/pc/dbc/{id}")
    private ResponseEntity getPhanCongByDotBaoCaoId(@PathVariable(value = "id") int DotBaoCaoId){
        List<PhanCongModel> phanCongModels=PhanCongService.getPhanCongsByDotBaoCaoID(DotBaoCaoId);
        return new ResponseEntity(phanCongModels,HttpStatus.OK);
    }
    @GetMapping("/pc/auto/k/{id}")
    private ResponseEntity AutoPhanCongGV(@PathVariable(value = "id") int khoaId){
        PhanCongService.AutoPhanCongGV(khoaId);
        return new ResponseEntity(HttpStatus.OK);
    }
//    @PostMapping("/pc/create")
//    public ResponseEntity createPhanCong(@RequestBody PhanCongModel PhanCongModel,@RequestHeader("Authorization") String token){
//        List<String> authorities=userService.getAuthoritiesByToken(token);
//        if(authorities.contains("admin")) {
//            if(PhanCongService.checkPhanCongGVHuongDan(PhanCongModel)&&PhanCongService.checkDoAnItHon2GVHD(PhanCongModel)){
//                int PhanCongID = PhanCongService.createPhanCong(PhanCongModel);
//                return new ResponseEntity(PhanCongID ,HttpStatus.CREATED);
//            }else{
//                return new ResponseEntity(HttpStatus.BAD_REQUEST);
//            }
//
//        }else{
//            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
//        }
//
//    }
//    @PutMapping("/pc/update")
//    public ResponseEntity updatePhanCong(@RequestBody  PhanCongModel PhanCongModel,@RequestHeader("Authorization") String token) {
//        List<String> authorities=userService.getAuthoritiesByToken(token);
//        if(authorities.contains("admin")) {
//            if (PhanCongService.checkPhanCongGVHuongDan(PhanCongModel)) {
//                PhanCongService.updatePhanCong(PhanCongModel);
//                return new ResponseEntity(HttpStatus.OK);
//
//            } else{
//                return new ResponseEntity(HttpStatus.BAD_REQUEST);
//            }
//
//        }else {
//            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
//        }
//    }
    @DeleteMapping("/pc/delete/{id}")
    public ResponseEntity deletePhanCong(@PathVariable(value = "id") int PhanCongID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            PhanCongService.deletePhanCong(PhanCongID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
