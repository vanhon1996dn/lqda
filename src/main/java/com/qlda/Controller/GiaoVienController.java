package com.qlda.Controller;

import com.qlda.Model.GiaoVienModel;
import com.qlda.Service.GiaoVienService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class GiaoVienController {
    @Autowired
    private com.qlda.Service.GiaoVienService GiaoVienService;
    @Autowired
    private UserService userService;
    @GetMapping("/gv")
    private ResponseEntity getAllGiaoVien(){
        List<GiaoVienModel> GiaoVienList=GiaoVienService.getAllGiaoVien();
        return new ResponseEntity(GiaoVienList, HttpStatus.OK);

    }
    @GetMapping("/gv/{maGV}")
    private ResponseEntity<GiaoVienModel> getGiaoVien(@PathVariable(value = "maGV") int maGV){
        GiaoVienModel giaoVien=GiaoVienService.getGiaoVienByMaGV(maGV);
        return new ResponseEntity<>(giaoVien,HttpStatus.OK);
    }
    @GetMapping("/gv/k/{id}")
    private ResponseEntity getGiaoViensByKhoaId(@PathVariable(value = "id") int khoaID){
        List<GiaoVienModel>  giaoVien=GiaoVienService.getGiaoViensByKhoaId(khoaID);
        return new ResponseEntity(giaoVien,HttpStatus.OK);
    }
    @GetMapping("/gv/dbc/{id}")
    private ResponseEntity getGiaoVienByDotBaoCaoId(@PathVariable(value = "id") int dotBaoCaoID){
        GiaoVienModel giaoVien=GiaoVienService.getGiaoVienByDotBaoCaoId(dotBaoCaoID);
        return new ResponseEntity(giaoVien,HttpStatus.OK);
    }
    @GetMapping("/gv/hd/{maHD}")
    private ResponseEntity getGiaoVienBymaHD(@PathVariable(value = "maHD") int maHD){
        List<GiaoVienModel>  giaoViens=GiaoVienService.getGiaoViensByMaHD(maHD);
        return new ResponseEntity(giaoViens,HttpStatus.OK);
    }
    @GetMapping("/gv/da/{maDoAn}")
    private ResponseEntity getGiaoVienBymaDoAn(@PathVariable(value = "maDoAn") int maDoAn) {
        List<GiaoVienModel> giaoViens = GiaoVienService.getGiaoViensByMaDoAn(maDoAn);
        return new ResponseEntity(giaoViens, HttpStatus.OK);
    }
        @GetMapping("/gv/slsvtd")
    private ResponseEntity GetSoLuongSinhVienToiDa(){
        GiaoVienService.autoAddSinhVienToiDa();
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/gv/create")
    public ResponseEntity createGiaoVien(@RequestBody GiaoVienModel GiaoVienModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int GiaoVienID = GiaoVienService.createGiaoVien(GiaoVienModel);
            return new ResponseEntity(GiaoVienID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/gv/update")
    public ResponseEntity updateGiaoVien(@RequestBody  GiaoVienModel GiaoVienModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        if(!username.equals(GiaoVienModel.getMaGV().toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("giaovien")) {
            GiaoVienService.updateGiaoVien(GiaoVienModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/gv/delete/{maGV}")
    public ResponseEntity deleteGiaoVien(@PathVariable(value = "maGV") int maGV,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            GiaoVienService.deleteGiaoVien(maGV);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
