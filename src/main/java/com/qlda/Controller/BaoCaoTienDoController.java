package com.qlda.Controller;

import com.qlda.Model.BaoCaoTienDoModel;
import com.qlda.Service.BaoCaoTienDoService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class BaoCaoTienDoController {
    @Autowired


    private BaoCaoTienDoService BaoCaoTienDoService;
    @Autowired
    private UserService userService;
    @GetMapping("/bctd")
    private ResponseEntity getAllBaoCaoTienDo( ){
        List<BaoCaoTienDoModel> BaoCaoTienDoList=BaoCaoTienDoService.getAllBaoCaoTienDo();
        return new ResponseEntity(BaoCaoTienDoList, HttpStatus.OK);
    }
    @GetMapping("/bctd/{id}")
    private ResponseEntity getBaoCaoTienDo(@PathVariable(value = "id") int BaoCaoTienDoID, @RequestHeader("Authorization") String token){
        BaoCaoTienDoModel BaoCaoTienDo=BaoCaoTienDoService.getBaoCaoTienDoByID(BaoCaoTienDoID);
        return new ResponseEntity(BaoCaoTienDo,HttpStatus.OK);
    }

    @PostMapping("/bctd/create")
    public ResponseEntity createBaoCaoTienDo(@RequestBody BaoCaoTienDoModel BaoCaoTienDoModel, @RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int BaoCaoTienDoID = BaoCaoTienDoService.createBaoCaoTienDo(BaoCaoTienDoModel);
            return new ResponseEntity(BaoCaoTienDoID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/bctd/update")
    public ResponseEntity updateBaoCaoTienDo(@RequestBody  BaoCaoTienDoModel BaoCaoTienDoModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            BaoCaoTienDoService.updateBaoCaoTienDo(BaoCaoTienDoModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
    @DeleteMapping("/bctd/delete/{id}")
    public ResponseEntity deleteBaoCaoTienDo(@PathVariable(value = "id") int BaoCaoTienDoID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            BaoCaoTienDoService.deleteBaoCaoTienDo(BaoCaoTienDoID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
}
