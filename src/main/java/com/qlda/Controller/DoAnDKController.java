package com.qlda.Controller;

import com.qlda.Entity.DoAnDK;
import com.qlda.Model.DoAnModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class DoAnDKController {
    @Autowired
    private com.qlda.Service.DoAnDKService DoAnDKService;
    @Autowired
    private UserService userService;
    @GetMapping("/dadk")
    private ResponseEntity getAllDoAnDK(){
        List<DoAnModel> DoAnDKList=DoAnDKService.getAllDoAnDK();
        return new ResponseEntity(DoAnDKList, HttpStatus.OK);

    }
    @GetMapping("/dadk/{maDoAnDK}")
    private ResponseEntity getDoAnDKByMaDoAn(@PathVariable(value = "maDoAnDK") int maDoAnDK){
        DoAnModel DoAnDK=DoAnDKService.getDoAnDKByMaDoAn(maDoAnDK);
        return new ResponseEntity(DoAnDK,HttpStatus.OK);
    }
    @GetMapping("/dadk/sv/{maSV}")
    private ResponseEntity getDoAnDKByMaSV(@PathVariable(value = "maSV") int maSV){
        DoAnModel DoAnDK=DoAnDKService.getDoAnDKByMaSV(maSV);
        return new ResponseEntity(DoAnDK,HttpStatus.OK);
    }

    @PostMapping("/dadk/create")
    public ResponseEntity createDoAnDK(@RequestBody DoAnModel DoAnModel,@RequestHeader("Authorization") String token){
        String username=userService.getUserNameByToken(token);
        if(!username.equals(DoAnModel.getMaSVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("sinhvien")) {
            if(Objects.isNull(DoAnModel.getMaDoAn())|| DoAnDKService.checkDoAnDK(DoAnModel.getMaDoAn(),DoAnModel.getMaSVs().get(0))){
                int DoAnDKID = DoAnDKService.createDoAnDK(DoAnModel);
                return new ResponseEntity(DoAnDKID ,HttpStatus.CREATED);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/dadk/update")
    public ResponseEntity updateDoAnDK(@RequestBody  DoAnModel DoAnModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        if(!username.equals(DoAnModel.getMaSVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("sinhvien")) {
            DoAnDKService.updateDoAnDK(DoAnModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/dadk/delete/{maDoAnDK}")
    public ResponseEntity deleteDoAnDK(@PathVariable(value = "maDoAnDK") int maDoAnDK,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        DoAnModel doAnDKs=DoAnDKService.getDoAnDKByMaDoAn(maDoAnDK);
        if(!username.equals(doAnDKs.getMaSVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("sinhvien")) {
            DoAnDKService.deleteDoAnDK(maDoAnDK);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
}
