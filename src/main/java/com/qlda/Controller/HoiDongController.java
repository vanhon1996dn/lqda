package com.qlda.Controller;

import com.qlda.Model.HoiDongModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class HoiDongController {
    @Autowired
    private com.qlda.Service.HoiDongService HoiDongService;
    @Autowired
    private UserService userService;
    @GetMapping("/hd")
    private ResponseEntity getAllHoiDong(){
        List<HoiDongModel> HoiDongList=HoiDongService.getAllHoiDong();
        return new ResponseEntity(HoiDongList, HttpStatus.OK);

    }
    @GetMapping("/hd/{maHD}")
    private ResponseEntity getHoiDong(@PathVariable(value = "maHD") int maHD){
        HoiDongModel HoiDong=HoiDongService.getHoiDongByMaHD(maHD);
        return new ResponseEntity(HoiDong,HttpStatus.OK);
    }
    @GetMapping("/hd/gv/{maGV}")
    private ResponseEntity getHoiDongByMaGV(@PathVariable(value = "maGV") int maGV){
        List<HoiDongModel> HoiDongs=HoiDongService.getHoiDongsByMaGV(maGV);
        return new ResponseEntity(HoiDongs,HttpStatus.OK);
    }
    @GetMapping("/hd/da/{maDoAn}")
    private ResponseEntity getHoiDongByMaDoAn(@PathVariable(value = "maDoAn") int maDoAn){
        HoiDongModel HoiDong=HoiDongService.getHoiDongByMaDoAn(maDoAn);
        return new ResponseEntity(HoiDong,HttpStatus.OK);
    }


    @PostMapping("/hd/create")
    public ResponseEntity createHoiDong(@RequestBody HoiDongModel HoiDongModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int HoiDongID = HoiDongService.createHoiDong(HoiDongModel);
            return new ResponseEntity(HoiDongID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/hd/update")
    public ResponseEntity updateHoiDong(@RequestBody  HoiDongModel HoiDongModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            HoiDongService.updateHoiDong(HoiDongModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/hd/delete/{maHD}")
    public ResponseEntity deleteHoiDong(@PathVariable(value = "maHD") int maHD,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            HoiDongService.deleteHoiDong(maHD);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
