package com.qlda.Controller;

import com.qlda.Model.KhoaModel;
import com.qlda.Service.KhoaService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class KhoaController {
    @Autowired
    private com.qlda.Service.KhoaService KhoaService;
    @Autowired
    private UserService userService;
    @GetMapping("/k")
    private ResponseEntity getAllKhoa(){
        List<KhoaModel> KhoaList=KhoaService.getAllKhoa();
        return new ResponseEntity(KhoaList, HttpStatus.OK);

    }
    @GetMapping("/k/{id}")
    private ResponseEntity getKhoa(@PathVariable(value = "id") int KhoaID){
        KhoaModel Khoa=KhoaService.getKhoaByID(KhoaID);
        return new ResponseEntity(Khoa,HttpStatus.OK);
    }

    @PostMapping("/k/create")
    public ResponseEntity createKhoa(@RequestBody KhoaModel KhoaModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int KhoaID = KhoaService.createKhoa(KhoaModel);
            return new ResponseEntity(KhoaID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/k/update")
    public ResponseEntity updateKhoa(@RequestBody  KhoaModel KhoaModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            KhoaService.updateKhoa(KhoaModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/k/delete/{id}")
    public ResponseEntity deleteKhoa(@PathVariable(value = "id") int KhoaID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            KhoaService.deleteKhoa(KhoaID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
