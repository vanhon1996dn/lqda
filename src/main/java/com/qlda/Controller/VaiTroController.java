package com.qlda.Controller;

import com.qlda.Model.VaiTroModel;
import com.qlda.Service.UserService;
import com.qlda.Service.VaiTroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class VaiTroController {
    @Autowired
    private VaiTroService VaiTroService;
    @Autowired
    private UserService userService;
    @GetMapping("/vt")
    private ResponseEntity getAllVaiTro(){
        List<VaiTroModel> VaiTroList=VaiTroService.getAllVaiTro();
        return new ResponseEntity(VaiTroList, HttpStatus.CREATED);

    }
    @GetMapping("/vt/{id}")
    private ResponseEntity getVaiTro(@PathVariable(value = "id") int VaiTroID){
        VaiTroModel VaiTro=VaiTroService.getVaiTroByID(VaiTroID);
        return new ResponseEntity(VaiTro,HttpStatus.CREATED);
    }

    @PostMapping("/vt/create")
    public ResponseEntity createVaiTro(@RequestBody VaiTroModel VaiTroModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int VaiTroID = VaiTroService.createVaiTro(VaiTroModel);
            return new ResponseEntity(VaiTroID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/vt/update")
    public ResponseEntity updateVaiTro(@RequestBody  VaiTroModel VaiTroModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            VaiTroService.updateVaiTro(VaiTroModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/vt/delete/{id}")
    public ResponseEntity deleteVaiTro(@PathVariable(value = "id") int VaiTroID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            VaiTroService.deleteVaiTro(VaiTroID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
