package com.qlda.Controller;

import com.qlda.Model.UserModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/tk")
    private ResponseEntity getAllTaiKhoan(){
        List<UserModel> taiKhoanList=userService.getAllUser();
        return new ResponseEntity(taiKhoanList, HttpStatus.CREATED);

    }
    @GetMapping("/tk/{id}")
    private ResponseEntity getTaiKhoan(@PathVariable(value = "id") int ID){
        UserModel taiKhoan=userService.getUserByID(ID);
        return new ResponseEntity(taiKhoan,HttpStatus.CREATED);
    }

    @PostMapping("/tk/create")
    public ResponseEntity createTaiKhoan(@RequestBody UserModel userModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int taiKhoanID = userService.createUserByAdmin(userModel);
            return new ResponseEntity(taiKhoanID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/tk/admin/update")
    public ResponseEntity updateTaiKhoanByAdmin(@RequestBody  UserModel userModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")) {
                userService.updateUser(userModel);
                return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @PutMapping("/tk/update")
    public ResponseEntity updateTaiKhoan(@RequestBody  UserModel userModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        if(!username.equals(userModel.getUserName())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.size()>0) {
            if(userService.checkUser(userModel)){
                userService.updateUser(userModel);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/tk/delete/{name}")
    public ResponseEntity deletetaiKhoan(@PathVariable(value = "name") String name,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            userService.deleteUser(name);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
