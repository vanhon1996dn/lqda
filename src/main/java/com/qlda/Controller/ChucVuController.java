package com.qlda.Controller;

import com.qlda.Model.ChucVuModel;
import com.qlda.Service.ChucVuService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class ChucVuController {
    @Autowired
    private com.qlda.Service.ChucVuService ChucVuService;
    @Autowired
    private UserService userService;
    @GetMapping("/cv")
    private ResponseEntity getAllChucVu(){
        List<ChucVuModel> ChucVuList=ChucVuService.getAllChucVu();
        return new ResponseEntity(ChucVuList, HttpStatus.OK);

    }
    @GetMapping("/cv/{id}")
    private ResponseEntity<ChucVuModel> getChucVu(@PathVariable(value = "id") int ChucVuID){
        ChucVuModel ChucVu=ChucVuService.getChucVuByID(ChucVuID);
        return new ResponseEntity<>(ChucVu,HttpStatus.OK);
    }

    @PostMapping("/cv/create")
    public ResponseEntity createChucVu(@RequestBody ChucVuModel ChucVuModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int ChucVuID = ChucVuService.createChucVu(ChucVuModel);
            return new ResponseEntity(ChucVuID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/cv/update")
    public ResponseEntity updateChucVu(@RequestBody  ChucVuModel ChucVuModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            ChucVuService.updateChucVu(ChucVuModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/cv/delete/{id}")
    public ResponseEntity deleteChucVu(@PathVariable(value = "id") int ChucVuID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            ChucVuService.deleteChucVu(ChucVuID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
}
