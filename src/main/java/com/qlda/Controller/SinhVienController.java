package com.qlda.Controller;

import com.qlda.Model.SinhVienModel;
import com.qlda.Service.SinhVienService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class SinhVienController {
    @Autowired
    private SinhVienService SinhVienService;
    @Autowired
    private UserService userService;
    @GetMapping("/sv")
    private ResponseEntity getAllSinhVien(){
        List<SinhVienModel> SinhVienList=SinhVienService.getAllSinhVien();
        return new ResponseEntity(SinhVienList, HttpStatus.OK);

    }
    @GetMapping("/sv/{maSV}")
    private ResponseEntity getSinhVien(@PathVariable(value = "maSV") int maSV){
        SinhVienModel SinhVien=SinhVienService.getSinhVienByMaSV(maSV);
        return new ResponseEntity(SinhVien,HttpStatus.OK);
    }
    @GetMapping("/sv/da/{maDoAn}")
    private ResponseEntity getSinhViensByMaDoAn(@PathVariable(value = "maDoAn") int maDoAn){
        List<SinhVienModel> SinhVienList=SinhVienService.getSinhVienModelsByMaDoAn(maDoAn);
        return new ResponseEntity(SinhVienList,HttpStatus.OK);
    }

    @PostMapping("/sv/create")
    public ResponseEntity createSinhVien(@RequestBody SinhVienModel SinhVienModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int SinhVienID = SinhVienService.createSinhVien(SinhVienModel);
            return new ResponseEntity(SinhVienID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }

    @PutMapping("/sv/update")
    public ResponseEntity updateSinhVien(@RequestBody  SinhVienModel SinhVienModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        if(!username.equals(SinhVienModel.getMaSV().toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("sinhvien")) {
            SinhVienService.updateSinhVien(SinhVienModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/sv/delete/{maSV}")
    public ResponseEntity deleteSinhVien(@PathVariable(value = "maSV") int maSV,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            SinhVienService.deleteSinhVien(maSV);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
}
