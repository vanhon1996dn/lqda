package com.qlda.Controller;

import com.qlda.Model.ChuyenNganhModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class ChuyenNganhController {
    @Autowired
    private com.qlda.Service.ChuyenNganhService ChuyenNganhService;
    @Autowired
    private UserService userService;
    @GetMapping("/cn")
    private ResponseEntity getAllChuyenNganh(){
        List<ChuyenNganhModel> ChuyenNganhList=ChuyenNganhService.getAllChuyenNganh();
        return new ResponseEntity(ChuyenNganhList, HttpStatus.OK);

    }
    @GetMapping("/cn/{id}")
    private ResponseEntity getChuyenNganh(@PathVariable(value = "id") int ChuyenNganhID){
        ChuyenNganhModel ChuyenNganh=ChuyenNganhService.getChuyenNganhByID(ChuyenNganhID);
        return new ResponseEntity(ChuyenNganh,HttpStatus.OK);
    }

    @PostMapping("/cn/create")
    public ResponseEntity createChuyenNganh(@RequestBody ChuyenNganhModel ChuyenNganhModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            int ChuyenNganhID = ChuyenNganhService.createChuyenNganh(ChuyenNganhModel);
            return new ResponseEntity(ChuyenNganhID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/cn/update")
    public ResponseEntity updateChuyenNganh(@RequestBody  ChuyenNganhModel ChuyenNganhModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            ChuyenNganhService.updateChuyenNganh(ChuyenNganhModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/cn/delete/{id}")
    public ResponseEntity deleteChuyenNganh(@PathVariable(value = "id") int ChuyenNganhID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            ChuyenNganhService.deleteChuyenNganh(ChuyenNganhID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
}
