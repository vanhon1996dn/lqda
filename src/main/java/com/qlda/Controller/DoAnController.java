package com.qlda.Controller;

import com.qlda.Model.DoAnModel;
import com.qlda.Service.HoiDongService;
import com.qlda.Service.PhanCongHDService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class DoAnController {
    @Autowired
    private com.qlda.Service.DoAnService DoAnService;
    @Autowired
    private HoiDongService hoiDongService;
    @Autowired
    private UserService userService;
    @GetMapping("/da")
    private ResponseEntity getAllDoAn(){
        List<DoAnModel> DoAnList=DoAnService.getAllDoAn();
        return new ResponseEntity(DoAnList, HttpStatus.OK);

    }
    @GetMapping("/da/{maDA}")
    private ResponseEntity getDoAn(@PathVariable(value = "maDA") int maDoAn){
        DoAnModel DoAn=DoAnService.getDoAnByMaDoAn(maDoAn);
        return new ResponseEntity(DoAn,HttpStatus.OK);
    }
    @GetMapping("/da/sv/{maSV}")
    private ResponseEntity getDoAnByMaSV(@PathVariable(value = "maSV") int maSV){
        DoAnModel doAnModel=DoAnService.getDoAnByMaSV(maSV);
        return new ResponseEntity(doAnModel,HttpStatus.OK);
    }
    @GetMapping("/da/gv/{maGV}")
    private ResponseEntity getDoAnsByMaGV(@PathVariable(value = "maGV") int maGV){
        List<DoAnModel> doAnModels=DoAnService.getDoAnsByMaGV(maGV);
        return new ResponseEntity(doAnModels,HttpStatus.OK);
    }
    @GetMapping("/da/hd/{maHD}")
    private ResponseEntity getDoAnsByMaHoiDong(@PathVariable(value = "maHD") int maHD){
        List<DoAnModel> DoAnList=DoAnService.getDoAnsByMaHD(maHD);
        return new ResponseEntity(DoAnList,HttpStatus.OK);
    }
    @GetMapping("/da/k/{id}")
    private ResponseEntity getDoAnsByKhoaId(@PathVariable(value = "id") int khoaId){
        List<DoAnModel> DoAnList=DoAnService.getDoAnsByKhoaId(khoaId);
        return new ResponseEntity(DoAnList,HttpStatus.OK);
    }
    @GetMapping("/da/autoPhanCongHD/k/{id}")
    private ResponseEntity autoPhanCongHD(@PathVariable(value = "id") int khoaId){
        Boolean checkHDs=hoiDongService.checkHDDaPhanCongGVByKhoaId(khoaId);
        if(checkHDs){
            Boolean check=DoAnService.autoPhanCongDoAnChoHD(khoaId);
            if (check){
                return new ResponseEntity(check,HttpStatus.OK);
            }else{
                return new ResponseEntity(check,HttpStatus.BAD_REQUEST);
            }

        }else{String error="Bạn chưa phân công giáo viên cho tất cả vai trò trong hội dồng";
            return new ResponseEntity(error,HttpStatus.BAD_REQUEST);
        }

    }
    @PutMapping("/da/update")
    public ResponseEntity updateDoAn(@RequestBody  DoAnModel DoAnModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            DoAnService.updateDoAn(DoAnModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/da/delete/{maDA}")
    public ResponseEntity deleteDoAn(@PathVariable(value = "maDA") int maDoAn,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            DoAnService.deleteDoAn(maDoAn);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
}
