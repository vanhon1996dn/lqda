package com.qlda.Controller;

import com.qlda.Model.DotBaoCaoModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class DotBaoCaoController {
    @Autowired
    private com.qlda.Service.DotBaoCaoService DotBaoCaoService;
    @Autowired
    private UserService userService;
    @GetMapping("/dbc")
    private ResponseEntity getAllDotBaoCao(){
        List<DotBaoCaoModel> DotBaoCaoList=DotBaoCaoService.getAllDotBaoCao();

        return new ResponseEntity(DotBaoCaoList, HttpStatus.OK);

    }
    @GetMapping("/dbc/{id}")
    private ResponseEntity getDotBaoCao(@PathVariable(value = "id") int DotBaoCaoID){
        DotBaoCaoModel DotBaoCao=DotBaoCaoService.getDotBaoCaoByID(DotBaoCaoID);
        return new ResponseEntity(DotBaoCao,HttpStatus.OK);
    }
    @GetMapping("/dbc/gv/{maGV}")
    private ResponseEntity getDotBaoCaosByMaGV(@PathVariable(value = "maGV") int maGV){
        List<DotBaoCaoModel> DotBaoCaos=DotBaoCaoService.getDotBaoCaosByMaGV(maGV);
        return new ResponseEntity(DotBaoCaos,HttpStatus.OK);
    }
    @GetMapping("/dbc/da/{maDoAn}")
    private ResponseEntity getDotBaoCaosByMaSV(@PathVariable(value = "maDoAn") int maDoAn){
        List<DotBaoCaoModel> DotBaoCaos=DotBaoCaoService.getDotBaoCaosByMaDoAn(maDoAn);
        return new ResponseEntity(DotBaoCaos,HttpStatus.OK);
    }

    @GetMapping("/dbc/create/bctd/{baoCaoTienDoId}")
    public ResponseEntity createAllDotBaoCaosByBaoCaoTienDoId(@PathVariable(value = "baoCaoTienDoId") int baoCaoTienDoId,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
                DotBaoCaoService.createAllDotBaoCaosByBaoCaoTienDoId(baoCaoTienDoId);
                return new ResponseEntity(HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }

    @PostMapping("/dbc/create")
    public ResponseEntity createDotBaoCao(@RequestBody DotBaoCaoModel DotBaoCaoModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            if(DotBaoCaoService.checkDotBaoCao(DotBaoCaoModel)){
                 int DotBaoCaoID = DotBaoCaoService.createDotBaoCao(DotBaoCaoModel);
                return new ResponseEntity(DotBaoCaoID ,HttpStatus.CREATED);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/dbc/update")
    public ResponseEntity updateDotBaoCao(@RequestBody  DotBaoCaoModel DotBaoCaoModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            if(DotBaoCaoModel.getVaiTroId()>1){
                DotBaoCaoService.updateDotBaoCao(DotBaoCaoModel);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/dbc/delete/{id}")
    public ResponseEntity deleteDotBaoCao(@PathVariable(value = "id") int DotBaoCaoID,@RequestHeader("Authorization") String token) {
        List<String> authorities = userService.getAuthoritiesByToken(token);
        if (authorities.contains("admin")) {
            DotBaoCaoService.deleteDotBaoCao(DotBaoCaoID);
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
    }
    @DeleteMapping("/dbc/delete/bctd/{id}")
        public ResponseEntity deleteDotBaoCaosByBaoCaoTienDoId(@PathVariable(value = "id") int BaoCaoTienDoId,@RequestHeader("Authorization") String token) {
            List<String> authorities=userService.getAuthoritiesByToken(token);
            if(authorities.contains("admin")) {
                DotBaoCaoService.deleteDotBaoCaoByBaoCaoTienDoId(BaoCaoTienDoId);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

    }
}
