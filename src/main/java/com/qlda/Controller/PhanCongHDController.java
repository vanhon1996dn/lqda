package com.qlda.Controller;

import com.qlda.Entity.HoiDong;
import com.qlda.Model.HoiDongModel;
import com.qlda.Model.PhanCongHDModel;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class PhanCongHDController {
    @Autowired
    private com.qlda.Service.PhanCongHDService PhanCongHDService;
    @Autowired
    private UserService userService;
    @GetMapping("/pchd")
    private ResponseEntity getAllPhanCongHD(){
        List<PhanCongHDModel> PhanCongHDList=PhanCongHDService.getAllPhanCongHD();
        return new ResponseEntity(PhanCongHDList, HttpStatus.OK);

    }
    @GetMapping("/pchd/{id}")
    private ResponseEntity getPhanCongHD(@PathVariable(value = "id") int PhanCongHDID){
        PhanCongHDModel PhanCongHD=PhanCongHDService.getPhanCongHDByID(PhanCongHDID);
        return new ResponseEntity(PhanCongHD,HttpStatus.OK);
    }
    @GetMapping("/pchd/hd/{maHD}")
    private ResponseEntity getPhanCongHDByMaHD(@PathVariable(value = "maHD") int maHD){
        List<PhanCongHDModel> PhanCongHDList=PhanCongHDService.getPhanCongHDsByMaHD(maHD);
        return new ResponseEntity(PhanCongHDList,HttpStatus.OK);
    }
    @PostMapping("/pchd/auto/hd")
    private ResponseEntity AutoPhanCongHDByMaHD(@RequestBody HoiDongModel hoiDongModel){
        boolean check=PhanCongHDService.autoPhanCongHD(hoiDongModel);
        return new ResponseEntity(check,HttpStatus.CREATED);
    }

    @PostMapping("/pchd/create")
    public ResponseEntity createPhanCongHD(@RequestBody PhanCongHDModel PhanCongHDModel,@RequestHeader("Authorization") String token){
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            if(PhanCongHDService.checkPhanCongHD(PhanCongHDModel)){
                int PhanCongHDID = PhanCongHDService.createPhanCongHD(PhanCongHDModel);
                return new ResponseEntity(PhanCongHDID ,HttpStatus.CREATED);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/pchd/update")
    public ResponseEntity updatePhanCongHD(@RequestBody  PhanCongHDModel PhanCongHDModel,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            if(PhanCongHDService.checkPhanCongHDUpdate(PhanCongHDModel)){
                PhanCongHDService.updatePhanCongHD(PhanCongHDModel);
                return new ResponseEntity(HttpStatus.OK);
            }else{
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/pchd/delete/{id}")
    public ResponseEntity deletePhanCongHD(@PathVariable(value = "id") int PhanCongHDID,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            PhanCongHDService.deletePhanCongHD(PhanCongHDID);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
    @DeleteMapping("/pchd/delete/hd/{maHD}")
    public ResponseEntity deletePhanCongHDByMaHD(@PathVariable(value = "maHD") int maHD,@RequestHeader("Authorization") String token) {
        List<String> authorities=userService.getAuthoritiesByToken(token);
        if(authorities.contains("admin")) {
            PhanCongHDService.deletePhanCongHDsByMaHD(maHD);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
