package com.qlda.Controller;

import com.qlda.Model.DoAnModel;
import com.qlda.Service.DoAnService;
import com.qlda.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin
@RestController()
@RequestMapping("/api/")
public class DoAnMauController {
    @Autowired
    private com.qlda.Service.DoAnMauService DoAnMauService;
    @Autowired
    private com.qlda.Service.DoAnDKService DoAnDKService;
    @Autowired
    private UserService userService;
    @GetMapping("/dam")
    private ResponseEntity getAllDoAnMau(){
        List<DoAnModel> DoAnMauList=DoAnMauService.getAllDoAnMau();
        return new ResponseEntity(DoAnMauList, HttpStatus.OK);

    }
    @GetMapping("/dam/{maDoAnMau}")
    private ResponseEntity getDoAnMau(@PathVariable(value = "maDoAnMau") int maDoAnMau){
        DoAnModel DoAnMau=DoAnMauService.getDoAnMauByMaDoAn(maDoAnMau);
        return new ResponseEntity(DoAnMau,HttpStatus.OK);
    }
    @GetMapping("/dam/gv/{maGV}")
    private ResponseEntity getDoAnMausByMaGiaoVien(@PathVariable(value = "maGV") int maGV){
        List<DoAnModel> doAnModels=DoAnMauService.getDoAnMausByMaGV(maGV);
        return new ResponseEntity(doAnModels,HttpStatus.OK);
    }

    @PostMapping("/dam/create")
    public ResponseEntity createDoAnMau(@RequestBody DoAnModel DoAnModel,@RequestHeader("Authorization") String token){
        String username=userService.getUserNameByToken(token);
        if(!username.equals(DoAnModel.getMaGVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("giaovien")) {
            int DoAnMauID = DoAnMauService.createDoAnMau(DoAnModel);
            return new ResponseEntity(DoAnMauID ,HttpStatus.CREATED);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @PutMapping("/dam/update")
    public ResponseEntity updateDoAnMau(@RequestBody  DoAnModel DoAnModel,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        if(!username.equals(DoAnModel.getMaGVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("giaovien")) {
            DoAnMauService.updateDoAnMau(DoAnModel);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }


    }
    @DeleteMapping("/dam/delete/{maDoAnMau}")
    public ResponseEntity deleteDoAnMau(@PathVariable(value = "maDoAnMau") int maDoAnMau,@RequestHeader("Authorization") String token) {
        String username=userService.getUserNameByToken(token);
        DoAnModel doAnModel= DoAnMauService.getDoAnMauByMaDoAn(maDoAnMau);
        if(!username.equals(doAnModel.getMaGVs().get(0).toString())&&(Integer.parseInt(username)<300000000)){
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        List<String> authorities=userService.getAuthoritiesByUserName(username);
        if(authorities.contains("admin")||authorities.contains("giaovien")) {
            DoAnMauService.deleteDoAnMau(maDoAnMau);
            return new ResponseEntity(HttpStatus.OK);
        }else{
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }

    }
}
